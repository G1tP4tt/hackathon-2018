webpackJsonp([0],{

/***/ 823:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentPermitPageModule", function() { return StudentPermitPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__student_permit__ = __webpack_require__(828);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var StudentPermitPageModule = /** @class */ (function () {
    function StudentPermitPageModule() {
    }
    StudentPermitPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__student_permit__["a" /* StudentPermitPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__student_permit__["a" /* StudentPermitPage */]),
            ],
        })
    ], StudentPermitPageModule);
    return StudentPermitPageModule;
}());

//# sourceMappingURL=student-permit.module.js.map

/***/ }),

/***/ 828:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StudentPermitPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the StudentPermitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var StudentPermitPage = /** @class */ (function () {
    function StudentPermitPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    StudentPermitPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad StudentPermitPage');
    };
    StudentPermitPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-student-permit',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/student-permit/student-permit.html"*/'<!--\n  Generated template for the StudentPermitPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n	<ion-navbar>\n		<ion-title>Aufenthalt</ion-title>\n	</ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n	<ion-card>\n		<ion-card-header>\n			Antrag auf Erteilung einer Aufenthaltserlaubnis/ Niederlassungserlaubnis/ Duldung\n		</ion-card-header>\n		<ion-card-content>\n			<a href="https://www.freiburg.de/pb/site/Freiburg/get/params_E1077201864/714882/AE-Antrag%20NEU%20gesch%C3%BCtzt.pdf">Formular zum Herunterladen</a>\n		</ion-card-content>\n\n	</ion-card>\n\n	<ion-card>\n		<ion-card-header>\n			Aufenthalt für eine Au-pair-Beschäftigung\n		</ion-card-header>\n		<ion-card-content>\n			<a href="https://www.freiburg.de/pb/site/Freiburg/get/327401/Aufenthalt-f%C3%BCr-eine-Au-pair-Besch%C3%A4ftigung.pdf">Informationen zum Herunterladen</a>\n		</ion-card-content>\n\n	</ion-card>\n\n	<ion-card>\n		<ion-card-header>\n			Aufenthalt von Gastwissenschaftlern\n		</ion-card-header>\n		<ion-card-content>\n			<a href="https://www.freiburg.de/pb/site/Freiburg/get/327402/Aufenthalt-von-Gastwissenschaftlern.pdf">Informationen zum Herunterladen</a>\n		</ion-card-content>\n		\n	</ion-card>\n\n	<ion-card>\n		<ion-card-header>\n			Aufenthaltserlaubnis für Studierende\n		</ion-card-header>\n		<ion-card-content>\n			<a href="https://www.freiburg.de/pb/site/Freiburg/get/327403/Aufenthaltserlaubnis-f%C3%BCr-Studierende.pdf">Informationen zum Herunterladen</a>\n		</ion-card-content>\n		\n	</ion-card>\n\n	<ion-card>\n		<ion-card-header>\n			Verpflichtungserklärung\n		</ion-card-header>\n		<ion-card-content>\n			Einladung von Besuchern aus dem Ausland<br>\n			<a href="http://www.freiburg.de/pb/site/Freiburg/get/327410/Erkl%C3%A4rung_Verpflichtungserkl%C3%A4rung.pdf">Formular zum Herunterladen</a>\n		</ion-card-content>\n		\n	</ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/student-permit/student-permit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], StudentPermitPage);
    return StudentPermitPage;
}());

//# sourceMappingURL=student-permit.js.map

/***/ })

});
//# sourceMappingURL=0.js.map