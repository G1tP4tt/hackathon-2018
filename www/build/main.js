webpackJsonp([24],{

/***/ 191:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AttractionDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AttractionDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AttractionDetailPage = /** @class */ (function () {
    function AttractionDetailPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AttractionDetailPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AttractionDetailPage');
    };
    AttractionDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-attraction-detail',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/attraction-detail/attraction-detail.html"*/'<!--\n  Generated template for the AttractionDetailPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n        <ion-title>Sehenswürdigkeiten</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <ion-card (click)="newsDetails()">\n        <img src="../../assets/imgs/muenster2.jpg"/>\n        <button ion-button full (click)="goToMap()">Auf Karte anzeigen</button>\n        <ion-card-content>\n            <ion-card-title>\n                Das Freiburger Münster\n            </ion-card-title>\n            <p>\n                Das Freiburger Münster (oder Münster Unserer Lieben Frau) ist die im romanischen und größtenteils im\n                gotischen Stil erbaute römisch-katholische Stadtpfarrkirche von Freiburg im Breisgau. Sie wurde von etwa\n                1200 bis offiziell 1513 erbaut. Da Freiburg seit 1827 Bischofssitz ist (Erzbistum Freiburg), ist die\n                Kirche heute formell eine Kathedrale, wird aber aus Tradition „Münster“ und nicht „Kathedrale“ genannt.\n                Die Münstergemeinde gehört zur Seelsorgeeinheit Freiburg Mitte im Dekanat Freiburg.\n\n                Der bekannte Kunsthistoriker Jacob Burckhardt sagte 1869 in einer Vortragsreihe über den 116 Meter hohen\n                Turm im Vergleich mit Basel und Straßburg: Und Freiburg wird wohl der schönste Turm auf Erden\n                bleiben.[1] Daraus entwickelte sich wohl das häufig gehörte, aber nicht ganz wörtliche Zitat vom\n                „schönsten Turm der Christenheit“. Kunsthistoriker aus der ganzen Welt rühmen das Münster Unserer Lieben\n                Frau zu Freiburg als ein architektonisches Meisterwerk der Gotik.\n            </p>\n        </ion-card-content>\n    </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/attraction-detail/attraction-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], AttractionDetailPage);
    return AttractionDetailPage;
}());

//# sourceMappingURL=attraction-detail.js.map

/***/ }),

/***/ 192:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParkDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__kita_map_kita_map__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ParkDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ParkDetailPage = /** @class */ (function () {
    function ParkDetailPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ParkDetailPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ParkDetailPage');
    };
    ParkDetailPage.prototype.goToMap = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__kita_map_kita_map__["a" /* KitaMapPage */]);
    };
    ParkDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-park-detail',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/park-detail/park-detail.html"*/'<!--\n  Generated template for the ParkDetailPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n        <ion-title>Parkanlagen</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <ion-card (click)="newsDetails()">\n        <img src="../../assets/imgs/alterfriedhof7.jpg"/>\n        <button ion-button full (click)="goToMap()">Auf Karte anzeigen</button>\n        <ion-card-content>\n            <ion-card-title>\n                Alter Friedhof\n            </ion-card-title>\n            <p>\n                Der alte Friedhof in Freiburg-Herdern wurde nach der Stilllegung zur Parkanlage umgewidmet. Fast über\n                zwei Jahrhunderte lang (1683 bis 1872) wurden hier Freiburger Bürgerinnen und Bürger beigesetzt. Er ist\n                sowohl Natur- als auch Kulturdenkmal. Daß diese „Grüne Lunge“ im Mittelpunkt Freiburgs noch existiert\n                und keinem „Bauboom“ zum Opfer fiel, soll damit zusammenhängen, dass der berühmte Freiburger Architekt\n                und Barockkünstler Johann-Christian Wenzinger verfügte, er hinterlasse sein großes Vermögen nur dann dem\n                Freiburger Stiftungsfond, wenn sein Grab „auf ewige Zeiten“ auf dem alten Friedhof gesichert sei.\n            </p>\n        </ion-card-content>\n    </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/park-detail/park-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ParkDetailPage);
    return ParkDetailPage;
}());

//# sourceMappingURL=park-detail.js.map

/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdministrationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AdministrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AdministrationPage = /** @class */ (function () {
    function AdministrationPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AdministrationPage.prototype.goToPage = function (page) {
        this.navCtrl.push(page);
    };
    AdministrationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdministrationPage');
    };
    AdministrationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-administration',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/administration/administration.html"*/'<!--\n  Generated template for the AdministrationPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n	<ion-navbar>\n		<ion-title>Verwaltung</ion-title>\n	</ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n	<ion-card (click)="goToPage(\'StudentPermitPage\')">\n\n		<ion-grid>\n			<ion-row >\n				<ion-col col-3>\n\n					<img src="../../assets/imgs/muenster.jpg">\n				</ion-col>\n				<ion-col col-9 >\n					<h2>Aufenthalt</h2>\n\n				</ion-col>\n			</ion-row>\n		</ion-grid>	\n\n	</ion-card>\n\n	<ion-card (click)="goToPage(\'RealEstatePage\')">\n\n		<ion-grid>\n			<ion-row >\n				<ion-col col-3>\n\n					<img src="../../assets/imgs/RealEstate.png">\n				</ion-col>\n				<ion-col col-9 >\n					<h2>Mietspiegel</h2>\n					<p>Bestellung des aktuellen Mietspiegels</p>\n				</ion-col>\n			</ion-row>\n		</ion-grid>\n\n	</ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/administration/administration.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], AdministrationPage);
    return AdministrationPage;
}());

//# sourceMappingURL=administration.js.map

/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DumpPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shard_alert_shard_alert__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dumpcalendar_dumpcalendar__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the DumpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DumpPage = /** @class */ (function () {
    function DumpPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    DumpPage.prototype.goToShardAlert = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__shard_alert_shard_alert__["a" /* ShardAlertPage */]);
    };
    DumpPage.prototype.goToDumpcalendar = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__dumpcalendar_dumpcalendar__["a" /* DumpcalendarPage */]);
    };
    DumpPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DumpPage');
    };
    DumpPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-dump',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/dump/dump.html"*/'<!--\n  Generated template for the DumpPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n        <ion-title>Müll</ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n    <ion-card (click)="goToDumpcalendar()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://i.ebayimg.com/images/g/OaMAAOSwWKtUtPA2/s-l225.jpg">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Abfallkalender</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="goToShardAlert()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="../../assets/imgs/asf.jpg">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Abfall- & Scherbenmelder</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="goToShardAlert()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="../../assets/imgs/sperrmuell.jpg">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Spermüll</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/dump/dump.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], DumpPage);
    return DumpPage;
}());

//# sourceMappingURL=dump.js.map

/***/ }),

/***/ 195:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShardAlertPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ShardAlertPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ShardAlertPage = /** @class */ (function () {
    function ShardAlertPage(navCtrl, navParams, alertCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
    }
    ShardAlertPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ShardAlertPage');
    };
    ShardAlertPage.prototype.sendForm = function () {
        var toast = this.toastCtrl.create({
            message: 'Verschmutzung wurde erfolgreich gemeldet',
            duration: 3000,
            position: "top"
        });
        this.navCtrl.pop();
        toast.present();
    };
    ShardAlertPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-shard-alert',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/shard-alert/shard-alert.html"*/'<!--\nGenerated template for the ShardAlertPage page.\n\nSee http://ionicframework.com/docs/components/#navigation for more info on\nIonic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n        <ion-title>Abfall- & Scherbenmelder</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-card>\n        <br>\n        <p>Ein spezielles Reinigungsteam der ASF GmbH säubert verschmutzte Straßen und Flächen, holt wilden Müll und\n            entfernt Scherben auf Radwegen. Sagen Sie uns, wo es in Freiburg schmutzig ist unter Tel.76707-710, oder\n            füllen Sie einfach dieses Formular aus und senden es uns Online...und der Dreck ist weg.\n        </p>\n        <ion-list inset>\n            <ion-item>\n                <ion-label><h3>Name:</h3></ion-label>\n                <ion-input type="text"></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-label><h3>Verschmutzungsart:</h3></ion-label>\n                <ion-input type="text"></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-label><h3>LAT:</h3></ion-label>\n                <ion-label type="text"></ion-label>\n            </ion-item>\n            <ion-item>\n                <ion-label><h3>LNG:</h3></ion-label>\n                <ion-input type="text"></ion-input>\n            </ion-item>\n        </ion-list>\n    </ion-card>\n    <ion-list inset>\n\n    </ion-list>\n\n\n    <button ion-button full (click)="sendForm()">Verschmutzung melden</button>\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/shard-alert/shard-alert.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], ShardAlertPage);
    return ShardAlertPage;
}());

//# sourceMappingURL=shard-alert.js.map

/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DumpcalendarPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the DumpcalendarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DumpcalendarPage = /** @class */ (function () {
    function DumpcalendarPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.optionsMulti = {
            pickMode: 'multi'
        };
    }
    DumpcalendarPage.prototype.onChange = function ($event) {
        console.log($event);
    };
    DumpcalendarPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DumpcalendarPage');
    };
    DumpcalendarPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-dumpcalendar',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/dumpcalendar/dumpcalendar.html"*/'<!--\n  Generated template for the DumpcalendarPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>dumpcalendar</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-calendar [(ngModel)]="date"\n                (onChange)="onChange($event)"\n                [options]="optionsMulti"\n                [type]="type"\n                [format]="\'YYYY-MM-DD\'">\n  </ion-calendar>\n\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/dumpcalendar/dumpcalendar.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], DumpcalendarPage);
    return DumpcalendarPage;
}());

//# sourceMappingURL=dumpcalendar.js.map

/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the EventsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EventsPage = /** @class */ (function () {
    function EventsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    EventsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EventsPage');
    };
    EventsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-events',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/events/events.html"*/'<!--\n  Generated template for the EventsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n        <ion-title>Events</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <ion-card>\n        <img src="../../assets/imgs/stadt.jpeg"/>\n        <ion-card-content>\n            <ion-card-title>\n                Stadtführung Schätze der Altstadt\n            </ion-card-title>\n            <p>\n                Fr. 24. Juni 09:30 Uhr\n            </p>\n        </ion-card-content>\n    </ion-card>\n    <ion-card>\n        <img src="../../assets/imgs/sunriseAve.jpeg"/>\n        <ion-card-content>\n            <ion-card-title>\n                Sunrise Avenue - Heartbreak Century\n            </ion-card-title>\n            <p>\n                Fr. 29. Juni 19:30 Uhr\n            </p>\n        </ion-card-content>\n    </ion-card>\n    <ion-card>\n        <img src="../../assets/imgs/party.jpeg"/>\n        <ion-card-content>\n            <ion-card-title>\n                After Work Party\n            </ion-card-title>\n            <p>\n                Fr. 29. Juni 19:30 Uhr\n            </p>\n        </ion-card-content>\n    </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/events/events.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], EventsPage);
    return EventsPage;
}());

//# sourceMappingURL=events.js.map

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HandicapedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__kita_map_kita_map__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the HandicapedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HandicapedPage = /** @class */ (function () {
    function HandicapedPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    HandicapedPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HandicapedPage');
    };
    HandicapedPage.prototype.goToMap = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__kita_map_kita_map__["a" /* KitaMapPage */]);
    };
    HandicapedPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-handicaped',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/handicaped/handicaped.html"*/'<!--\n  Generated template for the HandicapedPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n	<ion-navbar>\n		<ion-title>handicaped</ion-title>\n	</ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n	<ion-card (click)="goToMap()">\n		<ion-grid>\n			<ion-row >\n				<ion-col col-3>\n					<img src="../../assets/icon/behindertenParkplatz.png">\n				</ion-col>\n				<ion-col col-9>\n					<h2>Parkplätze</h2>\n				</ion-col>\n			</ion-row>\n		</ion-grid>\n	</ion-card>\n	<ion-card (click)="goToMap()">\n		<ion-grid>\n			<ion-row >\n				<ion-col col-3>\n					<img src="../../assets/icon/behindert.gif">\n				</ion-col>\n				<ion-col col-9>\n					<h2>Toiletten</h2>\n				</ion-col>\n			</ion-row>\n		</ion-grid>\n	</ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/handicaped/handicaped.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], HandicapedPage);
    return HandicapedPage;
}());

//# sourceMappingURL=handicaped.js.map

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IdeaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the IdeaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var IdeaPage = /** @class */ (function () {
    function IdeaPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    IdeaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad IdeaPage');
    };
    IdeaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-idea',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/idea/idea.html"*/'<!--\n  Generated template for the IdeaPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Idee einreichen</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/idea/idea.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], IdeaPage);
    return IdeaPage;
}());

//# sourceMappingURL=idea.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LogoutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the LogoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LogoutPage = /** @class */ (function () {
    function LogoutPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    LogoutPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LogoutPage');
    };
    LogoutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-logout',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/logout/logout.html"*/'<!--\n  Generated template for the LogoutPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>logout</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/logout/logout.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], LogoutPage);
    return LogoutPage;
}());

//# sourceMappingURL=logout.js.map

/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the NewsDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NewsDetailPage = /** @class */ (function () {
    function NewsDetailPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    NewsDetailPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NewsDetailPage');
    };
    NewsDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-news-detail',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/news-detail/news-detail.html"*/'<!--\n  Generated template for the NewsDetailPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n        <ion-title>News</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <ion-card (click)="newsDetails()">\n        <img src="https://www.welt.de/img/sport/fussball/wm-2018/mobile178099872/5661628087-ci23x11-w590/FBL-WC-2018-MATCH27-GER-SWE.jpg"/>\n        <ion-card-content>\n            <ion-card-title>\n                Blut, Schweiß und Tränen. Und dann kam Kroos\n            </ion-card-title>\n            <p>\n                Die historische Pleite konnte gerade noch abgewendet werden. Deutschland besiegt Schweden 2:1.\n            </p>\n            <p>\n                Beinahe wäre Mario Gomez zum Held geworden, doch seinen gut platzierten Kopfball lenkte Schwedens\n                Torwart Robin Olsen in der 89. Minute mit einer Glanzparade über das Tor, in der Nachspielzeit traf\n                Julian Brandt nur den Pfosten. Doch dann kam Toni Kroos. In der 95. Minute zirkelte der\n                Champions-League-Sieger einen Freistoß ins Tor und ließ Deutschland jubeln.\n\n                So hieß es am Ende 2:1 für Deutschland gegen Schweden – obwohl der Weltmeister nach einer Gelb-Roten\n                Karte für Jerome Boateng die letzten zehn Minuten in Unterzahl spielte und nun weiter auf das\n                Achtelfinale hoffen kann.\n            </p>\n            <p>\n                Vier Änderungen nahm Trainer Joachim Löw in seiner Startaufstellung vor: Antonio Rüdiger ersetzte den\n                verletzten Mats Hummels, Marco Reus und Sebastian Rudy verdrängten Sami Khedira und Mesut Özil, dazu\n                kehrte Jonas Hector zurück in die Mannschaft. Im Sturm begann erneut Timo Werner.\n\n                Tore des Spiels\n                Wieder leitete ein individueller Fehler das deutsche Gegentor ein. In der 32. Minute spielte Toni Kroos\n                im Aufbauspiel unbedrängt einen Fehlpass zu Marcus Berg. Die Schweden konterten, eine Flanke von Emil\n                Forsberg nahm Ola Toivonen hervorragend an, behauptete sich gegen Antonio Rüdiger und versenkte seinen\n                Schuss im Tor.\n            </p>\n            <p>\n                Die Halbzeit tat der deutschen Mannschaft gut. Mario Gomez kam für Julian Draxler, in der 48. Minute\n                traf Marco Reus per Knie zum Ausgleich, nach einer Hereingabe von Timo Werner.\n\n                In der 82. Minute flog Jerome Boateng mit Gelb-Rot vom Platz. Erst hatte er gut zehn Minuten zuvor einen\n                Konter von Emil Forsberg per Foul gestoppt, dann ging er übermotiviert in einen Zweikampf mit Marcus\n                Berg.\n\n                Mit der letzten deutschen Offensivaktion des Spiels wurde dann Toni Kroos mit seinem Freistoßtor vom\n                tragischen zum positiven Helden.\n\n                Auswirkung des Spiels\n                Deutschland und Schweden haben jetzt beide drei Punkte, Mexiko sechs. Die Szenarien sind zahlreich, am\n                Ende könnten sowohl Mexiko, Schweden als auch Deutschland bei sechs Punkten stehen, dann entscheidet das\n                Torverhältnis. Deutschland spielt am Mittwoch um 16 Uhr gegen Südkorea, parallel trifft Mexiko auf\n                Schweden. Ein deutscher Sieg gegen Südkorea würde höchstwahrscheinlich für das Achtelfinale reichen, es\n                sei denn Schweden gewinnt parallel höher.\n            </p>\n            <p>\n                Deutsche Taktik des Spiels\n                Deutschlands Offensivspiel kam in den ersten Minuten mit deutlich mehr Tempo und Spielwitz daher, als\n                noch im Spiel gegen Mexiko. Die Viererreihe aus Müller, Reus, Draxler und Werner wechselte munter die\n                Positionen, was sie unausrechenbarer und gefährlicher machte. In der ersten Partie klebte jeder noch\n                großteils auf seinem Posten, jetzt tauchte Werner häufig rechts, Müller öfter in der Mitte und links\n                auf, auch Reus und Draxler rochierten viel.\n\n                Mit der Hereinnahme von Mario Gomez rückte Timo Werner weiter nach Außen, Gomez gab den Stoßstürmer,\n                Reus und Müller machten ihre Rochaden weiter. Viele der deutschen Angriffe kamen nun über die Flügel, wo\n                auch Hector und Kimmich viele Bälle in die Mitte brachten.\n\n\n                Defensiv wurde von den Angriffsspielern mehr nach Hinten gearbeitet. Den ersten Vorstoß der Schweden\n                stoppte beispielsweise der zurückeilende Thomas Müller. Doch schnell war es dahin mit der defensiven\n                Stabilität: Wieder war Deutschland anfällig für Konter, ließ viel Platz für Gegenstöße. Nicht nur das\n                Tor von Toivonen fiel so, Schweden hatte auch mehrere weitere gute Chancen im Umschaltspiel. Auch weil\n                sich die Anordnung mit Ilkay Gündogan im Mittelfeld veränderte.\n\n                Nach der Halbzeit drängte Deutschland so sehr nach Vorne, dass Antonio Rüdiger die letzte Absicherung\n                darstellte: An der Mittellinie. Doch den Schweden boten sich kaum Gelegenheiten überhaupt zu kontern,\n                weil Deutschland vorne ballsicherer war, seinen Gegner selten mit Fehlern beschenkte. Auch raubte das\n                Verteidigen den Skandinaviern Kraft, sie spielten zunehmend lieber Sicherheitspässe anstatt schnell\n                umzuschalten.\n            </p>\n        </ion-card-content>\n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/news-detail/news-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], NewsDetailPage);
    return NewsDetailPage;
}());

//# sourceMappingURL=news-detail.js.map

/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__news_detail_news_detail__ = __webpack_require__(201);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the NewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NewsPage = /** @class */ (function () {
    function NewsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    NewsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NewsPage');
    };
    NewsPage.prototype.newsDetails = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__news_detail_news_detail__["a" /* NewsDetailPage */]);
    };
    NewsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-news',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/news/news.html"*/'<!--\n  Generated template for the NewsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n        <ion-title>News</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n    <ion-card (click)="newsDetails()">\n        <img src="https://www.welt.de/img/sport/fussball/wm-2018/mobile178099872/5661628087-ci23x11-w590/FBL-WC-2018-MATCH27-GER-SWE.jpg"/>\n        <ion-card-content>\n            <ion-card-title>\n                Blut, Schweiß und Tränen. Und dann kam Kroos\n            </ion-card-title>\n            <p>\n                Die historische Pleite konnte gerade noch abgewendet werden. Deutschland besiegt Schweden 2:1.\n            </p>\n        </ion-card-content>\n    </ion-card>\n    <ion-card>\n        <img src="../../assets/imgs/startup.jpg"/>\n        <ion-card-content>\n            <ion-card-title>\n                Die Lokalhalle ist jetzt Kreativpark\n            </ion-card-title>\n            <p>\n                In umgebauten Überseecontainern arbeiten mehr als 100 Entrepreneure und Start-ups\n            </p>\n        </ion-card-content>\n    </ion-card>\n    <ion-card>\n        <img src="../../assets/imgs/spinner.jpeg"/>\n        <ion-card-content>\n            <ion-card-title>\n                Eichenprozessionsspinner in Hochdorfer Waldkindergarten ist entfernt\n            </ion-card-title>\n            <p>\n                Das Freiburger Forstamt hat Nester des Eichenprozessionsspinners im Waldkindergarten in Hochdorf\n                entfernt. Die Warnung gilt jedoch weiter. Denn die Haare der Raupen sind nach zwei Jahren noch giftig.\n            </p>\n        </ion-card-content>\n    </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/news/news.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], NewsPage);
    return NewsPage;
}());

//# sourceMappingURL=news.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParkAreasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__park_detail_park_detail__ = __webpack_require__(192);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ParkAreasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ParkAreasPage = /** @class */ (function () {
    function ParkAreasPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ParkAreasPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ParkAreasPage');
    };
    ParkAreasPage.prototype.parkDetails = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__park_detail_park_detail__["a" /* ParkDetailPage */]);
    };
    ParkAreasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-park-areas',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/park-areas/park-areas.html"*/'<!--\n  Generated template for the ParkAreasPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n        <ion-title>Parkanlagen</ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n\n<ion-content>\n    <button ion-button block (click)="goToMap()"><ion-icon name="ios-map-outline"> </ion-icon>Auf Karte anzeigen </button>\n    <ion-card (click)="parkDetails()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://www.freiburg.de/pb/site/Freiburg/data/233128/Lde/resize/alterfriedhof6.jpg?f=%2Fpb%2Fsite%2FFreiburg%2Fget%2Fparams_E1867514544%2F266404%2Falterfriedhof6.jpg&w=165&h=165&i=0&m=C">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Alter Friedhof</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="parkDetails()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://www.freiburg.de/pb/site/Freiburg/get/params_E736056137/386927/colombipark_0001.jpg">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Colombipark</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="parkDetails()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://www.freiburg.de/pb/site/Freiburg/data/233164/Lde/resize/GuT_Leiste_Dietenbach03.jpg?f=%2Fpb%2Fsite%2FFreiburg%2Fget%2Fparams_E1225261193%2F391037%2FGuT_Leiste_Dietenbach03.jpg&w=165&h=600&i=0&m=F">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Dietenbachpark</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="parkDetails()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://www.freiburg.de/pb/site/Freiburg/data/233156/Lde/resize/GuT_Leiste_EkzWeingarten01.jpg?f=%2Fpb%2Fsite%2FFreiburg%2Fget%2Fparams_E-485096711%2F391003%2FGuT_Leiste_EkzWeingarten01.jpg&w=165&h=600&i=0&m=F">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>EKZ-Weingarten</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="parkDetails()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://www.freiburg.de/pb/site/Freiburg/data/233144/Lde/resize/GuT_Eschholzpark02.jpg?f=%2Fpb%2Fsite%2FFreiburg%2Fget%2Fparams_E13514474%2F390940%2FGuT_Eschholzpark02.jpg&w=165&h=600&i=0&m=F">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Eschholzpark</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="parkDetails()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://www.freiburg.de/pb/site/Freiburg/data/233192/Lde/resize/GuT_Leiste_Gruenkeil02.jpg?f=%2Fpb%2Fsite%2FFreiburg%2Fget%2Fparams_E-448396856%2F595442%2FGuT_Leiste_Gruenkeil02.jpg&w=165&h=600&i=0&m=F">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Grünkeil Rieselfeld</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="parkDetails()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://www.freiburg.de/pb/site/Freiburg/data/233188/Lde/resize/GuT_Leiste_Hauptfriedhof02.jpg?f=%2Fpb%2Fsite%2FFreiburg%2Fget%2Fparams_E-1685437215%2F595401%2FGuT_Leiste_Hauptfriedhof02.jpg&w=165&h=600&i=0&m=F">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Hauptfriedhof</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="parkDetails()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://www.freiburg.de/pb/site/Freiburg/data/233136/Lde/resize/japangarten11.jpg?f=%2Fpb%2Fsite%2FFreiburg%2Fget%2Fparams_E1490976%2F390891%2Fjapangarten11.jpg&w=165&h=600&i=0&m=F">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Japangarten</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="parkDetails()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://www.freiburg.de/pb/site/Freiburg/data/233160/Lde/resize/GuT_Leiste_Brandel01.jpg?f=%2Fpb%2Fsite%2FFreiburg%2Fget%2Fparams_E513524299%2F391021%2FGuT_Leiste_Brandel01.jpg&w=165&h=600&i=0&m=F">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Joseph-Brandel-Anlage</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="parkDetails()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://www.freiburg.de/pb/site/Freiburg/data/233184/Lde/resize/GuT_Leiste_KGuenther04.jpg?f=%2Fpb%2Fsite%2FFreiburg%2Fget%2Fparams_E-1783362524%2F391112%2FGuT_Leiste_KGuenther04.jpg&w=165&h=600&i=0&m=F">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Konrad-Guenther-Park</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="parkDetails()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://www.freiburg.de/pb/site/Freiburg/data/233168/Lde/resize/GuT_Leiste_MitlGraben02.jpg?f=%2Fpb%2Fsite%2FFreiburg%2Fget%2Fparams_E152404455%2F391052%2FGuT_Leiste_MitlGraben02.jpg&w=165&h=600&i=0&m=F">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Mittlerer Graben Rieselfeld</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="parkDetails()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://www.freiburg.de/pb/site/Freiburg/data/233148/Lde/resize/GuT_Leiste_Moesle_Waldsee04.jpg?f=%2Fpb%2Fsite%2FFreiburg%2Fget%2Fparams_E-12297340%2F390962%2FGuT_Leiste_Moesle_Waldsee04.jpg&w=165&h=600&i=0&m=F">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Möslepark</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="parkDetails()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://www.freiburg.de/pb/site/Freiburg/data/233176/Lde/resize/GuT_Leiste_Moosweiher02.jpg?f=%2Fpb%2Fsite%2FFreiburg%2Fget%2Fparams_E2087074138%2F391082%2FGuT_Leiste_Moosweiher02.jpg&w=165&h=600&i=0&m=F">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Moosweiher</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="parkDetails()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://www.freiburg.de/pb/site/Freiburg/data/233140/Lde/resize/Kanonenplatz.jpg?f=%2Fpb%2Fsite%2FFreiburg%2Fget%2Fparams_E320697256%2F390915%2FKanonenplatz.jpg&w=165&h=600&i=0&m=F">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Schloßberg</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="parkDetails()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://www.freiburg.de/pb/site/Freiburg/data/233132/Lde/resize/GUT_Seepark_Leiste1.jpg?f=%2Fpb%2Fsite%2FFreiburg%2Fget%2Fparams_E-20935576%2F390866%2FGUT_Seepark_Leiste1.jpg&w=165&h=600&i=0&m=F">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Seepark</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="parkDetails()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://www.freiburg.de/pb/site/Freiburg/data/233152/Lde/resize/GuT_Leiste_Stadtgarten02.jpg?f=%2Fpb%2Fsite%2FFreiburg%2Fget%2Fparams_E1651690329%2F595410%2FGuT_Leiste_Stadtgarten02.jpg&w=165&h=600&i=0&m=F">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Stadtgarten</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="parkDetails()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://www.freiburg.de/pb/site/Freiburg/data/233172/Lde/resize/GuT_Leiste_Stuehlinger04.jpg?f=%2Fpb%2Fsite%2FFreiburg%2Fget%2Fparams_E720506141%2F391068%2FGuT_Leiste_Stuehlinger04.jpg&w=165&h=600&i=0&m=F">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Stühlinger Kirchplatz</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n    <ion-card (click)="parkDetails()">\n        <ion-grid>\n            <ion-row>\n                <ion-col col-3>\n                    <img src="https://www.freiburg.de/pb/site/Freiburg/data/233180/Lde/resize/GUT_Leiste_Zaehringerpark02.jpg?f=%2Fpb%2Fsite%2FFreiburg%2Fget%2Fparams_E-806486310%2F391096%2FGUT_Leiste_Zaehringerpark02.jpg&w=165&h=600&i=0&m=F">\n                </ion-col>\n                <ion-col col-9>\n                    <h2>Zähringer Park</h2>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/park-areas/park-areas.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ParkAreasPage);
    return ParkAreasPage;
}());

//# sourceMappingURL=park-areas.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParkingPermitPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ParkingPermitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ParkingPermitPage = /** @class */ (function () {
    function ParkingPermitPage(navCtrl, navParams, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
    }
    ParkingPermitPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ParkingPermitPage');
    };
    ParkingPermitPage.prototype.sendForm = function () {
        var toast = this.toastCtrl.create({
            message: 'Formular wurde erfolgeich übermittelt',
            duration: 3000,
            position: "top"
        });
        this.navCtrl.pop();
        toast.present();
    };
    ParkingPermitPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-parking-permit',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/parking-permit/parking-permit.html"*/'<!--\n  Generated template for the ParkingPermitPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n        <ion-title>Parkausweis</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n    <ion-slides pager>\n\n        <ion-slide>\n            <ion-card>\n                <ion-card-header>\n                    Einwilligungserklärung\n                </ion-card-header>\n                <p>\n                    Für den Schutz Ihrer personenbezogenen Daten haben wir alle technischen und organisatorischen\n                    Maßnahmen getroffen, um ein hohes Schutzniveau zu schaffen. Wir halten uns dabei strikt an die\n                    Datenschutzgesetze und die sonstigen datenschutzrelevaten Vorschriften. Ihre Daten werden\n                    ausschließlich über sichere Kommunikationswege an die zuständige Stelle übergeben.\n\n                    Zur Bearbeitung Ihres Anliegens werden personenbezogene Daten von Ihnen erhoben wie z.B. Name,\n                    Anschrift, Kontaktdaten sowie die notwendigen Angaben zur Bearbeitung. Die Verwendung oder\n                    Weitergabe Ihrer Daten an unbeteiligte Dritte wird ausgeschlossen.\n                </p>\n                <ion-item>\n                    <ion-label>Ich bin damit einverstanden</ion-label>\n                    <ion-checkbox color="secondary" checked="false"></ion-checkbox>\n                </ion-item>\n            </ion-card>\n        </ion-slide>\n\n        <ion-slide>\n            <ion-card>\n                <ion-card-header>\n                    Benötigte Unterlagen:\n                </ion-card-header>\n                <ul>\n                    <li>KFZ-Schein</li>\n                    <li>Ggf. Halterbestätigung , wenn Sie als Antragsteller nicht gleichzeitig Fahrzeughalter sind.</li>\n                    <li>Bei Umschreibung wegen Umzug von einem Bewohnerparkgebiet in ein anderes wird der alte\n                        Bewohnerparkausweis benötigt.\n                    </li>\n                </ul>\n\n                <p>Bitte wählen Sie die gewünschte Antragsart, indem Sie den entsprechenden Button anklicken:</p>\n                <br>\n                <button ion-button color="light">Neuantrag</button>\n                <button ion-button color="light">Verlängerung</button>\n                <button ion-button color="light">Ersatz</button>\n                <button ion-button color="light">Änderung</button>\n                <br>\n            </ion-card>\n        </ion-slide>\n\n        <ion-slide>\n            <ion-card>\n                <ion-card-header>\n                    Voraussetzungen:\n                </ion-card-header>\n                <ul>\n                    <li>Sie müssen in einem Bewohnerparkgebiet der Stadt Freiburg Ihre alleinige Wohnung bzw. Ihre von\n                        Ihnen vorwiegend benutzte Wohnung haben und entsprechend gemeldet sein.\n                    </li>\n                    <li>Sie dürfen über keine Garage oder privaten Parkplatz verfügen.</li>\n                    <li>Das Kraftfahrzeug, für das eine Bewohnerpark- berechtigung ausgestellt werden soll, muss auf Sie\n                        zugelassen sein oder dauerhaft von Ihnen genutzt werden. Im zweiten Fall benötigen wir eine\n                        Bestätigung des Halters, dass er Ihnen das Fahrzeug dauerhaft überlässt\n                    </li>\n                    <li>Wird eine dritte Person mit der Beantragung eines Bewohnerparkausweises beauftragt, ist eine\n                        formlose Vollmacht erforderlich (bei Eheleuten ist eine Vollmacht nicht erforderlich).\n                    </li>\n                    <li>Verwaltungsgebühr: 30,00 Euro</li>\n                </ul>\n                <br>\n            </ion-card>\n        </ion-slide>\n        <ion-slide>\n            <ion-card>\n                <ion-card-header>\n                    Angaben zur Person:\n                </ion-card-header>\n                <ion-list inset>\n                    <ion-item>\n                        <ion-label>Anrede</ion-label>\n                        <ion-input type="text"></ion-input>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>Firma</ion-label>\n                        <ion-input type="text"></ion-input>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>Name</ion-label>\n                        <ion-input type="text"></ion-input>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>Vorname</ion-label>\n                        <ion-input type="text"></ion-input>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>Geburtsdatum</ion-label>\n                        <ion-input type="text"></ion-input>\n                    </ion-item>\n                    <ion-item-divider color="light">Anschrift der Hauptwohnung</ion-item-divider>\n                    <ion-item>\n                        <ion-label>Straße</ion-label>\n                        <ion-input type="text"></ion-input>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>Hausnummer</ion-label>\n                        <ion-input type="text"></ion-input>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>Zusatz</ion-label>\n                        <ion-input type="text"></ion-input>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>Postleitzahl</ion-label>\n                        <ion-input type="text"></ion-input>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>Wohnort</ion-label>\n                        <ion-input type="text"></ion-input>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>Telefon</ion-label>\n                        <ion-input type="text"></ion-input>\n                    </ion-item><ion-item>\n                        <ion-label>E-Mail</ion-label>\n                        <ion-input type="text"></ion-input>\n                    </ion-item>\n                </ion-list>\n            </ion-card>\n        </ion-slide>\n        <ion-slide>\n            <ion-card>\n                <ion-card-header>\n                    Angaben zum KFZ\n                </ion-card-header>\n                <ion-list inset>\n                    <ion-item>\n                        <ion-label>Kennzeichen</ion-label>\n                        <ion-input type="text"></ion-input>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>KFZ-Typ</ion-label>\n                        <ion-input type="dropdown"></ion-input>\n                    </ion-item>\n                </ion-list>\n                <button id="sendButton" ion-button color="secondary" (click)="sendForm()">Formular absenden</button>\n            </ion-card>\n        </ion-slide>\n    </ion-slides>\n\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/parking-permit/parking-permit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], ParkingPermitPage);
    return ParkingPermitPage;
}());

//# sourceMappingURL=parking-permit.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParkingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_request_promise__ = __webpack_require__(561);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_request_promise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_request_promise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__parking_parkingHouse__ = __webpack_require__(778);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_constants__ = __webpack_require__(779);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__kita_map_kita_map__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the ParkingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ParkingPage = /** @class */ (function () {
    function ParkingPage(navCtrl, navParams) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.icon = "ios-car";
        this.title = "ANAL";
        this.visible = true;
        var promise = new Promise(function (resolve, reject) {
            resolve(__WEBPACK_IMPORTED_MODULE_2_request_promise__(__WEBPACK_IMPORTED_MODULE_4__services_constants__["a" /* PARKING_HOUSE_URL */], { contentType: 'application/json', json: 'true' }));
        });
        promise.then(function (response) {
            var parking = new Array();
            for (var _i = 0, _a = response.features; _i < _a.length; _i++) {
                var entry = _a[_i];
                parking.push(new __WEBPACK_IMPORTED_MODULE_3__parking_parkingHouse__["a" /* parkingHouse */](entry.properties.obs_max, entry.properties.obs_free, entry.properties.park_name, entry.properties.prozent));
            }
            console.log(__WEBPACK_IMPORTED_MODULE_3__parking_parkingHouse__["a" /* parkingHouse */]);
            _this.parkingHouses = parking;
        });
        promise.catch(function (err) {
            // This is never called
        });
        console.log(this.parkingHouses);
    }
    ParkingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ParkingPage');
    };
    ParkingPage.prototype.goToMap = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__kita_map_kita_map__["a" /* KitaMapPage */]);
    };
    ParkingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-parking',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/parking/parking.html"*/'<!--\n  Generated template for the ParkingPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Parken</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n  <button ion-button block (click)="goToMap()"><ion-icon name="ios-map-outline"> </ion-icon>Auf Karte anzeigen </button>\n  <ion-card *ngFor="let parkingHouse of parkingHouses" (click)="goToPage()">\n    <ion-grid>\n      <ion-row>\n        <ion-col col-3>\n          <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAolBMVEUTQ4b////V1dWVlZUAPoVbWlZfXVkAN4CnscgNQIUqUY4APINQapsAM38ALn0AKntqf6jS2OPAyNiFlLUAKXuhrcb4+fsxVZBvg6rPz8+8u7kgSooTRYzy8vIAAAAAMnQAGlWwsLDp6emQkJA9PT2IhoPl6O8AAApnZWEAACiTob6wus91dXVRbJxcdKFle6Y/XpXY3efGzdygn5x7jbGNnLpPmSY9AAAHqUlEQVR4nO2d0WKiOhRFiTIipWitiAVFpUK11U470+n//9oFrDd2TDCQBI6ZrJfaB5HFJgcSEzSQ6hin/3TirgrEHbJh0EvWYdC5foJwnfSCM8PHJI0knipNE6XJ49fLL8M4UckvJ0riw4uDYXdf/PHDdPvj2tmmoV/Y7LvYMC4Eo/1mtXTMa8dZrjb74ozcx0fDxyR/FS6W87mhAvP5chHmSkVbzA2LNvjxrIbegfnzR9EWD4ZBmieolGCumKeYBoVhL4swWqglmCkucq1eYZhHuV+2vUfCWeblM8kNO+vsMrFRLcIsxE120Vh3MsM4O2HDlYKGq1wszgy7WWtM1TtJs9M0q6BBNzfM7sS3Ttu7IwFnm/WWjoY/zLZ3RwLmD2147bRj6AysQVNtvgVD2xo+jNDoYWjZTXxc44bO4KZ/6Lj5/ZsmgmzW8BAfpokgmzTE8WHkB9mY4d/xNRZkQ4ak+BoKsglD2/Io8Z0G6UkKUr5heXzyg5RsSG99jbVIqYas8UkNUp5htfjkBSnLsHp8soKUYlg3PilBSjB0BrPa8Z0GORMTpGhD/vgwYoIUaygmPoyIIAUaiowPwx2kMEPR8WE4gxRjKCc+DE+QIgx5rn2s1L9GchvWis+fvE8qH5OaQXIa1opvdDe0TGt4V/3A1AmSx7BmfMe6kdWmJoKsb1g7PryDdhNB1jRk6baf79vkvOzXDLLCgEAtw5rxkXcrO1hSg6xuyNn6yEdMYousaiig9VEOm6wgKxlKiO/02MkJsoKhpPi+HUAJQbIaSo0PIyFINsNaHYcq8WFqBkk/lAyGDcWHERvkRUOzXnx8Y/Q1r5GzAUnggqH51mh8mHpBvhEMLhhaL1U/hDM+TJ0gX6zKhoZV5QPExIepHOS54GVD95558/WKZznVSuu9W8PQvGXbuOj4MBWCvK1RaQzDe2LYtIz4MKxBPnmEN182tD4vbVhefBimID8JzZDlin+h1ogrnuUwBEkSZDEsqzVNxIe5ECSpzjAZ0mtNU/FhSq+RpDrDdudNrjXNxoehBkmsM2yGpFojt3iWQwmSWGcYe09/15q24sOQgiQLshl+rzVtxoc5K63kOsNoeFJr2o8P8z1Icp1h7eMfaw2M+DAnQVLqDKthUWsgxYc5BkmpM8wjUVYL1z5WDqWVJshqaJuX47OrLfK0c+pa/YUzMKmbEjdTwZ7eVuP9/cYYDAeuZTkyTw5xhtaOdKNxCf9ptHu5m3mutCYg0LBfx/AoOprcSirTQAwLy/6UNBrICyDDjNHrEPAMWhGGmeP7QJjbAWiGCO0ssbcV8AyRfzcUpmeANMy6CSIrDkhDFE2pN2GKGCL0QOntqWOIPkXVVLCG6EWQIlxD9CnmRAVsiN6ElBvIhmgq4hYOtOGTiKYI2hDtBCjCNqQOEcIx/ByX8TnZjcq/FIxoY4RgDGeWU4Jlue5w9qdspgB1kBCK4c3lamg7rjumP+pPAcPivR71q/QJryIMw+zdxi9JIUIxNGyPMhpJmOZUCTCGhuFNiIa85RSQoUFJkfOaCMmQMmFgx9fHAGVo3xBPU76BKVCGhku88+PrYsAytB2S4ZhrABWWIXn+FW0OAhvADM2fhI3wdROBGRpD0mmqlKFLmrU24yk10AyJW/nJs1/QDJ0/hK088BRTaIbEUsPVDYZmaL8TtsLVvQBnOCVshasXDM6QdGvaV8qQdJYqZUisNEq1Q2dM2IpStdQl9fOVuh4OSd38W4Xu2myDsBGkUv+QuMiKbxgDmCFxLOq3Qj1g4rWCc0wYliF5OPFdnZEoi9R1Qr46o4m2SRJUaUTYI6+7U2dUf0D+Zoa6GIYROIYD0h0p4v+eG4qh7dEe3sA79wuIoWX9pggq8S23bXmUM1RAhG0b2qZjDW9KFmiDn21iesMyBrPX8a7smQ3wZwz5pZSoffEKftYXJ8rP3PNFzBIGbShgZiJswxchE70BG/7mr6OwDYVMgYZs6ItaBg3V0J+JWtwF1DAyhK1eg2n4yxW3WBakYd8TuBoYoGH0KmxlXg48w4ngJevQDHeG0AANYIbRxBZYYr6AYxjtfkp5PgwMw6f78dQTH1+BZMP7C+z6L+O36XDgynuAi2RDY+CWYlmWQ394jhAgjCbKRRuyow3bQhuyow3bQhuyow3bQhuyow3bQhuyow3bQhuyow3bQhuyow3bQhuyow3bQhuyow3bQhuyow3bQhuyow3bQhuyow3bQhuyow3bQhuyow3bQhuyow3bQhuy8w8Ykp5hpZShcTM9R9RuciDQ0LDPEbSXPIg0hIk2vH7+McMtvB+l5sfZHg0DhNJl27sjgWWKUJAbxiFC4Wre9v4IZ77KxeLMsLNGyN8oaLjxEVp3MkOUZDdZe/VO0+UeFW6ZYS9CKFqoFuJ8kWv1CsMga5EofFZLcf6ctUKUBoUhSvIfzvpQSnH+/JE5RXkDzA0f8xcoXCznakjO58tFniBKHr8MUZw3ShTtN6ulY147znK12Rc/57aP0dEQdQtF5IfptnftbNPw8KStfRdhQxQn9B+xu06iJD68OD6C+TFJVXKM0qIN5uCHTAe9ZB0GnesnCNdJL/jf69tjtDtxVwXizqkU8UHhSvEfFrHeAWCfiFwAAAAASUVORK5CYII=">\n        </ion-col>\n        <ion-col id="text" col-8>\n          <div>\n            <h3>{{parkingHouse.title}}</h3>\n\n            <h3>Freie Plätze: {{parkingHouse.currentSpots}} von {{parkingHouse.maxSpots}}</h3>\n\n          </div>\n        </ion-col>\n        <ion-col col-1 id="trafficlight" [ngClass]="{\'red\': parkingHouse.percent==0, \'orange\': parkingHouse.percent<20&&parkingHouse.percent>0}"></ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/parking/parking.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ParkingPage);
    return ParkingPage;
}());

//# sourceMappingURL=parking.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayGroundsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__kita_map_kita_map__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the PlayGroundsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PlayGroundsPage = /** @class */ (function () {
    function PlayGroundsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PlayGroundsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PlayGroundsPage');
    };
    PlayGroundsPage.prototype.goToMap = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__kita_map_kita_map__["a" /* KitaMapPage */]);
    };
    PlayGroundsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-play-grounds',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/play-grounds/play-grounds.html"*/'<!--\n  Generated template for the PlayGroundsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Spielplätze</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n<button ion-button block (click)="goToMap()"><ion-icon name="ios-map-outline"> </ion-icon>Auf Karte anzeigen </button>\n\n<ion-content>\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/play-grounds/play-grounds.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], PlayGroundsPage);
    return PlayGroundsPage;
}());

//# sourceMappingURL=play-grounds.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PushnotificationsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the PushnotificationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PushnotificationsPage = /** @class */ (function () {
    function PushnotificationsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PushnotificationsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PushnotificationsPage');
    };
    PushnotificationsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-pushnotifications',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/pushnotifications/pushnotifications.html"*/'<!--\n  Generated template for the PushnotificationsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Benachrichtigungen</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-item>\n    <ion-label>News</ion-label>\n    <ion-toggle checked="true"></ion-toggle>\n  </ion-item>\n  <ion-item>\n    <ion-label>Events</ion-label>\n    <ion-toggle checked="false"></ion-toggle>\n  </ion-item>\n  <ion-item>\n    <ion-label>Verkehr</ion-label>\n    <ion-toggle checked="true"></ion-toggle>\n  </ion-item>\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/pushnotifications/pushnotifications.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], PushnotificationsPage);
    return PushnotificationsPage;
}());

//# sourceMappingURL=pushnotifications.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SportAreasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__kita_map_kita_map__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SportAreasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SportAreasPage = /** @class */ (function () {
    function SportAreasPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SportAreasPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SportAreasPage');
    };
    SportAreasPage.prototype.goToMap = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__kita_map_kita_map__["a" /* KitaMapPage */]);
    };
    SportAreasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-sport-areas',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/sport-areas/sport-areas.html"*/'<!--\n  Generated template for the SportAreasPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Sportanlagen</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n  <button ion-button block (click)="goToMap()"><ion-icon name="ios-map-outline"> </ion-icon>Auf Karte anzeigen </button>\n\n<ion-content>\n\n  <ion-card (click)="goToPage()">\n    <ion-grid>\n      <ion-row >\n        <ion-col col-3>\n          <img src="https://www.freiburg.de/pb/site/Freiburg/data/904433/Lde/resize/skaten_landwasser.jpg?f=%2Fpb%2Fsite%2FFreiburg%2Fget%2Fparams_E-815263777%2F981853%2Fskaten_landwasser.jpg&w=925&h=271&i=0&m=F">\n        </ion-col>\n        <ion-col col-9>\n          <h2>Skateanlagen</h2>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <ion-card (click)="goToPage()">\n    <ion-grid>\n      <ion-row >\n        <ion-col col-3>\n          <img src="https://www.giengen.de/ceasy/modules/core/resources/main.php?view=publish&item=resource&id=824&doCrop=1&width=664&height=285">\n        </ion-col>\n        <ion-col col-9>\n          <h2>Bolzplätze</h2>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <ion-card (click)="goToPage()">\n    <ion-grid>\n      <ion-row >\n        <ion-col col-3>\n          <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxIREhUSExIWFhUXFRYVFxgXFRcVFRUXFRUWFhUVFRUYHSggGBolHRYVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGy0lHSUtLS0tLS0vLTUtLSstLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIALQBGAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAAAQIDBAYFBwj/xABCEAABAwIDBAcFBQYGAgMAAAABAAIRAyEEEjEFQVFhBhMicYGRoTJCscHRFFJikuEHFUNTovAWIzNygrJzk6PS8f/EABkBAAMBAQEAAAAAAAAAAAAAAAABAgMEBf/EACkRAAICAgEDAwMFAQAAAAAAAAABAhEDEiETMUEEUaEiQlIjMmFxkbH/2gAMAwEAAhEDEQA/APGa51y3vcxpzBmwUFGi53sgk8hyk+gPkn9Q6T2T42j5K1h6+RhiGmIOvaaZnQanS50BG9LsBUom/wAjF+ITnMGuo7t/A8NUtQ2JaCLi2touZ3XHqoy86DhfTnw5FAEY70gTgLTHqPghgJMDVMCxSdl1Hdv8SOKvNabBx3jfAP4r9/qqrqbmdki+/lfThO6O5dHBUM5FiQ0QbhsWkgGeHDks2nJ0hN13L1PBupBoOXQ2Bzb95FpuNFIAilTDQGiwEx43T4Xreng4QSl3PPyyUpNoaAlAToSgLYzGgJwCeAlhADA1OATgEoCAsSEoCdCWEhjQE6EoCWEAIAnAIhLCQwhOARCWEBYoTgEBOCB2KEsICcAgAC4XSmnUIYWi15cCZG8Agbl3gnQpnHaNFwlq7MFTxIoMdlnrHhoJgZQ0gy2+h0KvbPa2rSNIlxLpOeJggtga8zv1K0Ttn0mOdVLWC29oDRl97kdbrhbN2iW4jtu7LyQXQSwmDlc37u4dy5NNGlJnRtsm0cPF4N7PablbOUEkGSN44qHDgBwLtNe/kuptfFNrVWgTk9kZWiQJiGxMjuVCvRgyMxpzAdBgrCSp8GqdrkhrAZjlkCbA693ehPfQMB24m2l99kikos4vFurE5gwENAAAy7xHef1VJkyRu1N4mE98gzAv5eCRlLjbv/uUuwC1ahjLER670xrba6nTfP0Vx9MNjrG2O8GXRI0B0sbTCKDGioMnaH4oi9oPnuRdgQMw9x2he15Anvj+4V+jRbmYHHJGYH3gDlvu3kRylVn4c5iDDRmNpJIPu8zM+iWnXdNzEACBymP7KUkxWWWYcOJu4Fuk9/ZvMq+1wpgN9oyO00wzSLjjHwVKu4ZYa09oXOtuETI4cE97oLWyCbGDcjiJ8vILJtuq4oVe52GXEp725WhxIAJy679wKbUqBok6QTygamdImy4+N2gapyi7RprLQDJMeEr0peq+haPk444bk77HahKAii7M0GCJEwbEd6khdyOVjQE4BOhKGp0FjYShqSq8NaXOMACSqOC2r1rgBSdkJgOtqLm36qJTinTKjGTVo6MJYTgEsKqFY0BLCdCWEhpjYSwnQiEirEAToRCWEDABOAQAlCAFCcAkCcEgFCcAkCcAgZDjMK2qwscLHzBGkLKO6M1MwYCAHSXG5awbm83LaAJQs54oz5ZpDI49jN7K2aadR9F7jkkFgFsxDRmNrjdI0PNQ7fxlBkUwxry3QkyGkEFzcu6Ra3yWsDVncd0WDiSwiSZl14Eb59ok+XnOM4SjGo8mkJpytl6nh6OIp03ZGuhvZEG3KJhokIV/A4LqhDT2coGW2u8zrwshaKNrlEbV2PLQ6SDYR37lfw+BNWTnFrcT48Fz2kA6SuxhtqU2MsO1abATx0XJiUW/q7HTkckvp7nKfRLHQ9uhuP14KYta5wDAQNbkeEHd9U/EYrrXSWhvG53Dy/sKbDCWuaIGa17E2BMONgLaKJUn/A7dc9xpcGkdoRqe+Lgzrp4d6jNVhAaGnXUEmRuEESlxOGNOKZdvkzoCQDuPcpsBXaLNGhzEmASLRB3EGfNZuXko6+Dw0gPgTByEzaeR0XPBAcQ7QSJbrIPDeOSMZXfLKgs2I0n0OgsLgeO9NqMLmuqXaMwgDSYLpII5ce4cLlKLilRlGLtux2Nx7DRY2SDlymII1JkibSY8pUWzsAS4OpuBAIBM9ocTHC0hc3rC5xcYJM8BfjwWw2bh2sEB0k3FgLagCN0H1T9PiWyXgeaesXRZATgE8NTsq9k8sYAkeQ0EkwBcngpcqzPSXGuc8UGzun8ROg7rrPLk0jZeLHvKhtWu/GVOrZ2aYuTy4n5BdnAbKp0btzE31MxMTAFtw8k/ZGzxRphvvG7jz4dwV6FGPH90u5eTJ9sew2EoCWEoC2MRAEoCcAlhFDsbCIT4SQpGhsJYSwlAQVYgCcAgJQEh2EJwCAngIGACcAgBOASGgATgEAJ4CABoTgEBOASGAQnAIQM8dHz8VN1YMQSZ3HdrqfBRZjpuN+F9ydTMe7Olt08xvXlHoE+bNDCQGiTcazf6WUtbENbMeFhExBzT4+SrF4JvYXsB325KMgR8PnKVWInpNdUDjJJHaOmg4kkf3Chp1CHTF+Y8NE5jLyNAdYskLCCQbb+E8IlFDJKmLJjc64nWQeM77lSM2g+HMBs8yeZGhO6U52CLW5tx0MRMzoTbnGq6NLZ9M2DS5xiBIjSw1nWylyjwhduShs5lIvGYybQIJDnSbEndprzWq2XBpgz2jOYcO0bDksvQwbmvc33myRldGY6COf8Ae5anZGGIYC4DSxuSZ9qTNyFvgn+rFJGGdfQ+S4Gpwan5UsL1zzSPKsj0spltZrxaWi/MEj6LZ5VlOmre1TPJ3oR9Vz+qX6bN/Sv9RHcweMa6iKpIjLLuRA7XrK49LpOXO/0TlubO7UcYiFxcDj8lKrSMw9tuTgR8R8Aq1BrnuDG6uIAA3lc0/UyaWp1Q9PFN7HoeGqh7Wvbo4AjxUoCjwNAspsYYlrQDGkgblPC71dcnA6vgbCWE6EsJiGQiE+EQgYyEQnwiFJSGgJYToToQMaAngJWtU1GgSUikRtan5IV9tLKIbrx1KqFSnZTVDQEoCUBODUxCAJwCUBOASGJCFIAhIZ442kYn1vA3wpOp/FF7iCIj3u/lzVZzzpoOCkoVSOHKTEfVeVyegSfZXSNQCJE7x96262qbWoQbOB3236aAaa6clP8AaDWqEvIFiTIO4bgNT9FbpU6LQ7tHtH/LLS2cpkHNJ7M63G4qoq2Jujn4OiXVWtyycwBBtv38F0tsYe4LmNacxFgW5heDBOhiJ3qPCOpMBqZiXTo0SIlsTcGCd3cpto4plZpe1r5MTmcXBpF4EaC3/wCb71Wv8kNvb+Cq0lxDbgGGgFwEXEAHcirXHZa0nsmBeRO8tE6W+PFNpUalTMQCSL2E7pj9OaShRb7xyuAJ7RgbteBglZKN+CiwajsRVyDS8AiDIF2g6jQ271ssFhyxjWkyQBJ4neYXM6Mizmw1wBJzgiQTbLEcN/eu/lXo+nwxX1+Thz5H+xdiMNS5VIGp2VdZykOVYHpPi+srug2Z2B4a+pPktT0n2sKFPI0/5jhbi0b3cuX6Lz9cPq8n2I7fS4/vYLsbE2y2hAdRa7XtARUvrc691lyabC4gASSYA3knQLf7H6O06dMdYxr6mriRmA5AG1ljghKUrib5pxjH6ilU6VMI7DJPBxy+sEKaht9z2kjDVXRvaJHgYXbp4Km3Sm0dzQPkp8q7VDJfMvg4nPHXEfkpbPxPWsD8pabgtOrSNys5VLlRlWqTS5MnTdoiyohS5UZUwIoRlUuVGVSURhqlpUC7QKVrGxzSieKRSRbw9Gk0doyeSK2JbENEf3vVMJQFGvuXtxwSde6IlRgJ4CUBULuNDU8BKAnZUgGgJ+XS/wCiAE6EhjQEqdlQgo8SypaYmxMd+iACd+qfVolsS2J0N7ryzvB9EgB02PDdySMPs3EA77jnbggHTXl+i7WynU3NNE0pqO9lwAzAzYHkTbxQn7iZ0ujmy8NVYZl7o7QuGiSYFtSIXeweFo0QaTcok3EiSXaT8AsfSwOMogFtOo0vzNgNcC0SNwNtNT+q0PRnYrmjrazBnzSM0lwtZ0k2Myu3DLslHn3OTLHu3Lg6+GwjKbcrGgCSe8nUniVQx+wKdUky4EuDiZJmNwGgtA8F2wxLkXU4Rapo5lKSdplWnQa2wAF5sIudSn5Ffo4QOEzCR+DI0v3J7LsJwfcpBqixlcUqbqjtGgnv4DxMBXjSI3Lm9IcA6th6jG+0QCBxLSHR4xCcnw2hRXKTPMsbiHVXuqOMucZPyA5KspCIN9d4Kma0OEb9y8rXbk9W6RqehOxbfaHjiKYPq/4geK1+RZ3Y3Sym6KdZvVuFpH+ny/2/BahoBEi4OhFwe4r0cKio1E87Ns5XIhyIyKfIjItTKiHKkyqcsSZErCiHKjKpsqQtSsdEWVJlUuVGVBQyEuVSAJYSGMayU7KpGEjSydCRRGGpwanhqdlSsYwBOATw1ODUgojypwCkDFaw+zqj/ZYe/QeaTaXcpJspAIXaZsCqdco8ULPqw9zTpy9j55otcDaZBHfddF2GfVIYGnjZs9qN5Ggv63VJmIcYAnna9l08NjXMEB5ANgQYniOG9eddPk6nZUZgpflLwDMSMzo3bhoLCy1XRGg2niG1iyq43AJuwOIIJNh7seJnuf0brBtQDtakdkWIkQHWsBJK1mGwTWFxaCMxk3keHmt8ONT5TMcmRx4JcRUzmYUGRWciOrXpKkcb5K2RLlVjq0dWnYqI6Ly1WHVxq0Qd/BRdWjIpaTGm0JVql2qiyqbIlyquxL5M9tzovRxPa9ip95o1/wBzd/oV59tfZj8K/I57HawWOnfHaGrTyK9U25jhhqD6p1A7I4uNmjzXkBzVX73Pe7xc5x+JJXH6jVPjudfp9muewr6gI5hdbo/0jq4ZzROanPaYbiDqW8CrG3MI3A0W4cGa1UB9Zw3NB7NNvKQSeOUeDugOyBiMSHvvTpQ9w4kzkHmJ/wCPNZLZSpdzV6uNvsew4TC0KoiMro5wm4jo7UB7MOHHRWsMxg0InmbhdLDVaoiAHAa3i3zWsskovh/6ZLHGS5X+HCp7Aqe9bkrdPo80Xe4jkPqtE6uOCZWAIWTzzZosEEZbaOHoMENAnvJP0XGLJ5LWY+jIs30Wdr0CDoV04p2jnywplPIjIrVOiXWAlTfYKn3D5LVySM1FsoBicKa7ey6LWSX0zMWLmyAeSixNUOdJF7xFu625R1OaSL04s5YYnBqlyqOtVawS5waOLiAPVVZNBCe1s7lT/elH3XF//ja6p/0BVyhtGqbU8PUiPacadMers39KlyRaizqYPZJdd9h5LrUtnYcc+8lcCgMU8Xfh6XCXPrkd4hgnxVkUX2nGPPHqqVJgPjUDyPArmnKTN4xijS0m0x7LB5Ixe0qFETVrUqY/HUaz4lZmthKL5kVqhOvWV6jmf+vNk/pS4XZ1Ol/p0qVEcQ1rZ/KJKy6bfc03Xg6NXpVhgey51Th1VKpUB/5Nbl9UKo6jS1dVJ5Nb8ykV9NfyT1GeAljWuHHuABEjXhoVZbTBcG02kkCSTcS7eAN2vFRVG6bz3CwM2XS2USx7TEgkAgDu09PFcexrRpsD0cxTKfYe0OcQfeECc17WJgTC1rWHfqszgel7GnI/OWiAHZO13u7Vzp5LXNYSAbXvvC78EoL9py5IyfcgyIyKw5p3NH5j9EZD93yI+cLo2M9CDIjIpwD90+bfqpm12b6Z/KfkEnMFApimpPsrvunyVlxYT2A6eGV3zVmltHJ7UeJhS8j8FLH7nKdhyNxTMi737xY7VoPkVyttUC6jV6iW1Sx3VzoHRa+5CyvyhSxrwzzXpnXfjMVTwVGDlN79nOR2iTwa34uWr2V0Zw+EpAkUy5nbdUc0F0gXcCR2QOAWD6HYpuGxoNXsyH03F3uuJ94nS4gnmtH+0TbYYwYZhu8BzzwYfZaP90eXesozVPI+5rKD4guxl/s9TamNcWyGkySfcpts2eZG7iV6bs3ZlLDsDKTA0ACSAJdG9x3nmsh0D23hKFM03uDHucXF7vZcPdGbdA3GNTxW5biqZaHiowtOjswynuMq8FVt5ZGa718D6Vjqrxx7gIHmCuczaLB7Ls3+xjqn/UFNdinHSi884YweIc4O9Fo6b5IVpcHV/ejjcm/co6m0nFc8MrH3abe97nHxAaB6qdmya59qqGg72UwP+5coagvBSc35JhjXb5PiVbp0Q5uao8BvAmB5lLhuj9EgFz6lQ86rmg97aZa0+S6FHZdBhzNoUgfvZG5j3uIkrCeVfajaON/czn0MVhmmabg4j+WDWP8A8YKnq9IsoGWhWfO8tZSHeRVc13oruL2hlEADz+io4PCNr1AHON50vClK+ZdhtpcROTjNr4qtLRSpMadMz3Pd+VrQP6lLsfZdR2Y1al4tkpMHMf6uf4Lu7Q2Q2k9rWTdsk85OhhW9n4XiFUppRqIlFt3I5lfonhnUy5/WucGm/XPYCYtNOmWsI091ZyjsOkwlzaVNp3uDW5iebtSt1tnEOZTIayQeySWugAgiQVlnYV8Zi05ZiYtKrC3VsnLV0ip1HMeabkVgsSZF0WY0RBivYTZ2YSXBo56+Srhqe1xCmTfgaryXjspouao8v1UD6NEe853dYKABOyqKfll2vCB5boGRzkkpV1dn4aiWy4Eum1/juhCh5UuCljb5Pnk0bunSbg6ibieNl0sPsyqB1lMVC1zg1pywCLeyBrf4KPpLss4eq2mYLy1pPbkEwMxM6DcNNNOPR6O7UrCmwNe7OxwEZj1ZYA5wDhpESbceS49Un9RtfFo2GwdgFozYgCo6BlzAGLkyWkQHC2i0ORN2fVfUbmfTyGSImbcZ4K1lXZBpLg55W3yQZEuRT5UZFe4tSDIjIrIpoNJG6DRldoI0snOe4iCTCmDYSjuS2Q9WVmYcHUDyCmbsxh3D8oUgYOEJ2SN5UOZajRl9tdAMPXqdaczSR2shADjucQQb/FT1+imEdAdhmGBAsbAaAcrlaIA8UkO4AoT/AKBr+zCY/wDZ1g3+wH0j+F0jyfPxXe2Xselh6badNgAaIkgZjvJcYuZXbc2dQmNaJVKUVykQ03w2QjDk6X7kHDHeF0WNZIkmPVMxAaNHE/33pdVj6aKTMMToFYbhnAXv33T2ujQqfPIgkpSyNlRgii2oWmx9E59QEXLie+Arz8CQM2SQNbhUzHD1QmmS00VC1XNm1zTdm9PioiEpcVU3aoUOGanC44VImOEcFPVcxrgd0XjisxQxMcfBS1ceSNFyuDOlTRpqWLDrC/8AekKrtmi0UXAN8gTeZXCp45w5K/8AvYkXI8kJNMNk0Z51NMLF1toYltTRt+PFUci6lO0czjTK+RPbRUwpoyo2FRE6iQYKuHZpIlrgbTw8FXKmpYktEBRJy8FxS8kbHFtiIQmuJKFNF7GexeycLjKnWOEMZTzOfYGXtENM8GtJ8F09nbNwroextMuN7OBDODQ0GLCBPJeU18S+oSXvDoBMuJEiAdd8280mGxtWg13UugOYJAIu18WuPxAW4rjWX6raNtVR7rhKTRwJ9Aq9GpmMmIJJ0HszY+V/FeObN6R4jC0ntpNAL3XOUvdLYaQ28e8N29UMB0nxNGt1hc4uMyCXBrpAaezppaN1le98hSXB7fXLQC6bCXaQIiVzaO0s1F9bLlygwCZkwCPiFgMB08q1XPZiAOqeHD/LaAWTuk6iLKhiOlb5ZRY4ZG9oi/aM5u1Oot6LTrcGbxnrdB0sZJEx4k/VS5SvP8P03HVw4Frg2zmkG8mYa4EAXAvOiXYvTtwtWZnbc5ge3fQRAB3cNeSlZ4+RvG/BviENaToJWW2R05w1ckPiiQM3bcA3XSeOnqtTgNo0zOWrTMakPaddJgrV5E1aIUHdACUOfFzA77KKrtLDyR1rMwaXlocC6AJMN1Nl4n0i6QVMVWdNRxZnd1Y9luSTl7PGI1vqk5oerPbMVjqdNnWPcA3jNr6LPD9oGEztZDwD78CBrq0Eu1tovNsHiKrab6eYvpuHapZiM2W4ymOyZAuNQIXFw7nOIgWE7zAPyWXV27F6+59E4bFMqNDmkOa4SCDYhTCF5n0b6VjCYctrZngRkAAEDfdbno50hw+Kpl/slvtNLhIFoM8LqlkTQas6Lu9Rli5mN6UYCm7IcS2QYIEvgzBBLQQFAzpfgC7L9oAuROV2UZeJI37u5XuvchxZ2QxTBxAgLL4rp5gmPczM95b9xsh3cTA8dFSb+0rCmoGGnVa0mMzgLAxBygkka6cEOcQ1Zu6eKcLT5qN1Xk3yXIPTHZ/Vl32hhAGlw+24NImbfDiqeH6ZYUs6zNBDo6sub1kZozZZ03+BSUojaZ3SOSRrJTKXSDDuGYVKR0n/ADGGCdxg2K5bOnWCdV6oVWg8SC1mptmMCbdyrqE6HbfTjckpujcPESuBtDp1gmODTUDyTcsGbL3kfKVzekXT3DUaZFCo2pVcIaBdrdJLvkN6W6HrzwbVzmkaX4iyjIHFeVbN/aVVD2msA+nlh4a1odm7UFptAPZsZ0Uu3en1V1Vv2WpkpQ0kuYMwkdprmusYj1N0t0g1bPT8on6KQ02biR33+C8i2h+03EOaWU2NYTo+CXAbxlmJ1vz03qXoz+0N1Mu+1Pe9hu2GDMHHWHSAG/hjfra73DQ9SITSF5Ntnp7UdiRUoVKjaYtkc0AEWkOEmbzc8V1cJ+04Bh62jmdfKWENabWDgSSLzpNk+oLQ9DQwDevN6H7SSMznMYQXQ1ubK4NneYO7io3ftRJcD1DGtBktzEucOAdADTv0Okc1PUseh6lRe0gtd4foheXVv2p3OTDdxNTu1Ab37+CEmyqKuW8kDh7I0EQNOQ8k402m5DT3safkst/iOp9xvmUv+JKn3W+q8/oZTSzUOosdqGn/AIt70DBsscjezpYW008h5LLHpLW4M8j9UDpNX/B5H6o6GX3+RWaX90UfuC/AuHwKV2xaBM5AD+EuHK91mT0nr/g/L+qZ/iPEfeb+UJ9HN7/IWjTnYlH8XnPxTf3JT3PeNNC3d3tWYd0hxP8AM8mt+iYdu4g/xT5AfJUsOXyws0zujtM/xH/0/wD1U1LYmTtMrPbaOE6bx4LIu21iD/Gd5pp2tXP8V/5in0cv5CNRU6PEiOu4yS0EmeN1Uf0UIHZeC7dIi/ms+dpVj/Ff+Y/VKNp1/wCdU/O76q1jyr7vgDUUtiVGmQ8Tz08gVGzo09r5D2ltzmJIdJuZbos8drYjTrqn5ykdtauda1T85S6WX3QWbmnstsAOuYI7NhcAH4Tfeo6mxWFrmgvAdqARujlxCw52lWP8ap/7HfVL+86382p+dyjoZPyHZqW9F2C2d/8AT9EV+jrCL1HcJtzWROKedXu/MVG95OpJ7zKvo5Pz+BGuGwabTPXGf+I+aYdm0dPtI/MyRyF1kkJ9GXmXwgNTU2Zh9+IHm34pBgMONMRbSxbHwWXQn0Zfl/wRqnbOokAdc4xwfTHqVH9gwzLl58alP5FZlLZHRl+QzTNw2H3GT/5h3JK2Do7w3xqnzsLLM2Qn0X+TA77sHSBgdSO+o8+cGFEcDSmz6Y/5Ej1XFSJ9OX5Adk7Pb/NpAcyEx2Fp/wA+n6rkoT0l7gdI0KQk9c3wa4lMLKP8w+DD9VQQno/d/Ai040t2Y84HwTJZxdHcPqoEK6GTZmfi9PohQoRQCJUITAEIQgAQhCABCEIECEIQAIQhAAhCEDBCEIECEISAEIQgAQhCABCEJgCEIQMEIQgAQhCABCEIAEiEIAEIQgD/2Q==">\n        </ion-col>\n        <ion-col col-9>\n          <h2>Slacklines</h2>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <ion-card (click)="goToPage()">\n    <ion-grid>\n      <ion-row >\n        <ion-col col-3>\n          <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUSEhMWFhUXFhYWFxcYGB0YGBYYFxUWFxUYGBcYHiggGholGxcXITEiJSkrLi4vGR8zODMsNygtLisBCgoKDg0OGxAQGy0lHyUtLS0tLS0rLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0rLS0tLf/AABEIAMMBAgMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAEAAEDBQYCBwj/xABFEAACAQMDAQQGBwYEBQMFAAABAhEAAyEEEjFBBSJRYQYTMnGBkRRCUqGxwdEjU2KS4fAHFTNyY4KiwtIWQ/EXc7Kz4v/EABoBAAMBAQEBAAAAAAAAAAAAAAABAgMEBQb/xAAqEQACAgEEAQMDBAMAAAAAAAAAAQIRAxIhMVETBBRBImGhM3GBkUJSsf/aAAwDAQACEQMRAD8A871FoCB3v4QCIHM46UX2fofWQreyZg+0wxwBMR/eeKfsm9ZvetkpZULIlu9u6beSR5dJqS3qE9VdcI+8AHekFQ3n9kHxzzXLpkM47StOqhNrdwgEESrR9bPTyrXaXX3nCLbUrta3uKSdjOJAjqsjqDzziqezrHu2wwVDGHQz3lYQ0EcmCc1Pp2uadD+1IXcMjqQSwPOIAPQ1ODI1sxyQN6X6p7VwH153XLTC5tON6soHTPdgY+ya67K1BOmvOXk+qK3CwJG0xtUDq2ceH3Gt7cRXIOY2ncxIiZUkgHqcjFWPovrtJb3oSysyHbuII7w7oIj2via3jKyaLTsvTvdtgaZPVNs/ZyTLAESJIgGZM8116bXX0umCtcZtVfAUhD3dohXaI4Ihfj5TWn1Pa2l0WnF5x3zbX2R3m8BPAnzryo+lbXdb9MvKpjuqkmEUcQR4ST5kmt1UY8kU2wPUdmi4u6wH2W1QXHedpumAVSMDvHAnzo7sRmUbCsspLEMRMwRI8OnzrXaZ9b2kHSxYXR6W4265ej9pd4EqCBmAMgfGqj04P0YW7NtSrqp3Pu3MysRlm5k58KjJj224LUtxu0GtINxRXQ7gfN2BkwQeI6kdKAs622CpVtx2i3sHLQ0d73gCouz2FybZDCVDOS4YMI+xyCRAxxFQf5SRd3Kh2AwDyCcGNvxrmnxpkUgf0nUFpNtlY4IInpghgY4oHs/WtaYOJ3zknMqRxPI8oqx1Fwwy3FYGG27fZMGVx0AnzqhuzIj4xWmO9NCZf6ntNVuJet3JYkYIJ2iIO7xz91E9masFpuKrA4YGIjGY56CszZWSJMffx41ZKgKjYpUkjdnAzggnIH3VEsaqh2anQ6ZDccG4RJJVUbuqsSvtcZk1jb+vc3ZZzdIMAz4HpHnVh2hbFlSbbt31giBHeBmGByZqs7P0qO2W2AQZ8gcgHoYpY4KNyYN2R+veYBkt4cgk8Vbp2VeYKWW2ikHJgbRjkcg4++qce2QvM4PHxqzXWHYO8SweYPEePzmtJ38Aizv9mNYUPYfduMHIAEmFgznp5VT9qgoFkSzFySQRB3DzgiB95rSWLlu8otFtinaVIiYAHdJEAwaD9JexbhKsuRt2oCZOJkk+fNY451KpDZQ2bdx1ZpIXMwCR3QCMDpkfnS0rKTAhSYAfJjnoPGfup9N6xm7kiRDLbaCV4IyYk+Bq+9HezBd3aYoyuzxbZlMooBZjxHIE9ciK60kQN6OWnm5ulvUBjjkRMY6gk/CtN2JqCjo2VZhuPkOIBHIkT7jVWdBYsjYrM73AC7sCBO4gqFiQfefCrN7Nw6gKLgiB3fAcglvL41z5ElxyUmekaa8HXcuRx4ceVSRUXZtgJbVQZxzETOeKK2V6cG9Ks5nyQkU22ptgpQKqxUDlaYrRBNcmix0QbaVSxSosKPnXV6RRBRgeSYOekY6fCcVrPQe5eDXbavBNlwLeCbjSOpwesVk7WxtzE+rIEgBSQxA9kcxPniiuxtYFupvyJGYJ2zgmMZrz06Z0UeidgejV61ZvXblsCVeLe0q6YgFNszwuINBP+zvW9O9t9r3UywEiQCcdSAfiCJFemdjagXLFtwSZUZPXHPnPM1JqNMsi56sM6SV4BloDETiYAzW7xRdNGWvcwvpX6PC9qrNpXFtjpr5LRKsyvYEMDwCDWUudh/R9TaV23XBsQmIBMScETiY+FaH0n9JN2utrZI3W7d62+6Ch3+qIVTI3THPiOtF9vhVs2dReli9zbvmQpgm2Rt6QGEHjdWeaKa2Lg2UnbGgXVPasWXlhbLOzsfV29oYnHQAZMDmKzvYXombtm/qmZha047sAE3GWGYQR7IHxzWi7Ctm4zWLeLl+4ykj6lpYLH3T/APior1LR9m27dkWFXuAFSDndM7i3iTJn31WKNqx5Hp2Kz0Y7bTVWw1sHaFWCSJ6iCPeDxish/iT2RpLSnUOX9YQFFsPm4SRBO6TCiT8a0npT2xb7Ot2/VWkG942juCAJMkKa8e9K/SW7rLxuEBJgKq5iBGDyT51WSX+JnFNu0HWkO1GKKN6llzsuMBkOoGCuIrrTdokETM75YxGAYiJgcc1VabtbaqC4pfYAqmACqiTtz50Zrwt5WuW1loliTAO3OAWxAHxrjyRV1Wxsgf0p1L712kBM7Y56bp8poLsixcB3gLtdSrAlDKN7Q2k+QPjig7aXL9xFAJZtqKOpJMACvd/8C7caK8DEi+RjIxbTg+FaRjpjQUeLXOx7hbuARP2hx0HWi7PZOokEA4OckyPDA91fU00t1Ox0j5vfsq41tk2tkYHq7mDGYIX3/wB8VfanY963bmAoVeCrKWUESe8oDGWXrX1Jury3/HVv2Nsf8O5/+7TUkkFHhtxs+B/Gkj/OmtICe8CBGP8A5oq1cBXYqCTEmJJjz6VTJLG3aRhi4xEYleGmSMfGtN2ho99hVBIZfYyfa4kmOPKqLse0lop6xx3i6uoz3YHJFanTsCCEcMoJBgTsHIM+BNcWaVPYow94equiVgqpEAxDAESDHjmK1vor2/qVMBRccKFQN3YkSSCBM++sxr9Gbd0sUPqvtFtxP8fMxUnZ+vVdVuRybfQsuQIgyoOSMium21aFQdqDda6Z7pG59kkMwY+0eMZn4VsOxvRoNcS5duKq7RCgySRk5IwDIxWPsqup1jXHYkGMMPaA7oAHw4rXW3W2AAQQDAG09eYkwOeQelZucVPcpQbR6D9ItgDvCJCj38ACporHabXAxuLeUf3jjpVlb7fChR6tiBgmc/AH8yK64+rg+SfaZHukX8U0UNpu0rVwSrjHIOCPeDUh1dv7a/MVv5Iv5MvFPimSRTEUNqu0EUEggmMAZmo27VthZnPh1pPLDtD8OTphkU1B/wCb2vE/KlS82PsPBk/1Z55q/wDDm8XQF7fq1RtziULGcKQSfnHFZ/tL0fuaV2a229PWbFgbiRyCYEV6FqdY7mWYny6fKoA9eNHNJcns+zi1uR+jmr1ljZv1G9OTaa3kDqFbBU8eIrTv6R4/0v8Aq/pWdBpF6pepyrayvZ4flFB272jbGptXGs2wqblg7m3hh9lwZIMd4Zoftjtotp1t2yvqzcF1DLFkIloMmIBx8ag9NUH7M7T7SlmAxzADN+VB9raUI6W1I9lZg9XYgeWADnwit1kk0m2cc8Ki2kuDUeh7tZHroX1l0cxPdJJxPE4J+FXus9KbiA77ir4YEmeABEzNYvtPt/1b7LYU7AImQIAE5BzRVrSWtWnrHLlsAN7IEGe50ifeay15Fu20jojDE9opN/ci1d/V65/VaphttsWWBK7ojMCeGEUNf9E9Mlsl2beJ76kwDOAFHNaHs/SrZQW0kgE881LrrJe2yhipI9ocj3VLzzb5LXpcaV1ueP3T3jBO2Y+FaXtzs1EsWHsAMSu4nlnGDPqxiBPJqu0PYt1rhVluhdzAlVySDg7WjE1urnYK3LaW7jncoIBUBDBOcQR06VtlzRi1bOLFBNSTMn2BdFoB2D+qaOQo3HdGH8ueRxXsf+DyKNLeCDav0gwJmB6q11PNePdv9h3EcWraOUAkPJHJMhj7Jjxjwr2L/BrTMmiuIw7y32B68W7Q5rSDUt0ycjpaei2tadr2v1G67eC2l05RUuMqSyuWlBhpgcitLWQu3bljV6hmDE3ChB7yLsUbVG5VMxuj3g1Uab0/v3Sy2NJcLKSM3N05xC7M5xV0ZHosV5V/jg0rbEj/AEn58Tdtf+Nbr0d1F67dv3Ltq7bUiyqLcXblUb1hUTxuPNYD/GnTNde3bUgEouSYGbv9KOAPIFuFzF12CqAqzmAOFFR37BVjtJI6Hj+xVjZ7B1OAAoIkRuHXy8fOlr9HqNoR3tiQDG4TAJQcDPFJSTezJo57IA7xdhtUYLdG6R/fStj2WitaJtsMyZHO7OCeeeKymj7MuWwe/bIOHBOIgNBET8Zq90F23YChmQjk7ZzEkHPx5HQVhmWrgZXdo6pA1xUWWIAPdMGYyBOBz086r7zW9ytaUSQu4R3ZiMDn3+dFa21b9c723lSQepySJ3GBjcatOx7am6bpCggCEWQIgEGesflV1pRcd9g7S+j7KReuSWCYFtQDzgccgfdSs3XVlJO4bwScBgGA5UQCMiccCtMt4FSZHnHT41T6y2gZpfd3tsIokQSDukHnHyrBW+TpyxjFJwLiwwE5xP48CpCQRiqi3qdhIAMGWgDGY4Jnz4of6UA31oLbccdMiOmRmsnF/Br7tRpUXZE80lYjESPgD/X7q5sKYyZPjXZFFHUp2rJEdTxz4dflSih3SAccCevQV0dwkDMRzz55FDi+Stasmimof6UfsP8Ay/1pUqYakCDtJSYH3UUl9fEV5/b7UdWyMHgfWGOOcVCO0tQPZuAiZkpJ++t/Cuzgh6x/O56Qt9fEUyapS6iZkivNbmt1LH/Uj3IP0ojsjUaj19mbpI9bbkbVGNwkYFVHDG+Speqk1VGz7TRXXbuAyORIJB86CtvbLDcJLYUkZGzEfnVW9pi7BmcgOTgjChiePlUXbKFrh293YDA3QQSTk+eap4k9kzmlkcg4dkCCGYyWmQoHlljyMDFW9rULaQJ3jHHU+6ayd3dI77Yie/AkDMAdKjs2HYqNzHj67Gq02vqZUcrjwjb6HXowYwRAJIkTgihb/agJgAjzkYnANVOiuhTcKbdyI0QD3QIkE/WJHMeHnVV2jYdoB8ELYMk9MgcVk4qQ/cT+DXHUgTmSIkGMEdKJOunuiCcYnOeoHurK27qyCFLXJIYxiIAE+eBn3+NTl/Vt6xlljESJ2ssoI8AMnzgVzPEmyNTNMHVgQxBmZB8jH9iq76LaLmTBLNuhnALD3HniaiGsN1F2kKTzjET1MYM0PrbxZBthR3hjqVAJHP39Y8qMcWuNiXwFDSJzkggggsXECZPePBqAoj4tsCeB7QAJ4yDmDTaDv21k7QbcBxmG27j5ETP31UaO61q4EZyST7IAgckNiuvBJ003uiUQW+0iXg7faOJfiBjx5PNGDW7A8beR3id4gHAPh7VG6jsdzcN5B3Sd0QIzG8T75+dcL6POdwtyNzAmIAPemPDFLLkjq0thpkDWIuOXcqNsLKnkjg49/uqP0kglHDASIyoJOSZGPOuNf2LdtoDcVh3yBEfZmZnjFHt2XNtTuKttgnkGGPTxyc1MdMZpp7Eyi1yVCawuCN24sAPZEcJ0+fzrrtH1sAtvhcE7BAEuAJBoPU2rluJ3meIODAA6e6kuqdiVYXCWPuzJAjoK7K+USSafXNuUrukcyuGO5TkT4VeJ2goXeqxyHWMiFJx5Yqk0gZTBDEeLZGNswQMeNXNtlQ+rZWWYDEGc7TJnwxNRkasA3T6wsgSbkFoJHQhiR8DGaddT37ntMfWP7sMxHSuOyzt7x3d9lxuwDv8ADpzUfbOjbd6xUwCxw4G7vTwJkAE/GslXBW736G1+qhztJgwcHAI2kxjnNTaJpTd3xIEQYbgY4warbCL3RtJBA2knBjZgxiTEY5pr0oRCYkydw85AHQ/pTcfhE27suv8AOAqg+ruR0gE5zP4HNRt2/wD8O7ifqnz/AErrT2lwIAiSZI6nOOv9aLOkTBZlWcgM4Ux7qy8iW1HXCU5Lkr27b3d31dzOPZI5kczU+u7WIZh6t/abhfAkePlU7WrfCvbY+AcE/dRC6MXCxV0Yg52tMTPMUPKq4K+pvkqD2437q7/L/Wnq4/yY+XzpVPnh0Xpn2eVXLjEz161pOxOzVfT32Jnak8QVYEfHiapdCq8sCIYYz3gATmM9K23ZVpPU3EtkFrqkST3ZIHLETH6eNbZZ0qOPElZmeztKgv25MjfbkEmCN2cda9G1tzR22CRaV8RAAYEnukY8ayY9HL07ptd2DC3DuhcnbK8+B4ms3qe0Ge+hd2YK6qpIg7QwiR40nFZZJp8Fubgqo0t+7+0uBiCfXAAAZ2hiYx5CuVupvdmAyS0bZaPMeQrjUo30i97X+o31vfVYl5pPtYETvH6Vs4XaM1JpJmvT0h0KgDaZ/wDtj481XaztK0xLqhCkCAFGfDAxVE2idwSqk5bAbPOB5TRFtIcKAFkA94yZAPtdMHnjFY+KK4ZbyNoM7K1DE3G2yAjd0qAcISTHU4HyqB+2Zae/BVUAImSRM4PHSPOu9BqA90wIX1V3G4GDsec8n3+dUJuiFG0QCv1xkwZJrRYk1uZ6mW2h7VZTBDkEDdgSfDr7uKJs6tbxIuSwndOQcfVI5zjIn4Vnie+uBBj64gzXdqy252gwBBYNHkcjA60pYo8j1svNddu2QtxTuBgbDDcyRJHPTNG2NSLvdCsCm0kT1KvwOmP7xQmjtJ6i3vjeyKyk52AMdvEyYnPjRnYultqbgmbhJZvEAK23HSZOfOsI1/NjcmLsp9llQ65ADRyRJg4OJgic9J61RXrzblcl+pwcfWx8iKsdBqx6mTkercTMr32BAJ+6SennFVuk0DXYCqsQTO4x1nrzWuGOmUm+yTUdmdplrAYZIywJg45n7q5X0rVRtFrynd/F5j3VT+iSkG6oAgA4yRJtxn5Cpn9G1OQRBzx4sD41OTHj1fUbRcq2D+2vSnfbdAgBgd6Z5B4xjioLWtTub2UHaCTmBMRgjM/lUNzsNWWDt3BQPfAYCc+dTXuzwSjECVVVEyAQIkGDkRPNZpY9kiMjbe5R9o/tCqKCVHBYhcsM9PGKivdnbYaBEnIcYM8T8a1lv0Ms3FVt5AYSQBHIPGcVXa3slLYKIZAZQe7tUGR3uckzz5VusyTpMTxtKys0jhAo2hZExuGe6gABmZwfkfGu943CUCMWaO8BHfYkwTAEeMnNSajRbip2qNoECTj3cT4Vzc0NwuHRViJ3EHBzMrOeY4p60Zh+kssLiMRA3oTumGllCkQYyfdQt27cRYIETIgoFWWBMENPWjey9q7bV5IbeoXccmCpGw8EAjPSgLNiwAUIJONp4zCzgxOIp2qEB6a/PdIUjH1szHPteRoybTbgqKIJ+tu7vfAgzg/pUDWEUm4NoQDIYEGRuBCxziD8aJ0VwtOyEHlLCZbPAPjinKqsAjQFrtxECjapLGDJHPUHPTmiPTPQsXtFEnuZ5+3Pj5mpuziyjbbyzEESCAADlmEQY8Ku93dh23DgnceY8omfkK5pZNM7NoL6aMf2b2ayGy2yGLpux7PfUGTPBFXXoGvcvNAzcAx/tn860JZNoUN1TAYke2JxNT6bS27c7FChjJjqYiflU5Myljrs3xY2pWdRTVNtHjT1yHRaMJo2T2LbK0AbRtA8Pn/c0n1yo3qjbCkTGIHByPh94ri6wRiQoG7w4AGD4wZ99TJtZleGHA/Hr0rr/wCHmq0RnVuxf1nKrtVu7wRkSR3ZqpudnAEN6uYIz3e8Z59/6Veai+wjaAR74n4HzNPp9UDMwSDBE8ZIyY45qo5ZR4RLbY2rCK15+57bHIHhHNVdjTJtDMECmMhVM56Yq47QSJacFvsg8c1XWojvJj3DMA4j3V0ubqwZJd1lv6rgAYmMSAJB6A+FJdQj3iV2sAPLGInI8fM0E6krKAd4wJAA7o+yOvArvSWSzkqzoYEBUnIA5b7PkSKyXyxpBnZt1S7FSpBR5A2zlSOAviRQensBjtGTjG1eYmPY86Gt6gK4MkxMwsGfODnirHR6pmBYDYJ7sRuAiSTP/wAkDyq5SmhPkDDBbgRuhH2cZ49j86hv9oC2zSYIJxIwZg/U6AmpWuqcDA5nbx99c3PWFvWIwJG1XkCSDgEnwPXzAq39wLq3rm7mQBAAniZwIieDz51X6PXn6V1mG3Z5EZkFcjk89BUHZO/1hWTEZ8QCekj4f1qx0Itm8GGWYHIyMqZj3fnFZY0oyoEwjTQyDaRBByIHtScgL8MUBeuerH7TAM7Ap5PKrkSOvzqDQ3NsAF5CgYAA5HtRyP6UempXazNg5BhR4jgHgyB1mp3T3Gix7EvH1OoILhjbEbuhhvL+4pxr4SWMRzn84qbQXptXCGJ7kjGevTxqnCsQRxug4E5/LkVU7aTNVtsR67tSARuJJxgwPmRFdaDVNtzMDAaeczJ8P6VXkTchyMeK8iJjxmant327qwRJkR4CPDr51GSPyjNtlxZ7QiBvnpgyTgQOK4dVUlVZ2GeRtkwec+yMGa40y7xtVQ3hjGI6jk460dqND6tkS5tK+s3nvGWUgbvdwcdc1m+TSG63Myb12CWwNzGYkwZEHwB/KjbHaIUA5AyIHjAn7+lD6rYXYJBVj3ei96RBJE+OefnUtu0BJAESBEGQZyR1Y5nPFatWZfIOO1rrNidp3QcgzBg56Y6V3Z1bMCVY93jdPeMTER/cV3e1AQg8KMbgJiRx95H51wNYohoHekdDOAc45zVWq2Qju/2mRLeyDjrHU9RUN3XQVJYgTnOfwqX1YuWmHciAYJzJwCM/d7640mgTm6QwETGYM+6fhU6l8gE6PWszkEkycAGcYmehqTV9qk9y2syw58AokbZxnx8DUabQHRRAkhWGRzIzOP6U2hsqrgEKzNBDsxEHgiBxGMedYtopOkG2EIt5DKSwAwR1IMzVqmoSNqyQuJk49/lVdYlmXfdELuBE96QpMgdAK51GsFz9jYfJxAEbjMRj3VShsjRTaOLmrYkn1pEk9TSqpNtxgjIwe6eflT1r4xa5F1a06Fdu4AnnJJgHw6dP7FQsyp3RwOvkOMniarOyrTpJcGW67hjPiDzEVxqfXMCByepK/rUxwtWmRe1HWqvM7tsEBc+JEnJEf3zVbZuO93pBcTnk8R7jnmtD2LowLZ9awVoIAkTng4qPszsl9890wxIMiT4COPOtI0uUNxWxYdvDMYj3kc8e44qk7NvFmIHEZMzu6CZI8a0fa+nuci2X4kCJOM1ldTorgGEcGRA2xHMcVbqVoloKfVACJCzuyMQ05PkPPyobVX1tl/2h3MsbVMqQepZsxg4HlUN60wAUhurZSTkkZ+VQanSnu90zHG38BTUEgIuzJe4QMSGPPghMedHaNWDNu2kZJkjA3xAngAeFD9naa4HBKlRBjueKkT5Vb67s9h3wvtW0BgTkgMx8R7IEVbpiBG03dLITthpAb4gcVWvdh5XdO6B3uQV64E0e1pmBWSFM/VMDjpXdhCgkmIjhSZ6NAODxU8cgRdnXAr7uSZnvcwJgjrmrQahbdxAGG0FsA5Hdbp199cd1dpbasmFIWSGIxI5Boe1dNy+qtDd5vqbcQeh4wTUKnKx0RaC9LBJ3lpgk7T4xzk8cmr3szQhlfJ+0ZgwAYg9OtZzTWV9bCjckHaDMCfDymK2HZ93usRtl12n3bgc+eKzn9O4487k2ltBFcKD7Pz5qgN5hiGE9c5Ejn4xWkS3KXBjj+8VUpphu3tAhRI8yT49P1q09rLfOwAujZjJ3AgAqZA4AMcUxtn1zOeVWADyOPDnrUly6C8L7PXEjzHwA/Gphd75J9kMBMd6CRtBnIGazk9iXyWvYK7SAEAMht3jgfKF+dD+kty5bIG5GtspEABuDMCJI55+FWnZmq23H3FVU7QJPO0Ak56Risl21e33mYFSkyIiAZifH9YrJbys1ltHYHGot7RIACxA6ySNvST1z5V1e1AIzuyDKrOdpJHhHT5VL2bpkYM9xe8CescY/A9ah1eptWr4LqbiAQwJ4HUe/IraLTlRzgOlSLgfbCNiCZ3T0IH51adnm2Q20d0k8nvLIOZ+Hxql7UvW7rlrS9wFjEeyvIHhgVN2fcG3BMzPSIEEiT5VU47A0GXLgtbbYEwcY5WJEH3z86IGpG0mRGSRyeZIiPhNLVafvKQSSwiSCNqqe6pznxmBjxqs7a0LLAxgeIxycxyZms9CkKmaHs3RrcYqrkGMDgSeGMY6/M0Fb0xW4+OWkZBBHEx8/hXHYOvVUSScyD1EE5OfDBijCrjvkSrM0Pxujy6e4VjpabRVbA9u7O4Lg7H6QJI248s1LpdNcKEot32pDooO31eWjMiPGKi0VuJJPjz0ll8es8VrOxtSttFQ5J38+fj0H9a6JzSWw4JN7mHPbt/8AfA+fq1z99KtXcs6aT+zXk/8Aut+VKq8i6L0Ps9B+kWYBKLmMbZifHGKYtYPNu3/KP0rgUttG46Q8afH7O1njAqdE04+pbB8oFQbafb5U7YUgoW7P2Fpjbs/ZHz/rQxTxpLbHgKW4UggWLJ+oPn/Wld0VhgVZAfEEn8JqAWx/YFM1heoFO2OkDt6O6T9wPhTj0d0f7kffUoCjqB8aeF5kfOnbFSBm9GdH+7I+JqMei+j+y38zCjjB6n5mo7hUcsZ/3Gi5BSOLPo9pBwkjnvd7P/NMVJ/kmlJnYs+O1f8AxqEZwA/jyVHvzmpR6zx+G4/pSthpQh2BpREW1x/CuPurr/JdP4D5L+lRMrRB34+y0/mDTrdjlnHv/Uijn4DSiHUdmWlu20UDa87uMxxS1HoppmYsWcE+DY93FK483LcPPOQQYo8hvE1V18EqKKpfQzT9LlwfEfpXf/pCz+9uf9P6VYjd9o/IU5dvH7hStlaUVVz0Lsnm6/xCH8Vof/0HpxxcP8qf+NXpZvH7qQdp6H4UrYaUVFj0K045Zp+An3hYFQ670A0lwy26SIMMVn3gGrhFuCZcGTORx5COldFn/h+R/WlSu6FpRml/wv0YBCvcE49vx94NdL/hnpgZ9Zd4j2xHyC+VaQO3l8j+tdC4fL7/ANaqxaCkt+g9hcBmjzP5xNC6r/D9CQVuADOGG7nwiIrRh26gffS9e3gPmalJL4DSZVP8O1HsugzPsHn+eu73oExEG6sf7WH/AH1ozrHBGBHXJn4YpzrvIfzf0p7dBpMynoEwwLluPNW6GR9bxrp/Qy99u316N1+NaUazy/6q5OtaPZE/7sfhRt0Ggx//ANO3/eD+Z/8AypVrvprfZ/6qVFroegycjozfL+tSBv8AiN9/61CtPNceo+j0ImF1ulw/NqkGof8AfH5v+lDU4o1MXjiErfu/vvvP5iu/pV3979/9Kht2WPCMfcMV16tR7bD/AGrk/E8CjyEOEOl/RP8ATb37wfNanW/qPrOqj+Ir9w5NANqvsKF8+W+Z4+EVCzTyfiaNcg8Kfwv6LO9rLhEQrebbPuANCMbh+on8o/KhppTVKbXyHtodL+iZlufYX4A/rU9q+65FuPcGH50+jVdhKR63diedscoDgtPx8KEu7gTMz58/GaSzNujNYMbbWlFkva1wf+2P+qu/88Yc2x8zVSHPiac3W+0fmaryPsr2uPotv8/P7sfP+ldL6Qfwff8A0qs0qXbjBEJJPn958BRPaGqCgWrbSF9p+S7dYJ4QcDx5qtbq7MpenxXpUfyFjt8c+rPvkV0PSFeqN91Unr28fwpC8f4fiqn8qXll2V7PH1+S7/z5Pst9360l7ct9Q3yH61SNd8l/lUfgKe1uYwqA+4fj4UeVi9ni6/Jef55a/i+X9af/ADuz4n+U1TazYrAKFaFG45jd9aIPFD71+yPmf1oWZtWSvR42vk0J7bs/a+4/pT/5xZj2/ub9KzhuL9gfM0hcTqvyb+lV5WJ+ih9/wai5r7YVW34adpg9MHp+Nc2+07P7yffP6VT6W6jo1rYZHfTvdQO8BjqPwqvYr9lh/wA3/wDNSsz4dEL0ceNzU/5nZOC60419r7a/OsoCng3zH6UxKfxfdV+Vg/RR7Zrfplv7ax7xT/Srf21PxFZGU8W+Q/WmOzxb5D9afkZL9Eu2a1/VnqvzFRHZ0I+f9ay8J9o/y/1rkhejH+X+tHlF7Jd/g1Wxftf9VKspj7X3GlR5R+yXf4HBqRWpxpiMudo8/aPuXmpRrAohVB/idVPyH6k1xuXR6d9HVrTswkDHicD5nFSo1tOe+fCO6PiRJ+Ee+gn1LMZck/l7qVGlvkNLfIRc1TERgL9lRA+XX41FNcU6mqSS4GklwPSmkaamUKaU0iKagDqpV1T8TI8DDfjUDCIn4VzvFJqxNWE+uB5RfhI/Ax91d2UDsFVGLEwAGmT8qDUyYAknjzq4e99FRkEevcQ7D/2lP1B/EetOME/2Mputo8kmov27Km1bJLkRccEfFFPhPJ6xVYSn8X3UIWrkmiTtjhj0/uF7k/i+YpvWp9k/Fv0FCzTTSovSF/SR0RfjJ/Ex91Nc1bkQTjwEAfIUNNNRSGoo73UifOuKVMDqaW6uJpUwJrN4qwYcgyKN7RssSHQEo43L1AP1h8DVYKsuzmD22tEAsJdJ8R7Qx4isp7PUZz23K9se+mmk7T0A8hP5mua1KOppTXE0xNMVHZNNXNNNAHVKuZpUASsaVNTzUGgprpWrmlTAmL0xeo6VA6Oy9MXNcUqAOt1MaQpEUAKkB0pAVc2kXSqHbN8iUXpbB+s38XgKcVZE56dvkbGlXMHUMMeFlT1/3n7qp2M5OTT3XJJLEknJJ5JriiTvgIQrd8j0qaaeaRY00qVNNAD0ppqVAD0qU01AhU9NSoEOa7098oyuOQQajdpzj4YrmaGrFVotO1LVsMHAbbcG4QwjPIjb0qrmrXs9fW2ms/WXv2/+5arTp3HKt/KayxuvpfwZx22ZxSpjSmtixEUqRrkmmIelTU1AB30s9Qh/5B+Qp/pI620+RH4NQwpVlpRWlBPrk62x8Gb8yaXrLf2X+DD8xQ4pqNKDSgqLPjcH8p/SnNq1+8Ye9P0NCmlT0/cK+4UbCdLq/EMPypvow+2n80fiKGpwaKfY6fYT9FboUPudf1pvodz7M+6D+FD1b6TSrZQX7o7x/wBO31J+0fKmoyfyTObiiXS6M2EF10LXT/ppEhf42j8KqtRvYlnDEtkkg5ptRqndi7Mdx56f2KS6u4OHYfE0Ny4QoRkvqfJEaaifp937ZPvg/jSPaD/wn3ov6VNy6LtgtIUV9MPVLZ/5Y/CuTqV/dJ82H/dRcugt9A9KiPX2/wB18nP5zS32fsOP+cf+NGp9Bf2B6aiv2PjcH8p/Sl6qz+8Ye9P0ajV9g1Ahp6K+j2+l4fFWH6030QdLtv5kfiKNaDUgYV3c2n2Z4zOc/Ci7HZdxzFvYxAmFYEx41E2hueAPuZT+BoWSPFk6lfIKRTRRR0F3923wE/hTHR3Rzaf+U/pVao9j1I40uoNt1ccqQf1FWXbe5WFy27bLg3CGMA9RzVY1lhyjD4GrXstPXWXsH2l79v8AMe79axyNJqf8MynSeopKY05kUxNdBoNNNT0xoAempopUASrTilSqSxzSpUqYDGlNPSoAcUppUqALT0bsq19QwkAEwfEDFQds3ma85YzDEDyAMACnpVf+Bgv1n+wFTUqVZnQMaQp6VACpqVKgSGp6VKgoZqVKlTJGFOKVKkBfehP+q/8As/7hVDd5PvP40qVc8P1pfwYx/Ul/ByDmuhcYcEj40qVbtGlEi6y4OLj/AMxrtO1b44uv8/1pUqlxjXBEkiC5cLEsxknJPiajNKlWi4GhqbpSpUxjTT0qVAH/2Q==">\n        </ion-col>\n        <ion-col col-9>\n          <h2>Basketball</h2>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/sport-areas/sport-areas.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], SportAreasPage);
    return SportAreasPage;
}());

//# sourceMappingURL=sport-areas.js.map

/***/ }),

/***/ 209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SportclubsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__kita_map_kita_map__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SportclubsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SportclubsPage = /** @class */ (function () {
    function SportclubsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SportclubsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SportclubsPage');
    };
    SportclubsPage.prototype.goToMap = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__kita_map_kita_map__["a" /* KitaMapPage */]);
    };
    SportclubsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-sportclubs',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/sportclubs/sportclubs.html"*/'<!--\n  Generated template for the SportclubsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>sportclubs</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n<button ion-button block (click)="goToMap()"><ion-icon name="ios-map-outline"> </ion-icon>Auf Karte anzeigen </button>\n\n<ion-content>\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/sportclubs/sportclubs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], SportclubsPage);
    return SportclubsPage;
}());

//# sourceMappingURL=sportclubs.js.map

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TouristAttractionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__attraction_detail_attraction_detail__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__kita_map_kita_map__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the TouristAttractionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TouristAttractionsPage = /** @class */ (function () {
    function TouristAttractionsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TouristAttractionsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TouristAttractionsPage');
    };
    TouristAttractionsPage.prototype.attractionDetail = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__attraction_detail_attraction_detail__["a" /* AttractionDetailPage */]);
    };
    TouristAttractionsPage.prototype.goToMap = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__kita_map_kita_map__["a" /* KitaMapPage */]);
    };
    TouristAttractionsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-tourist-attractions',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/tourist-attractions/tourist-attractions.html"*/'<!--\n  Generated template for the TouristAttractionsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Sehenswürdigkeiten</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <button ion-button block (click)="goToMap()"><ion-icon name="ios-map-outline"> </ion-icon>Auf Karte anzeigen </button>\n  <ion-card (click)="attractionDetail()">\n    <ion-grid>\n      <ion-row>\n        <ion-col col-3>\n          <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEBUQEhIVFRUWFRUWFRUVFRUVFRUVFRUXFhUVFRYYHSggGBolGxYVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGi0dHR0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAN0A5AMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAACAAEEBQYDBwj/xABDEAABAwIDBgMDCQYEBwEAAAABAAIRAyEEEjEFEyJBUWEGcYEykaEHFEJSscHR4fAjM1NikqIVJHKCQ2Oys8LS8Rb/xAAZAQEBAQEBAQAAAAAAAAAAAAAAAQIDBAX/xAAkEQADAAICAgICAwEAAAAAAAAAARECIQMxEkETUSJhBBSBcf/aAAwDAQACEQMRAD8A1YanypwE6+3T5IzWInUkk4Ky6VQ55UoXRR8ZiG0253kASBJIAuQNSrSQ6QnhEEoVoBhKEcJQlAMJQihPCUAQnhFCUKUAwlCMBKEoAhKEcJ4SgCEoRwlCA5wnhHCaEoAhKEcJQgAhKEcJQgAhKEcJQgAhJHCSCAwlCKEoUAMJQiShWgaFU+KcRu8K919WRGWZDwefkriFnPHNSMOG9XfBrSenUhY5Mpi2bwVySNA0ggEc7+9FCr/DlbPhaTuYYGnTVvCdNNFZQqnVTLUcBhPCeE8KkBhPCeE8IUGE8J4TwoIDCUIkkoBhPCdOhQYShElCACEoRwlCEgEJQihKFaIBCUI4TQlAMJQihOlEBypJ4SU2XQMJ4RJJQDlT5E6dSlGFPusZ8o1QgU2jo9x07Ad+vZbNYDx9VnEBpIhrGzPLMXTby8lx53+DOvCrmi9+T+uH4QtJ9io9vo45x/1FabKFgPk1xEPr0eUU3t9Bldz/ANPJb1XidwROVTNjwlCZJdDmFCUIU8IUeEoTJIQeEkyUIB7JrJQkgElCSeFQNCSdJQAwlCKEkoBhKESZKBoShPCUKgFJFCdABCdFlSyrNLAYShGGJ92lEOcLzLxa+ca903BAEa8LWjXTrqvUt2vIdv1A7FVJk8b7dg/LGs8o1Xn/AJD/ABR34F+TO/getu9oMbyqMqM6CxLo06tHT7V6nC8W2XiDTxWHqE3FcAmR7JcMxkGNCdDHde2ZVeB6aHOtpgQnhFCeF3pwgEJ4RQlCUQGEoRwlCUQCEoRQnhKIBCUI4ShKIDCUIoShBAYShFCUKUQCEoRwlCAGE0IoShKWAwlCKEoVogMJIoTJSQeUpTwlChRkyKEoUALjAnsvF6jw6s4mNM0mdC8E8MEEX0yn1Xr21sW2nSeXEA5HRcCdBa45uC8dp4jjJEQBTuBBEFps5t+Q91oXn/kPo9P8ddkXHCKYI+i+3EHRwg2I7nXT3L3DA1t5Sp1Prsa7+poPQdV4jjaT4LXaSwh05psGkzNxw2+EiF6X4W2yz/DsOSSCN3SMRaKtOnzPR7b+einC9scyqRqoTooShemnmgMJQjypZUpYDCUI4TwpSw5wnyo4ShKIBlTwihKFKIDCUIoShKWAwlCKEoSkgwamhOlCmzWgYSRQmhaTMsGEoRQlCUkBhJEkrRBkoRQnhZpYBCeEUJQlLDE/KK4xTaCRwv07vpn19leduDgC4yOFjhDiLh2U9g63MHUeS9G8euGanI0FRsciP2b/ALY58lid6ypTc0C7coJ5EBwv5GT7l4uXL8z28OC8CI+kwuDQeFoDWkuN/aeCQQL3b000vbhgHPqbvDsMEF7pzESYDoMdN2I7rpSrBxzwPbNhYWiYHYOUXDVctTM2xDuE+dj9pUT2aa0e+4QzTYerWn3gLtCjbJM4eietKmf7ApcL1p6PE1sGEoRwlCUQGEoRQnhKWAQnhEmSiDQlCdJKIDCUIoShKIDCaEUJ4SkgEJI4TQlEBhKEUJkog0JQnSVog0JkSSlEFCUIoSDgpTXiDCeE+YJw4KeRfE8+8eU81YDoQOc8TL9tGhYai07qsQYENt1l7YlanxRiSca8FxvVLQOUNa5o19fRZii+KVVpH0J5fRexeXJ3JnrxUxQW1qG7eB3zWBjjpUHc7gcREnXL3VLQdJNuel+q0O2sRLA8TGWkPUUGAz1Ej4LOUdT+uapln0JsK+FoEfwaX/Q1ToVb4WqZsFQcTJ3bQT1LRln4K0XdPRxeOxoTwkklJBQkkklLBJoTpEpRBoTpgU6UQUJQkDIlOlEGhMiSSkgMJQnSSiAwlCdMrSQUJoTpilEFCSaUkAk0Ic6HMoXQcJyAuaZ1SBJNggPItptqHFh9UEPNR1rHLJGYEi1swHoovzSKbyHSd26RlaLGDqPRaLGUC+qXTLTWc/pILwQbi1s2qzmGrOL6rTEbutyHIEAyLrxt1ntSiIm1GEUW2MTqe3D93uVZTbzvMLR7dAOHF7h8Rz0P4lZhr7kH9WC3jtHPPTPbfk+c/wCYUw8iAXZCDMsJzCehBLmx/KtLmWO+TjFB2DFOHSxzwSWnLdxcIdoTDhbVa0Fd10cG9hwnBXMlIFUlO0pSuYehc5IWnUlKFwzJ8yQlOjkLtDeLa9O6AlVfiavlwtQDV4FMebzl+wlUdsm7Kq5qFN05pY0za8gdAFy2xi2090XPy5qzGjW8zayqPA+JO5dScZLC0j/RUaHD+7OPTrKqvlBxIdVZSJ9mm52oEOe4Na4HtlKzdFn5Q38pSq/ZWN31CnV+uxrjpqRcW7ypMrUJTtmTZlylKUhKdC5OCuJKcFIKdUiFzL0syFqDhJc8ydISnOU8qL857JfOey1GZpJlKVFOK7J/nY5/qbJGKYd1D/MVP9dU/wB5F/w7Kpp4Vo3jwDn3defqiWu/NXrnziHnl+0On1nuj4EKE6m4MeCLbutE+b+vaF8+7Z9SaRXeJrUSR9dhNxzH5rHZeK36stj4idNB4H/JNhpNuiyVWmWuLTyJ+8Lph0efk7PVvkud/lHjpXd8adMrYLB/Jbi2CjVpFzQ81pa0kBzhumXaNT7JW7lenHo82XY6eUMpSqQOU0oZTygHSlNKaUAUrHeJcdmrMozYVDIMmTAykCbniK18rzTxSAMbUcSPagayHGnTt+uoWOR6OnEqzns7bXzd+8BMbssdGpMuy87wZue6ibT2wKuIqVi/hzsyw18miKpIPC5ps0ixvexEKpxDOBzcwtDX9nFxMd9dVGL2kEXkwJ7DkuSbh1aVp6B8nO1y5owziSQKj29BTBaB/cXWW6leSeEtoMw9Z1V0uDqFalDReamhudJV7gfEYa+mX53NphzWANaIa6bXPUrpjn9nLLjfpG+lKVmP/wBpS/h1P7fxTHxnT/hP97Vr5MfsnxZ/RqEpWXHjOn/Cf6uaFO2R4hZXqGnlLHZcwBcDmA15WP65IuTFuJh8WSVaLqUpQ5k2ZbOYcpIMwSQFY2uOoRUuN0NBcegkmB2Hmq4U6LZl7XSCADiKILSdHWdcjorbw2KFOuwtrBxcx+YZ2EMtodBPkeSxlyI6LBkXalUUHBr21JIBEMfF7R3VZtHaLDQdMw4AQbG5AFtQrTx7VY40C0tdwO9ksP0raO81n8LkNJ4IlxkNs6fZ5AT9qx562aWH5Gf2fUa+uKNPMBk4jAdJ1ENItEhaXFbFpbtwzvndvBG7pjiLSA0QyevNUGxq+XHPgOnk0AhxsJBBv1W4q4sAAupuvJ0JIvPQryZuM9uCqPOdr1A1jmAjSnTBIaTEAlwIE6Rbv3WcxbctQtJnTSCOuq1fiHDkNeSIl1OJBB9kD7j8FUf4XnxjqZNmABxHb810xaSOWWLycLrwZloE13DMSBkgnhsWm2mlu1+q1h8Sj6rvePxWebSDRAgAcghIWFz5+jq/42D7NCfFA+o73hcneLP+Wf6/yWcP6+1cHmQt/PmZ/rcZpXeLjyp+98/cuZ8XO/hz/vj7lmg5M4p82f2P6/H9GkPi538If1n/ANUI8Wu/hD+s/gs3K4fOIcaZF8wv6T9/wV+XN+zL4eNPo1bvFr/4Q/qP4LP7R2ialY1SyJ5A2vTc2ZjW4PnC5Err85yMBPU6ajWNe4+KnyZZdly4sMdooK5cGvsbuBP9RI81GoGVb4yu0sqAC7XgExEy5w630lV+BpXB1EN0veAt3RxmyywzIClsCimplIEG/b9d1IFTsVzZ6MejqAiXCjXBvfsnfiAHAXv2WTaaJIAUcYlzKu8YeJhlvpqPI3HkU1WtDdCora036396uNWyZNNQ32F2yajWPbEOI537jzF/cu78c76Met1gdm4vI4UzOUvDmwPZdI+FgvQNtPoUKZIqOL9AIpulxsdWiQL8uQXpfMeH4kA3aBA4iJ84Tqs2ntWkx4DSHS0EmQIkm0CnFhCZT5mX4kBgSx7ZIM8zlbEiLSfIfBdX1aLWFrg90tlrgxrctQWEhrgC09Zm+mil4PZOakQ1zmtptOhuTGhMXP4rPbYxRad2JtewuJmGun6UETbXquHlk1o9Hjx4uPstWbYpBommC9mUNcWWDRJAyl5BvqCD5p6G1X4isZYIiOGi2mBlBNnNGp9fgqDcVYBcGhp1JN29LKx8Nhxrtl7TwuhoDr2B525KPJxmljj5KNkTBtB2nUtF7DiEFtMAi99QVd1tqBpq5rspltpJMWza68yB2VO1pG1Xtk3e7S5uNPipmObx4hhHtB1uhDCVlqv/AA0nFr7Kvxa8HMWz7TIB8iF02LhuEFpbmqSSZaJyGDEnQR7yqrEPdiAKmlNhpsvq4kZS7WwtAQ4bFPDd2w8WchnvJDiDpGY+sLcqhz8plTQ0abRULXviC6YIMRqNY/NScRhmjQvNi+DkjhIF4dP0uio8dS3DqTXG4Y4vMES4tImSbmZ8yifSq/Nzi3OaGmWMbcmBIjWAJE87gdVFj7N+b6Lelg2FpeC6IINqdiREE57XPNQ6uzmtORrnSAbOyTY3+kq7ZrKlcmk0iC0OcHEwYBDW8Pn7/JRMVi6orAf8TNAk83dfIuKviZeblJ1HAPJcWt0MEyCJAE80FTCPLmtdYk24m8gSfRDXwz8O0Mc6WF0udoCHRnnnNp7rnhs7/wDNOs1vCzW4m5H6+xJ7FfRNGBfOoPafXouOL2eKbBWyOgGCQWlwblgG4HDI1hTtjUS6aj7Ak2vLr2PkroNOYk3Dm9BcyZE8wRAUsLL2YnB4pj6hBcWti0iTPS3qVc4ilRZpWD2T7cBomJIPS0qu8Q7DNE72mDk1I+oT/wCPdScDQZVo5DBvMSR74Mqmd+yFtEMNN0ESHAECLcR5DuCg2Thf2Jfnc0CdBYx6osdhctOq8QMxaSOKZDreWpXLDVIw4vbNfv8Ar71r0ZXeyX81Bcwb5xkOIMX0mRf9QjFK5Zv6lr84kayJjr8VWMrOmm4j2WhvO5Gv2j3InYlwcct8zXNDSLWIMn0B+KkFRY4egBTDt+5oInKJi99Ae6Z2F/aAb5xOUkHn9XWfVRqOIljAQ24I05CD17/BBSxxLw8sHsxM6wBm+KqQ8v0Ta1IFrnGu92WCQ6SD2udLpMwToEVjcWAPLsMyguxggjdB0gOILiARccri4CL/ABRoIy0svk9xt0g6K+P7M+f6OG0aMEhztJN7Tp3vYK0ZVc+GgtcDMOhxFgCTBd3Cp8S5rnOLg6TOhFssC9rDiAld8O8sogsLgM5EuImwbYRoJc0+cLaSmzDyd0DtDFua/KIEAWtoRInXkQkjfSkkv4j1N5sAPgkudR08Wel7W2O3cuxABtoMzrBrratnodVmjQAqZmga2zO4iTlkyfNx/wBpU3I0EgF5OQCpxGQwEHM+Itw6u5E9SpWJ2nUqQKvGWEPZvGtc5nDb2hEOa4ajouSy1s6/E/KlJiXGqRSIyiA6pcGLy1s8nSB6SpGGApHMACYI4ri/Pz7p2Uw1rqhMl0uc6B0tZoFgAAAAudecpytLocWy2XAFpAOmsXt2Wdv/AIdnFr2R6ODYK4rxJBnKYLSYjpZStrkYhvsNYQbECSDESND05rlVqBjSXSADBMGJkNiw6lSq5a0XLW6xmIE5fay/WNx7wjboWKhEOCpZKLXhwyARBEPLRIIB/wBxI+2FBw2DY2u6rf2i4NcQRBJgwACB2/BS8VtSkGk5gXgOyS0mDEC5FvzVPSdvqu9qupsaxoBDWPgkNhhc1pgTa/WV0xejhlPLXRN8QVjVrUXcJm8fQgnMB3GqlVK/+Xdh7Foa4wGyS90kGbxBOi4VKQecO7KWNLGA5LGXNBgF3Z2sK2fRa2vXZRDRSFR2SDIdfWbyNBPZRuJfo6YpZNr7KfYlKpQpuxGQfUzEEwYLoLZF4E6clFxmGJxRdZ8ZDMEAktabes81c7WccopD2nCSP5VFosfw5votImf5gQmOTapMsUn4/RH242q+nUFUBpY9rcvMTIM3PMKO3ETRNM2DbNAn6WsybWHJXFYve4vMucSS51yZNySe6VSgMhlgJjy9xC0t/wCGXrb9lhsz2YOZpblaeIiSGCSfeioYxjnZf2kSRmlwaSLa+fom2W3gc02mJ6iWCVHp4Bwyw4WkHW86crLLBP2lQJoVYLvYcbucY4THO6xPh5sVDabgRJGg7efwW82jVy03WgOaWWjVwIE9LkafZKxmzsDUpuveXTyuGjivPcXWl0TLs57TpuFOrw2LwZ7hwED0CjYSgDQFidbTHS+nLorHHUXOZUabQ6Y1MyHC89CFE2YIaGwSDrMWmBb4clfRj2caFFpptvP7yD9i5UWnfNa4WLHZe+YZZ+1WtHA1KdDOWu3YfkLw05c5kxPko7qgPEBMWBIBNxpGo/JBCNWZka2BINM+k6n4ocMwOptg3DCNerp+9TdoUg2qaYcCYyZrsZJLdMwEN7mEbNh1/wB22kXFjQ15Dg0B5GYtmbkZo9EBBFOZsQBTHqC7l2lQ2iKmk3+IA/FXjcC8Pcyo0tLWBuoOpkaa6q+wHgHPTNR1XK+Jp0y5oLjzzfUJBsPsVpGqYLaZm5Bbrb0arKm0Ow7RAMuqt56llMj4hqtMVslped5T4gYLXFwNgBEAjkAixuFaIfSb+zLhwZodSeRoCbZTlsXTa2tzMeRPR0y4csdlTQYXNBb0g66+/pCS0FfAGuRUYwlu7pNENkcNFgiRqkkMUn7OxZqMD5dBJiftAFhKPG4nM4m0lrGkibhjQ0fAfErrsvZ1WrRa6hRe5gyiQNBlnKRroQhxGyq7BL6NRrdSTTcAPN2gC4pNnqeSWiE1gDTfK0Aknk0DUpbPwrWsbDYgEgSeHNBIjrYSecIdpuApWuzMzeuaCQA64BjSYI9Cqmhtp8ETncXGAYAAgZQI1vJ9y08WlDHyK1kmrgjVLmk5N4cziSODiDg0Htc+bldmoatEYWzsmdzC/KcoawveGw2ZdlN+/upKW1wHtbUpRmjiLyAA7R0AXHqumwNsMe8GudwCSJYKj3NZDmvkZXSSJHL8CTRHlgw30aDBDqm6c9pLXNcW+8zHMWN1MxGGwrMK3c12PqV2tbiKWYuJLHZwWkCwkAxY+eilfKFTw1dmHq0G5GNpuaCWXcxsZeHMNLkE3Mrz/H1DRrupseSGOsYEmOcEldJrRy80srC/2jtDJloEXouIaQGVAGmeEHNBEmQZ5n06t8RNDQGUCY5l8eZIa1VzNp4d4bWe0iqKv7RhbnZUplgEibAtc02OoeOih1sY2rVJJytc+ZLRwMcdSG9BFh39eb40+zquZ49TZYYjbRe4vFKmHEASS82AsPaA+C5v2jUaXZgHTlMCWgEZSMvu07lX+1vDlTAMbiaFXescBNVksyh0EGATLTa8/dOUkXhoE66uPvcT8FtJJGPyydJTds1T7AaL54An2WAXnWzZ812G0q1UFpfxRmblAbmj2mkNAkwJHkReQqgMg6SNY/XJGapDg5vDBBbe4gyOLqOsKPFBZZkh+OfvXVBo8kuaCW+1GaCLg2F+wWxwG5qNFZrqkhmVzS+4to7v38+pWG3xLi42JMwAABN7ADRaLwPjKFLEtqVyQ3QW4QeTn3uB5GNeSmWLfThvDLHHtUsfEGAq4amx7hVyVByqRlcNA6WkhxAB169Fnd8XOa3STlkkk8eUXNpFgvaK+zm121g9wqUaoZlAiGgNuWOHOeKesLyTa2w6uGxW5tmHHTcYDXtFx5TEdiCO614wys/JMhV3PkyYkmBpImLTqLQu3h3aDcPiadZzc7GniaIMgtLbA2JEyAeirtph74Y62QmARdri7MQO3ZNh6InjcQOvJa0ybm0XHiHatLEVHbkGnTbmcGeQbAIFtfPVUtCteBzsZIAvynleDPKEW0sMwcVOoM2hbmBzfgVXMOlzMXkDXte4iOiqRyyyTJ2La81Mpc8uDiIcTqdRfnyjquuEq1XPDGtaTYOkHhDYF79uai4e/AQBJ4XOkASAJ7i3wWmwtFlMFrddXHmT1Kxnl4nXi4/PvohU8bUpuJYWsdmF2sbyJicwPX4oq+1sRUs+vUMdDkbfsyAljKMuMaHXsf8A6AVy3Jju3QjWD+f2rPk2dfjSfQ1TF1HUxTJmDIzXOvXmLk3mLxElRcFiHOplmYiQDNvcbaaeUDorBrToQPdF+toUd2z4JLCADoOhgGbeiqyXszlhl6ArY2s0Ma2pUGVsEBzgAQ51hB8klOwgythxEzcxM2Am8Rp9/NOnliY+PP6Nz8nfiClVpHDtrCjVz06pBIcHtaW52NHKQIjUZpV7ittNYa1KrVOIa8HK0Mpsa0ZnS0uc4ZuFzRf6h638Kx7fm+KcKZLchGWCZFgdTfmrva2JdiKVMvN3F087xJI84+KvWkc+9s9qwOCwlXCvoNNFzKk7wUi3LmNuGNIIEeS8q8SeDHYR4nNurxUAzTeRmjS3ZN4dxNJ2ExDqdBtOpRDWl7XEGoWtnM4CBcg27qNi/G+KA+gGzdjWcLr6OzFxI9VnyZ0xxQDdnsazeSakNceK4LYmB0vJ8ynxLaVN1OuaT8oJad27he5v0XFx4D1jUT3XXZdXf4etXytp8b2GnTndwWBwIDiSCJjXkrjwZh2vwIY8BzXEhwOh0N/VR5Ndl8V6KLafiQYkZDSFMAOyw7NqIg2Cz3iCkRVdUAkEtJ7FzZAPcgE+h6K42zhBg8YxtMlwlrm5okB0jKeRjqpvh2iypUxFCoM4fRa6SbtDXS0dyC4X/lW04qYa2ZR1LhBLbGwI5npHW4trdRHEtMTPv911uvDexmYzHVKVRxaGMZVBYGi7cgA0/mVP402PToYl9JmaARqZ9poda1rkreOzGX7L35PPFraZ+Z4gg0HEhpdcMLgQWu/kM+h9UXjTwicNU3lK9B5gSf3Tjoxx+qTo4+Rvc4JwkzN4A9wsvRfk/wBoPxDxhq/7Robl47zTMjI7qOn5BHxtbGPIYp7YseS52V1418PsoYw4drnFsZwSSSA4TlN7xESgOzmfN3PaMpYGm2jgXNaQR14pnt7szVOvnuFMdETIkPE2BtE+dvencE1EQbcwReYuCDoQoayWtGu8JeLxhQGuc4sJ/dkWE3zU3fRMfQPPTUkeh4vC4baFAPBDxcsqN9thOovppBaV4Q6tUYN2Hyx2rXNBEhsz2PcQpVHabqXsgDu0vaffmK2cGmaHxtsWtRDc2YiSG1WiQ8HQOMy1w6HXlKyAw7iTYmeR5XB+5aGh4txOQjeVCJAyueXt9zpj0RbBa01S+q3eGWuAJIEk5uLr08kaCyS0zP0tnQNI9+v6CnYbD5uEBpqcswkPAEweh79l6ltfC03YOjVazJnyEta5+WHMJyxMEei8hw7j85YJP78DW0Z4iOii2Wr0jZeG/Cm/Lqwrso0w8TTcyd40U8+Zri60tB5EcJKqdsYd2Exj8PmD4cBmaIbxAOiO0rXfJnQDaGIZ0xFQTESRhawmFVeNaQO0ajucj/tsH3pklC8Ty80qRMG2mSXVi4NAJG7ALi7k3itCjtF/yiQeY/XJdnUf2eefpZYjtqmbAovJE9J0EdlySuj15ZeKeXZWVqpaXcyHEBoIuBEkGZ+kNQNQu7nuy5gYAE3EkXAmRr7TQRbX396lFuZpgXaSOovETzTMpCbayZ1vBi49AoZrjdOzzMENDRAtry1J7+iSJ+L3ZLAAYJEknkSOR7JLPxm/m/R//9k=">\n        </ion-col>\n        <ion-col col-9>\n          <h2>Freiburger Münster</h2>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <ion-card (click)="attractionDetail()">\n    <ion-grid>\n      <ion-row>\n        <ion-col col-3>\n          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTAZQ5Sfm90Tvidyz_OiJSEhNEiv8CNKw_WKHQWQjwuPOCw8unTtA">\n        </ion-col>\n        <ion-col col-9>\n          <h2>Historisches Kaufhaus</h2>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <ion-card (click)="attractionDetail()">\n    <ion-grid>\n      <ion-row>\n        <ion-col col-3>\n          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTEvx_8nRheqew5TWcdxsbsO6jxlyJahGnU7xMXQuKQ79OMYKtm">\n        </ion-col>\n        <ion-col col-9>\n          <h2>Augustinermuseum</h2>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <ion-card (click)="attractionDetail()">\n    <ion-grid>\n      <ion-row>\n        <ion-col col-3>\n          <img src="https://farm9.static.flickr.com/8139/29259172572_9133f663b8_b.jpg">\n        </ion-col>\n        <ion-col col-9>\n          <h2>Bächle</h2>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <ion-card (click)="attractionDetail()">\n    <ion-grid>\n      <ion-row>\n        <ion-col col-3>\n          <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUSExMVFRUVGBYXGBgYGRkYHRgfGBkXFxcYFxcbHSggGBomHRUWITEhJSkrLi4uGCAzODMtNygtLisBCgoKDg0OGxAQGy0lHyUtLy03LS0tLS0tLS0tLS8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIALcBEwMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAEBQMGAAIHAQj/xABCEAACAQIEBAMGBAQDBwQDAAABAhEDIQAEEjEFIkFRBhNhBzJxgZGhFCNCsTNSwdHh8PEVQ1NicoKSJLLC0jSis//EABkBAAMBAQEAAAAAAAAAAAAAAAECAwAEBf/EACoRAAICAgIABQQBBQAAAAAAAAABAhEDIRIxBBNBUWEiM3GBMiOhweHw/9oADAMBAAIRAxEAPwC2U0vg6hjxqGNqQvj1mechlSW2MFPHtF8bM2JbHI6hjAzucEVBgZhhkZmyVTggNgYUycEolsZ0Y8xsuM041bAMSg42wNqON1fA4mJpxqTj0YwjACaTiLzl1FJGoANEiYJImN4kb4lNt8Ufi/tAyeXzIBYNKAOVgkaWJj7kx1xnKgF1K40DRiQOCAQZBAIPcG4xExw6Mes+Ijje2I6r4JjVsbUj6YgNTGyscGgWGpghcA0mwXrwkkMbzjU4wPiM1MBIx6xxEzY3JxG+GQCKo+AK5nBVU4Cq3w6FYFUwDWqTg7MdumA2QYohGDsmIWGCmGNAPTDCg/lnGYJPxxmMAtdHNtUuKTAaiAxIggMwLAzcWt3kHbaUgThZ/tpQNCBiZgiw0bC8gCbgkbiTOGuWTUSTM236joY2HX4xjlizoaNqeYg49aozExqi36Zt1ItfoIv1xj0BONqbhcFq+jJkX5vaD8Le7vPfVaP9caMKna4DTsYNyvxsF/8ALBge/piUMMJtBsjyiNHNvJ/e32xNpxstTGwIOFtmISuNSMEkDEbDBUjEJp40KYmOIajYdWA81HG6PiEnG6YLQLPc2jMjKhUMVIBYFgCRAkAgkfAjHAs37Os2K1Om0OWqtrqb09OqkgaN5LvUEbmJsL47D4n8VUsoNI/MrEWpg+7OzVD+lfTc9OpHO18Q5jzFql9TBy8SwUyUOjROnSCgiBIMmZJxyZXGzoxRl2jqSGooA3gAGF0i2kSq9LajpvG3xheq8yRb6H9WmfidPwOIeBcepZpJQ6XA5kO49fUeuCqyHHXCmrTIStOmRJWeVB22bb/mv9l+uN3f1xDUBxC1TFEhGwguMbitGAGfGE4NAsPGag4IGcwndsYCe+BxNyHP4n1xolQE3OFiPgmngcQ2Mg+NXxFRN8bVXwtDAGYdtRiYt0+pFr9B164CepUPS/wttvPebRhkxOIalsMkBsVVKjmbbap+Nyv9PrjQAxfe/wC9vtgw9cQvh0hGDhMaOMEKpO2JRRAuThgAAU4zBxK98ZjWCiuZLjldyAOWlK3Ku0RdRb3QOUep+c2Dh+cg6WLAkKZ1lfhAgADe3SRhBw7O1AYNT3ZCo4FM7wQJQK0QP79MMRU92RZjFpQqRJA07RuJH9ccsEdEmW7LVI3bV8T85+8fK2JyQcVzL1SAIMg7REXwXSzRGLcCXMcC3XHvnEb4VnPzacYucONxNzGX4mNsSjODCnzcesZ2OA4I3IbUs1OJDVwoVT88FCqQO+A4IZSCjUxqz4DfMA48ZiRGqD37euDxByDguKX4o8ZrTmjlmDPcNUsVTvp6O32HrtjjHHeO55qtSnXr1HZGZGE8sqSp5Ry9D0wDSqZhxKliPT/T4Y4p529I6oYV2y6vU1EszSSZJJkkncknrje0Yo/58xLTb7z6fHG4y2ZP/Et8ccx08i6UMy1Jg9N9LKZBB/zb0x0DgXjSjVXTWdKVQbkmFb1B6H0xwenlq7bF9yNz0scRZilVQwxafif89MUx5HB6J5IqfZ9NrVVxqRgw7gg4hqJjkXsVJ/G1rm2Xb/8ApSx2B2x6OKfONnDkjxdAjriOTgiocQtipI8xl8erjcgYxjZDiQYGiMEUmBtgNDJk9OqcSVHIxHTXGVWOFGNHzGBXqE43dcQVHixtO2/Ym/0wdIG2Yxxpo74ioZsMisRBMAjeD1GI6+YBtMTt8u2GTsV6PMznCkxFvvcRFt4P1IwAc+XWxhh3vcWIMWJufmMaZpyGhgWDbGBYjYR3vbuflgFQEcwZVgSBJsV9OnT5/TEJSaZaKVDVFUiWQT12N/jj3Cpc/o5YfvsetwPvjzA8yHqDy5DShWKR5y8oC85hlJ2gxBB7COtientWnTDLVEgAkmJNri46Dcf5suq58rTKSNBBAuXMMwAOogCQY/vfGtFdQANRWuZpKdB1CQFYCZED06bzOBy9A8fUs6ACwtPTr2PyxKVwpyFRVC8oli/OSYChpG+4iNv9I83xB9WkTAgnaSLTp6RJi52BOLeYkiXBtjgrjIwuy2fdxPUQDFwLekyZ3GCfOY7C/MDF73j6QPrh1NMRxaCgcTU6nrgakDHNvf8Ae2N4w1AsMfM9sRtmpwPjwjG4o1k3mY8FY4inGYNGs5l434MPxtZrAVlp1YJgEiUf43UE/wDXjrHDPA3CsvkkrVqJCrSRqj1GqqTyjU7oGsepEYpHtCAFJHvqkqIkkzDQI9UH3x2Lw1Rf8JSSvT0ME0shfzrCQJcjmlYJnvGPH8RHjlaPUwyvEhRV8F8IRfNbLUAsA6mJII3Fy18C0eG8DkD8PlubYtSMHbqyx1GFOZIq5hxdKVFmREBkRqIsvSTzf6YlzWSpsBTUCQrNaJAYlZPUDkY/I98RVvaodutFiz/BuF5ZFqPlMuEZkQMKCMAXICkwtlki+wxzn2q8Iy1CujeVTCvTJVadHSFVSDJYWdiWc2iAB6Tb/BWbqhquV95EEgGDA1AGPUgzp6GfnW/bJl6qpTfTUNNUqLsNFMHSqxA5WaBYncWwO0PF0ynexCkGzmZI2FEgfA1Ej9sdbzOXxyX2J6lzGYYCxRR/+xNvmBjrdTOL1GPT8MnwPPz1zAGQjEbnBj5lMD1KieuOlHOwYtjUOcStVXpiM1BhqFMNXHtGtB74jJGPFMG2NRrGaZoRjPOBwHmhYYFLHA4jcg7M1VAmdu3+f3wnzOcbrJE9hIBnp1gW2xo1QsbEkSP2uR8jiCqTYEDW39Bc95B6YnKh4tnlBoeZBWobRe4XaepgD5T2xLmK3Mq9dQ3+lvSA2EeeViOWQZv2nmIa3/T2vN8G5PW8HURp2Bg3a2x3ABP19cRWR9Io8a7ZmezIDBHEh7Gf02tHa569QPTEVWqDEE6hytsYJMEiD3uPtHX3iKluUxMrHUErzWO4Nvt64DegIEtO5lTziQI/VcWB79pxKUnbKqKokzOZXUfMV9Vp0IWWIGkhutoxmABXzH6agjpGn+v7YzE/M+B+HyD8Sby38tgHYKdLUzOrUIUlIkFBqJ9VHxwyyaIzOOYgCTqna9t9jpIliCJO+K7k6Req9dCqgMVJaDKkC+kxJb3v32nDDKZp1ao5KsYUhWjmtyah0JYkwbD6YyezNaLVm8zSFErQQkghSWJVVki5BIk3/ue6WhRGsFnZgsbbCRIGkwAICQDJAYeuBq/HGqBaSqmhAo1AQYXlUAwTqiY9STaBgahnWWpp8pmQEi8GNiIkwGkkz/zAXw0ppsWMWi2Us2tO6sICjSYkki0Sp0wNSzHrg3L5xl5VAYksYEj6ki42vNzOEWd4hrVSMuxIY8rQJ1LoixNpMz6YN4HT0S7ky2kGDudtxv8AA29Bi0Zu6RKUVVssVBjEkG9+/wCxxuag+Hxt++JKdRWFjbHgM373+uOxdHKzQGdjOPcY1MdQPpjXyx6j5nDAPYx7GNdB7n7f2xsAfQ/UYxhB4so6vw3MFP4mjDN7oOoQ1S45Bub7A47Hw8saaF2RnKKWZAQrGBJQEkhSbi5tjiXtBFRqNNERizVP03gBTc2tuMdZ8KcWFbKUajKKbFAGQAgIVsVE3gRHyx5Hi/us9Pw/2kV7jfB6mXrvXpJqpVJZtIJIYmSDHSSTO0T6YW1eImoxFOizFwqiQTELeQLnmZu2OkfjKf8AxEB/6gPscQ081TXerTFhbUo2+eOZKS0mV0+xT4P4I1BWqVFValQCQv6Rcx2m/TFZ9tGUzH4N6i1ZoBk10tCcu8Pr948+gQP5sX48Tof8al/5r/fFH9rnEKFXImilZDVZkKBaljB5tYUwV0k2Np0noMZKkMnbK54LQJkcuFsDTVj8W5mP1Jw5d5ws8M0tOUoKSCVpqpIMiQIMEb3GGBx7sP4r8Hjz/kzzGpxtjxoG/wAPrthxTUjGsYkIx5GMY0049VcSrGNkIxjGuYacC1iAJMfPEjsQRIwBmK8ixGn+17HtticpJIeMbZ46NuPSYPwPfpfANUa2s+lgQV1EQ0b83+GDqdRCWU2PS8SPQbHG7soAOpe4Hfp2jEJJMsm0LatVNagLENJsL2gFTseZgbd/UY1L6KkoeVyDE2DQBFxyyAY6CdrjHmczSFSpTWhYWFypiAR/y2Wwn5g2ipEtRZLkpoqrIsdDB2E9en/l2tiLlsrx0S8RQaQsoNcwGB6mzT3nYx9cLcm4eoAZKQDBMxABIEW1LJHyGDa1BiisE1HSCGEEEEXIvGm/UYTsfKeotQSbPygEAPMwB8Gn4ztic3Q8djHOZSlrN6g2stSBsOnfv6zjMF5bOSoJVWm8kCYNx06CB8sZilxE+or2QqqD5VOmtPy1ILtDLUCkaahUXJZXViT1HUCAvz9HSpKFAmsjXsDEuSmqSAIcX3IAAwz8M0fxFeo1Tlp0gCLLB95RUAa/uoxDDbUbgY847TpNmqQqDTCt5jUyGd7jRAJA1mGHUC4F1nEWrjZROnQDlMjUQFjRchgSSWCzqmDqJBDGNu3fbG9PidKTOhFUwQ8MbtBFMKBcG9zFjfHjZmnUYU6FMIvWppLsw2KzfpNg1iG5u7duCUkpU4AZ6hUguhVjBkkFgV0QAIA30+uAl7Bv3IKFbzEDIzLG083WDYoVTaYkm2/TDLI5yiUH5mioIglVcRtGwNyN5j1m2ATwyolSHXYuQUAuQdUAKNVjqtYwD2jBuRNN2hrMJJJMK15VtRuOk95OKRbsWVMc0OKSsaDqJuBEEARIOxGor8jcWw/y5GkAEmLX37XxVRmllhzCwmTAWAQwYm0TJHSCuGh4kSVEkhQTsVsQVUz1Fz6mMdOPJXZzzhfQ6K41jCyhxhTC6lmYNydrkesgNHywblM4tQCNzuP5bTf1x0RyRZFwaJYxsBjYrjAMOIVfx3l1ZKUqDBePSw2v6ftgH2fcByeazPl1qRqA02JFxzArJNwQouLb4N9oGZCLQldUu1gJMACY7WJE+uBvZlxADO0yGWn5gCg1B5hchqYZEvKMQWg9BvjyPF/df6PV8L9lfs6Svs24XN8nTPxLnp/1Y8Hs74SWI/CUZWCVE2mYkTtv9MWHjdCo9ColIgOyFVJJETYmRsYmD3jHCvBWazbZ+otZa+gMxWEdS2l7DUqy0wCd8czLRV+p1LM+zvh9OlUNHKU/M0sV1EkagCVmTtOOLVcsyDym5SoKkiIm+rmAiZNj1+2PpdYdbizC4IjcbEHb4Y+cs6lGlWqIHLKhZdLDWDplZJCgNJuLCJHa4aGg2WrwXq/A0STqP5l+/wCa/phwfgftgPwmg/CpBBGqrERYebUtYRbbDQpj3cX24/hHi5f5y/LAM/nFo02qvq0oJMAknoAB1JJAHxwj4z4nCNQFFS+sLVqWPJTIkT/Kx6A9jht4pqBMpWJ/kIA7s1lUepJA+eEvCMqMrTH4l0FSsQX1RGnQESncXCqAIxzeKzyx/Sjo8NhU9ssORzIq01qgMocTDWI6Xi3TpiQjCrh+WbKsab/wKjnyWH6Sb6Gjabx/0nvhwafqf8/HF8GVZIWRzYvLlRHj0HHpQ9/tjyD2GLEiCuSQQu46/eMA1sk0G7fW3wCmfXthqfUHCvMZpnJRA0dTcdeltrfviGRL1Kwv0FuYoKvvNMRGoarAkkcpn0JM742ZUZQymIJ5W5wvcaWuDEn5DBVZGQTKoogkwCbEXj1v1wjrhmf8uVcLcxpBAvBgD0ibjHLLTOmOyWqhKhlAAZYmJWYkcrHe477dOolPONTbUACAAx0kkDTZmCsQwPPJ3HWd8FHKSEqzU3n3SATsRCgjYEEz0+eBczQ8wToXUovTliSGsBpaIEaiGk3ttOJyT9B00OaOcpgaS003UvTZf0gjmFrgXLDsJHTCvigYlSYOpSD19wK6mDcEQy9Z1R64Dy6KukJWBKMWAP61kALBEo4ANiOgB74l89qj3aCAac7E3ltLR7gAQGL8xsIuHktUzKFOwL8BUq865oUR7ujVEaOQmJ6lS3zxmLHU8PUiZ8ptgOWIsALQD27nGY3lS/6zeYvf+yEPG6JoKcxl6uYqBRpcmlpRUHLTIfSBAbSIP6ThJwxVqua2YdWZlAl9FiCAsBrEBQoJ0m7QQ15u/tH4/TfKihRZ3eq6U1jUwIBuFBF9gIF5OCqWQqKi+RQUU4RVpsyBgFkkkibnSoOozvcYDjvQVLWxaqHyy6VhJImNDG0yWMReOtjIXANXO1aVTzJ5SJDLpggyCVEBSZJHSR0iAJ1ruKrVEQUiTuwKlx71x2A2YCwYwQDzM8vlGcOjLpZiywxPK0kcxCmRpYCI6zADDGs1ANPPPVAhirKdSqQAASCOVxA1HT7txfYwcb0c7Snz2cCLlRBZYlVUKBO0SJ3PpiLMUqKZfynQpVB08pYgpdnAhSBK0zBFwzCO2PaGaUyBl6b1JCtmArGnSlY8tfLhJUKAxkGeh2B5A4k9LJpVZ3aNxAVTVZQBOptMqPkOg7nC1KuYBqaGaCBNQ3nlEDlIuDykTaNpnBGZo1G5BmNYBUwJQALMOSS0jmaxJBm4Owk4lkXpaddNE1FQpSGXm1EsRykNYntb0nB7B0b0AarAWVkAJNySQD2iwm/xHfFkol6WlQARso+5BIG/LthZwnKKY8uqylSSNdpkqY0xqW2wO09sN+HSYB1QZN7gQAAGNrc83g8vyxaGtk5bGGVrGpBCESCRPpF9vUYK/DuP0HBXDg5AlV2G/wAACTC726WwyyyE7xt0+XfFvOaJPEjl3tOaoqUI1gk1BABM2TeD0/rjPZJxan+IWg9B2qtVqMlQHToGlZVlLAFQBUJ33Fj0ee1iAuXBMEmpETJgUwYA33wJ7HcpTNd2fLt5qx5daG0qChV1N9Kkxa19R+fnZ5csjZ34FxxI6/q9D9v74ArFo5Q0zaBt0nBHEM9TorrqEqsgTDNf10gxthKnjLJaiPOEruAlQkTBAI02kEH54kMM/NqCbMf+3pIgr3aNVu+OBcfpefm3qr+WrMSiBL2HISLXY3Kx1Ax3AeMcpsHqMey0ax/+GOS+J6tOpmXenSakkhQqoFcQAGd1ZORtxHYA9TgDwFfDfH1PJqMs9Ko5plwxGgAlndpA6e8v0ODT7V8v0y9b6p/fHLeKEGs5UsRO7b7DeAP2wV4f4RUzVZaNMSx77D1Y9B3x1R8RkiqTOeWCDk2dQ4dxD/alRKl6VHKsH8vUNVSoQSpaNkA+skYa8Z8NPXem6VgArK5V5cFl2gTAEHbAfiVKuWoZWjlw9MX1tTk3CGQTpvLGZMbYB8NZ7N/+oV2zFSaNYLIaFKlYYECxgtB6wI3xzTlKb5MvCKhUUXvM8LFSiKJYqLXWN1uCJmLwbdhim5rxa9FjSekrukqWDwGKmCwGkwD29cKuCvmvPoh3zUTLahVIETGq0Rtvi2e0jwzUzKGsrU18pZXo8zcTsw9PUjBhknien2CWOORbQp4V4h/EZmkstSMP+XqBSoNLEkcsl1IW0iAZveLWyY4N+LezSUq0WBBFiLwfmDHpjr/g/wAQJnaMkAVkgVF9ejqP5THyuOmPR8Lm5WpPZw+JxKNOPQyqsegwFSoMDAIXuQL+gHQffphnVojsMRGgoEQI3OOiSs54uhBxIuz6QW3AWVknqY6Dp07nERU0wKaTrYFWIJ7Auxg7ydr7CMMmSHLJBmAJ2G15377f442yeSUDWwLdJmOwt2G9h36zfn42y/KkVthmUfzElgLujdd4cE2DbbRIN8HZal+KAcaS66tveQysSbWiCQbGNjthhW4iusJTaKgI7QokKTBBHXrtv2wrz1By7VEbWYBqIW0CqBsyMsAVN4IiepxNqnraH7+GKM/w7RWWm8MdY0HTpDeY0OChtKzeN9UjrjTh1FoQtzL5tSms6iQqvUHKTaYB6/oXucOeLZdKuXZxVqFVXWi6jZlNrNJUg+uFGRo5mtkUKsHW40aYMq5HI3UggGI3PzxJxSlr8lFK1sbLkg/MJg7SXWwsLAwNumMwRwqqXoowIuLjzHWCDDAqRYggiPTGYdKLEbaK7TQ1eKIlCnUrrlNTHRpM1f5yarlRBCDe5p2Ha5nOV3rFWyhYIQKkVlkEKGRasKEtIaJ/lmATiu8Fzy8IoikTTeuxl1TU1RniygXkLOmbCQ25nBHDfF1WmirUyGcQFW1kUi0li5Z7gSx13J3ucKteoztjziPGaZQF8o9Mkz5jnzRpYEHnQtpZgRZo6yOhWVeKZekgZWpVH5SEXVrqXDGkKajlcaotp94zaQNanG6+bplMrlvKp1mj8RXZEQTCHkEkn0nptiv53ww+VWpOaGpEZzVUBTKiKa6yZCN7ukEbrEzGFfwMvkLyNNa3/qs1T/JY6lpKoenSFQyk8wGphB543Fh0uqV2CawoqKBA8ryCpEzpI1FViRcdifTFZ4Bw+mBTFqSzodYQaG5pD6oPS0nY2JJnFgq8KpqC9GmjBZtSdlLCIQhSRpfTBG4sRaZOijSZpliHLUygVlk03aSW/iNCyV1gCoEJDWFxAg4Jo5Z2pkgakANJQblTpDsUmQR+mCQR5d5JOKo/FqRJpiipddVPUihVgmSWqryq4IVyQeuxJMM+H5HMLTXzq2kAExSJUTJnzCQSxJMhxpuv1ZAZ7kc3SR9JX3XMQCZ5mEBVJLwA1wPmbS4pcPqli4BVSpARiZJLapbQYA2AEn19JqXB6VNqdZUKlgwqc2rUJluYyXuA0HqDsSTi2MtCnTNQmVAJsTeBJAvvbYYeMkuxJRb6KyMvVQE6bejGQIvCmV9bd/TEuTzdRTpc2J1Ae71noLj+22N34ur1CbIoB0jqTKi/rfBnCaFPMIXnSwNvh0P+fTFOUBHGZTPanXDplnKnlNUhhcr/AA7z02xP7IqrNmX0kinoDMqsFEkME1092tq+BGBvarkjTXL6SIHnCJieVIi1rgdt8H+xFUZalRk/MVnGoLTIAISOb+IHMtb3YvvfHFnrno7ML/p0daGEXFMn5KZzMrDM6h4YWXy0C9DJFgSLTEYbZiowXkXU3SSAPiTvHwBxzzjv4mpRqPnKq0wlR0SmB/FIIAKqYBQSSGOo98Sb0PFWT+DPEtWpUFF31F6mkSFlVFN3LjSBaaYSO7jsQaT4rR6ObzAYPJeqylg41KzNpZZu4ADAXNxv2O8MtV/GUtBMzRU8ggKGOqYWYjr63NsKPaJxygM1UDKQwZgVdme6s0lW1DSpP6RthY9FZUno5lxmoTXqETc9RBuBuMXrwFkXo5Y8SCBlRmp6QSG6AubREuB1suKZVqUi7ObljMXIvf44b5Tj+im1MCzADZuUag3KNNto+GKPokuzqPh3xXVqV1peUYJizKTcFhva8DriM+0KrOkZZTzMo1OwNjBldNjtjnWU8VlDqUkGUMgPYoIUjsY64ifxGJB0tIYt7p3Jkz8xgcY2a2dVynjarUy1auMuk0/KgB2Mio2kTyzvO2IuG+L6+YzByb5empYEG72JTUNwNvrYxfbl9HxUyIaa6wpCggLE6SWWRMG5JuMenxXUNVqsMXcEE6QNwVMAbGCdu+BxRrYFx0Glnq1N1XkqNTYCYImIJsTA672w54XwvM0FTP5MGoqA+Yo3SIFSnUH6lmQD10yBbCCrnU1u5pNqYkvIEzN92tgnK+KjTWoqLaqrU3DKCCGUjULyridxv1nDJtdAaT7Oy8F4rTzdFa9L3W3BN1Ybq3qPvY9cT1aM9Bjhfg7xAclXFUc6EaXQEiR3jbUOkz/XHcODcZoZtPMoVAw/UuzL6Mu4/bHp4syyKn2eblxODtdGjZST2sRYQNiP6nA+fqlA1yfmLfG3TDZIYagZF7/AwfuCMBZtUAljGpiFuLzJhek7xh59aFhd7KbXyZBLEanqEASFsIJkyDFhtPQE4ly2TqKpqVFWoh08qjvsAG3Y729cG1karU0odQAMi5UmQL7T70n0jDH/AGSzXqM3JNhyg3vMTc362m3THKo30dLl7laqZikHq0lZqdOvTNMEgoq1ADDMPdZWBAPYqLjcSeBc8fJek0SrkgWlR5dLXP8AKNTGT3YgdsHcTooR5Rphy36ObpssySN/QxOE3B1q5PNsvvHRdiAxkaTre8sYaDpv9MSdxlZRfVGifivh4VKr1CGBchjbTuAZgHrv88ZiXO8dLuzN5Uk/CYsDDVFNwAbjGYRqNlU3Q+4P4Yo5MmKYZyBFdiS7NF1JmVkGIHpaRh4yuvMjHSAeVi5uYbSdXMPd79RbFC8Jf7Qz9Pz1zVCiKYARCgqa4LBteolqa9PnYWw9q8S4rT5alLJAyPzTXKpY/qUnWcdEZKujllF32G5rJUmfWyMh1LqalLF5AidPMy3Toeu04R+KKIp0n0OjiqFpcyQS1TkEzysNryI072GCszRqNBzXF6NJZny8r5dM6gIGmozFiBHUR8ItWeNLwukpJarmarFWQtmA8lRdqjU2IAmLRJC22kLNjQLK5ywoUtZJYALraX5lmS2sWOpZMHo1xuSMnX/EugosfIFq9UGFfSH/AC0OkOSxZQziwAPUmONeaGYtChif0qFEiCIUAAC02+/Vtk+PZgKafn1AsGwYhY3t6XPyOJKaKuB12twNQgQDy0TSy6Dp0oeUlVAiBqLbbnEvD+CupnUGqAsA2gLawsViV1A2IP6+mmOO1M3mdZPnNzKASXaSq6dOx2sPphlwzxLmkHLmKnwZtYi42ae5v64PNewOD9zsHCaeksKhMKSt9RHMEKyCxveJj7zI3GSNWlZIJBaIIMW/THML7GcUTgvjdmcrmX1ozAh4uhGncJGpeUetsWupVBhtbLMFJA0mOaxP6jO4Fxh00+gO12a5P+KwOiFWVaJgDTq2NxpUQZ3JncYLyOYZdJDnc6ghgnoBHxkkxF98IOJaqdVmZv4tEhCpUKQ70jV5WNgFVmg7E+gJa8J1rSg6iWadKavefe7KJMncnabYKFsWe1XiJJynIbeapkBpBFKSPSQN1E9Bgv2JMRXzIgQKdMNaWBltEMBEGKhIJJkL2OAvaMtSkuXNVACdYWWUAABDsDe5Gxv3w39imakZlRTqbrNQkFbTpT3p187tYRETFp5ZO3Z0w1A6rrHfFe8eIGyjHUoZDrUEgaoBBUHvDH5gYetUxTPaHwirX8p6StUC6wUABALRDwetiOvywr6DHsWeDqZpZ+lDB1rZedQEQWAfTHp5RGOfeO+Bq2drqKApTUWFBZjcAalYmCGhmgARPzxbeC8Ir5dhmBpKUtBYo1MswcgflhSwLFTEdZtvhL7QaZp56tIUPy1CVGixUAM1+YnlDXGFXRR1ZU8l4e0VcrrUkMtUvEkDy9SiSNhIG+HXCuB0vw9eabs70qflkIxuKiatNuY6dXeww84L4jq5bJU0fLNWpOK2txUVVs1RijBQWkEBd45T3sBl/bTVRQtPI01VQAB5hgDYRyjDWxGkhNl/Dk1EdaNR0CH3abMC2qpOrSsWlbW2GBeG+FazUxrymZDAMP4NS/LuZTvsMWbgftTrvUaimTpU/Pf/AHbRDlFUGApn3F7YJf2nZmlFLy6ACgqCwbamouYqCZEdL9sG37C0itr4YZEYtl6qlglNNVNhJqVdDKAQAXKtYROGdLwbXStSZMnWZaZLXSL6iw1AgT0+mJs/49zWYpMxWjFDy8wmlWF6dVdIYFzbluLfHC1vbPxE7Jlh/wBjf/fAuQUkDeJ/DmYpVM5mHoVFp1mK0maL6riesmOsbYqVXLKGuDbTI2IOm4vi38T9pfEK9FqdVaGhxaKZmZ6EsYxV+JtUuXAB1LIA7rbqegwY/IJAuZ8tqUBSHW/pG8T3uMb8GylUVkNOo1NtSjUpIKgkS0zsJ+2NcoF1kP7um+/YdsMMxmHo1IX3WUTaep2uO3fDAZbKhq061ZmrfiNNWppLXRwrFZCSVAbmNu8jpiXMZhjSD0WJol9Sho/LcAO6k9IuAu0ERhVwY65Gsn9REXUcq2WdySB/3dr4bcK4dVRyqozU68h6biAwBI1IVP8AEVlPL1Aa+GjiyOLmlpAeSCaTYPwfxY6VSKa02qVAlNdZ0opXVEy4CzqNz1gdcHVPEX4fM5ukG1qzkhtQZfMCorNBNk1K0KDYGNxiDjHhqtWK01SkoQvpBYBhqbUFCldTDSEiTNz8gV8ElhD1AO8KTMk7mfhhXkqtgqPqP8n4gpKo0azmHUy5XlDQWYySOUEz8MK24mKnk5ggllVVqELK6jYkA2nnUwTuMFr4fTnFMMisgUwZiGglSxJEqYIwLneApSXTzsmoFgWEHSGINgNLX/bAlmvRoxjY9psagFTTU5wG6He/fGYq3D6OeKTQVUpy+lTeIZg0kmTzTjMP58fYXj8lDoKTLBgDcnmg+vxw2yObZByEAsJOmDO25iQbi2FWUyxInbt6/DDVcqoQkMQQ1MAQCIY6WJ7HbCtlkhlleNZhRCsxW9oWLAbjrA6nthalIAQo+69B8cMvDlH8wK2oB0BMjbUrgjbph/W8LUyAi1gQswZUteLRqufljX7ivvRUatLVT0aIllbUCJJA0xue/bGlWkWVKZBEBYImTAI+4b7DFoq+GkCMPMk7iw6Buk3983xEnhoCA1UXMXEbgN6zdY+uNa9wCkZCQCQ5I68w+XQfLBPGKnnGmdLp5YO15kIpnlFuQW9cXrJeDTmMstHzANAYa4N9c7diJHfGnE/AppArT59UsN5B1s0TAEQ0fIb4HOPVm37HPeJVQ7KVDKVAU+sAC8KOi+vUzibI8SrUGDqXhQRDBipBkGVI0kXPTDyj4LfTZ9yFAgkkwoG3KBYkk7Se2JKfgqsp0rVphhIZGcywk2p6QQRDHc2tPpuS9w18CLNccrVCGLgmnqK2UFBUIVx8INu3pAw+4B4nzNTN5YOykNVooeUgNLqJkTzX3+2KUCwastgUJEGGA/Opr6gjp64Y+H6tZ81lV1e9XyzAABdqiqt1g2AixtGDb9w0vYvvtkzbA5UaSOauVY3kRSB0kCwkzcdjYYU+DPGJyyOhVGWowJ1GIYWtDDcaZ9FHzsHtgyyxQV7MC50wSRIgQxN/dIEGRAnHNlq+WsDSIIMk35YmIWZMzv0OFYyOm0vaVmCkqKZjc6ZBuvKoFQGYJN+gnGU/anWI/h0VJ6kNG5Ex5nWLfHHL83xlKdMhVAZjIi1oAvbb3vt6jEuSoZp3TzXWiazqiKytLNIAGlQdG99UfDAD+i/VPHtVX1JlssaglVlDyQqklTrmBtPSDt1UeJ/FGZzLg1VUELCqEsBZiTLTcgCe/aMJKwdY1XJEqAWYEco2AuJ3noe+PKmbOoCSp5h7sAwW9yZkSSBAi3cTjB6ZdvCXDBmuGuBpV2q1QrFQdJ0hZiexbr1wrzvsod9HlV0ChAvMpmb9sMvC3iWnleGmrXsxq1ISwJOlB0/t/XFYzvjXiGbdaeXHkowJUtyCwZpLmBsrbkj1wq5W6A0q2Oqfs9ZKy1EZqZXy4UEkaggU1ASNUlg7CYi8WjA3HPBz0kqVlzKstM1AyESwaooIUkG3KVIBAgMNxBw39m3E8wcrXet+ZUR5AJEwEmARbVzNHqekziTxnktWQqyectSQORH60Zz8WqBj/wCI6DF0vpI39VFLFErQzUsCBlQBAj/eav6/6dYOAeC1zDqpr6Q/ZQY/V1PpiLhvDnppXkyGpFfhzKZ9dsR53w7mWrVDTo1GAKQRABkKtrg99gcIhm0dMpezzKjytdZj5e90UNG0ggwPnjnftAyvlZirTkHTUS42INFCDHwIwzHhHNLrd8lVaHLACNgWixYSIO3XCHxbX1uzeW1KTTJR9WpT5KSG1Xmb/SLRgILoSUKRdyo3ZYv8Bg/jWaCuBBMKB+5/rgPNjTUOjfQkae5ppMesz98bcYuxkx7ovPRR2GGQGhj4d495DtUKEpAUkQSCTKsBIm6nqN56YfVfGD2NMCtMjnhAm2yjmPb5GxJnFFpECnUgzen+7d8bUq2llIAHX9IntfTbFVlkoOC6ZN44t8i4ZbxjmmrKirRUBwCQrmNNyZLRsCb4245xvP0avl6iAQGEUhquJAMg8wsf82QU+MuhJGsi/wDvLX3tAB+ePON8RbzWRwHZHcGWYAEHSVsRIGkXtt2xHjGhq2OaXEs6yViXzFkWDp071qQMEKLwW+U488P52sK0vVZgdSzUqagDNyELg6oUgW6/PC3I5n8nMnQgASmPdJ3r0v5mPbAmVFUhqq0mKoZZ1pAqsQTqfSdMSOoicCkMtDHilFlrOBrI1EzJvNyfvjMWqS3MKakGCDMSDsceYXkPxKaeCVoOim7CRDAdNANr98M1yj0aVNlDK76NY3MAjVY7e8ce8LXNMToarp1qwhyLQJFj9sE8Xp1hlqAbUKikBpMExonm6264ZgRtws169ZKbA8ykKCFB1NIYCPe2W3rhkvgWK/ka2NQIHjRcLGiSoE/rP0xV1zDjyaTyWJIkGSSzWv8ACPpgniy6Xqor6bD/AHgBBgECxJFz6CJwd+grS9S2ZrwvUbMIpgPpEaw3N5ej3Yg6uaYvYHCnxJwFKVQmqxDPFSNDWBYRIgmJpkfPFZpM41zUPuET5hMS1MiDrsTHT9sHZXi5TMUWMstKozatanVywsybAMd7xcicH6gUjpVLw+xyFGgKoBRyxMPBDSIkDs6/TAPHvBT+UwGZFJRzEzUtzT0FhcDbtj3gniNXy9TKs6s60mjS2o+6ZJZbKoJEkkAfDCzhmVr+XSrtR/LEM5qNXZF//H1tUFSoVZRGYgGVshi5lFzXbDUW9CDMcAzFEVxRzDlqS0ZgsGqa2aQnYAaTc7R8mfC+E1ToDVKtSqPKqyCAEJ1swIYEgw4Qm1wBtbFs8DcPZK7rmKYV3o0qmktUY8rMjnnZtI1loAixBgTjMjlzlmzVetSdQLqzGq2r86odKhmZRyeSAFAkk2OwynujNaOZZ3Mz56GnSQCTqpIqvy1acAmQYOoEyBt8RjTw9SZM3lC/6sxl2g9jUSPtH+GBOKMj1855Q5NJKAgzpFWkZ5r7Bje/1xtwdm/FZeSxCVqLXJIADqx32ECZ2wxlo6p7fqFQHJxran+eDzbEinpnr0P7Y5PX4kCdLKYIAO4MgAG+0W++Oge17xdSzzUqWXTUKYqgswg3qCCpn3SKM3/mGxGOYZ9TKrB5ZGrobi89uuANtaN8nVQZgVKslE5iAoedtMqSARJEyY6X2x0nPfl5mkjGatSv5SuEBIeo0SSX5bwTBMxjmXDs+2XrCqokqSIJK+m6mR9cXw+JynlPRpE1atKo1T8x3IIY2B1X1AJffm67YzVtDRnxTXua+IKmlspO3lFiByglajgdP+c9998Zw2nrolEoKxWD5kq1RIJblZVhQZOw3Jv1wD470LWeiF/hugQjVEFHYqbwSSV7e4e5woy2damCoWmsrpsBq7CGBkt0n1PxwqWguacmw5siEBaooZUaqGRmIHNSpwdQBggtIMb46T4O8CK1PXWVPJqUkqJrbUBqALSobS6zJGoT9Mc34ZVeuKq6GYDnYjUxMqqktuY5Fv0nHTcvxmjQXX5ehaS5ejUNRl1EeShCqgI0v5YWxk6pEAXw6Jy+AziHAaWWvl6upXeWRQFRNKhAFCiwJk/4YrXEKdZkq0kdqvIXOsgBAvuqDTQSSSYkEckEiZLDjXi7LsGRyaBpVKQJKq+oNeqIQzC6CsmJsRgfgWbCrmjUC1CtWpTAIGoqr8qjpblmQYkG8HHT4bGsk+Lfp/r/ACcuRTjLl6CfOcZq1BUpOwKU6KuDpAJYOF94C9u+LL7R/Fz0A6ZNl10U1VKm4UF6aaVmzE+YmxMc3bHOMxx2kBmKbe84ZBAsDq9OnoMLOLZynXkjluTHXaLD5/bHNKKjJo67tX+CyeEfahm0rKuYbzqbG4bf5MdjvHTC7xy1fOZ2s9LLVilRlYAU2Y2pIliB/wAp274Q5HiFKgxKozPI0vq0lY/lgWnqZ9O8nZrxfUdQja/LCuoU1ajTrIJZ2ZpZpAvaAIiwwEgN2AVeG5mgrPUy9amNtT02WLx1He3xwJmK5cDqZk/QD+mJU4mF1+XTVS6sjXdrMIIuxk7fAgEYFE/8P7HBATUFinVHop/f++PCg1U4BggdT3+2J6NCo2rRQY6rQoJ+w2wbluBZxhbL1BtGoMv72wbAC0KWp9CqzmQAqgsTeIgb9sScdyNZczW82kyVdTF1KkczcxI9DMg7QRFsWPw7VOQY/isurPGpASCViCpGk8vMs7SYFxiPxrnVzGcqOVSmxtab2AUsTJZoi/WBtbAbQdirhgHk1gf1HLqR8aoP7Lg3h3GK1PJ18stNWXMPUJYkgwwpgkCY/RucBJkEJXSQ8lV+JMLts1/3w5zHhivpOkMxiQBAmBYbxGByDxDuEcZpJRpo5IZVAImdrC/wjGYVU/BOaIBJCk9J2+hxmFDbE6cWMp5ZenpVAwR9OrlAZgYs2qTeZkdiTJxXilaoEpvVDeWxIMEETIB1TJm2+MxmHFBqWbhqTs5JpsrxG+kyeabGFHQ43z3EhUZmky+kc19lCb/BRjMZjWACrZwEAEbWn7AnuYxrSzKjof8APpjMZgmNqebgnTIkRIMbx6z98b5zjuZqahUrVGDElgXYg9LiY2tG0YzGYATUcczIbzBmKweI1eY+ojeC0yRPTHmY4zmHnXXrNO+qo7TFgTJvjzGY1GAzVPc43kkbnHmMxjElJHJtc/Ef1w4ynCw5JqvUUm5bSrXtN9cknvGMxmCCw2hwOgDIq1WI7Inw6tg6g9GnpVXrBgAFgUwbeun+uPcZjNBTPc3l6NZi9UVi0+87i+w/T1jHn4TL/wAhP/e/r/hjMZjUME5JaKtOglSCHQOVFQCSFchZKyFMT0wp415lR28t3NKQQjuWAKjQp2EkKNIJvH3zGYSbozk6oCXJVSpDEGSLCLRN74i01Na63VnD6hIJOowZJt/KvXHmMwI7FCqPgrNPdUt3LIB/7i32wXQ9nuYJ5qlJfgWY/TSP3x7jMMZbGmT9ngHv1A3wUj93I+2GVPwRl0PONUd4H/sAx7jMYNBdLg+TQWpA/EFvu5OCKS01/h0UUeiqP2AxmMxjGHN1CSAbWtAP0nHk1OrEH0+Hp1nHuMxgpFW41T1V2m5gD12kX+mAc0B57nTJhOsdCP6YzGYAQPP1no+XVi61EaJFyoJHcdMWzgHjKpWUgodSxMEbHYyYnbHmMxn0D1HA44/8r/VMZjMZhQn/2Q==">\n        </ion-col>\n        <ion-col col-9>\n          <h2>Schwabentor</h2>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <ion-card (click)="attractionDetail()">\n    <ion-grid>\n      <ion-row>\n        <ion-col col-3>\n          <img src="https://media-cdn.tripadvisor.com/media/photo-f/12/fc/1e/85/schlossberg.jpg">\n        </ion-col>\n        <ion-col col-9>\n          <h2>Schlossberg</h2>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/tourist-attractions/tourist-attractions.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], TouristAttractionsPage);
    return TouristAttractionsPage;
}());

//# sourceMappingURL=tourist-attractions.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserAdministrationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the UserAdministrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var UserAdministrationPage = /** @class */ (function () {
    function UserAdministrationPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    UserAdministrationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UserAdministrationPage');
    };
    UserAdministrationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-user-administration',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/user-administration/user-administration.html"*/'<!--\n  Generated template for the UserAdministrationPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Account</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/user-administration/user-administration.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], UserAdministrationPage);
    return UserAdministrationPage;
}());

//# sourceMappingURL=user-administration.js.map

/***/ }),

/***/ 220:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 220;

/***/ }),

/***/ 261:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/administration/administration.module": [
		803,
		23
	],
	"../pages/attraction-detail/attraction-detail.module": [
		804,
		22
	],
	"../pages/dump/dump.module": [
		805,
		21
	],
	"../pages/dumpcalendar/dumpcalendar.module": [
		806,
		20
	],
	"../pages/events/events.module": [
		807,
		19
	],
	"../pages/flat-search/flat-search.module": [
		808,
		18
	],
	"../pages/handicaped/handicaped.module": [
		809,
		17
	],
	"../pages/idea/idea.module": [
		810,
		16
	],
	"../pages/kita-map/kita-map.module": [
		811,
		15
	],
	"../pages/logout/logout.module": [
		812,
		14
	],
	"../pages/news-detail/news-detail.module": [
		813,
		13
	],
	"../pages/news/news.module": [
		814,
		12
	],
	"../pages/park-areas/park-areas.module": [
		816,
		11
	],
	"../pages/park-detail/park-detail.module": [
		815,
		10
	],
	"../pages/parking-permit/parking-permit.module": [
		817,
		9
	],
	"../pages/parking/parking.module": [
		818,
		8
	],
	"../pages/play-grounds/play-grounds.module": [
		819,
		7
	],
	"../pages/pushnotifications/pushnotifications.module": [
		820,
		6
	],
	"../pages/real-estate/real-estate.module": [
		821,
		1
	],
	"../pages/sport-areas/sport-areas.module": [
		822,
		5
	],
	"../pages/sportclubs/sportclubs.module": [
		824,
		4
	],
	"../pages/student-permit/student-permit.module": [
		823,
		0
	],
	"../pages/tourist-attractions/tourist-attractions.module": [
		825,
		3
	],
	"../pages/user-administration/user-administration.module": [
		826,
		2
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 261;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 34:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KitaMapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(262);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var directionsService = new google.maps.DirectionsService;
var directionsDisplay = new google.maps.DirectionsRenderer({ suppressMarkers: true });
/**
 * Generated class for the KitaMapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var KitaMapPage = /** @class */ (function () {
    function KitaMapPage(navCtrl, navParams, geolocation, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.geolocation = geolocation;
        this.loadingCtrl = loadingCtrl;
        this.markers = [];
        this.blitzerMarker = [["47.990012", "7.836557", "Geschwindigkeitskontrolle 50 km/h"],
            ["47.990659", "7.856998", "Rotlichtkontrolle"],
            ["47.999056", "7.846157", "Geschwindigkeits- und Rotlichtkontrolle 50 km/h"],
            ["47.990835", "7.853392", "Geschwindigkeitskontrolle 22-6 Uhr 30km/h, 6-22 Uhr 50km/h"],
            ["47.988948", "7.881811", "Rotlichtkontrolle"],
            ["47.986351", "7.871511", "Geschwindigkeitskontrolle 50 km/h"],
            ["48.024957", "7.856532", "Rotlichtkontrolle"],
            ["47.994564", "7.856235", "Geschwindigkeitskontrolle 50 km/h"],
            ["47.993090", "7.832336", "Geschwindigkeitskontrolle 22-6 Uhr 30km/h, 6-22 Uhr 50km/h"],
            ["47.994985", "7.828762", "Geschwindigkeitskontrolle 60 km/h"]];
        this.kitaMarker = [["47.993090", "7.852925", "Katholischer Kindergarten Unserer Lieben Frau"],
            ["48.010120", "7.808920", "Kindergarten Matth‰usgemeinde"],
            ["48.008984", "7.850273", "Katholischer Kindergarten St.Konrad-St.Hildegard"],
            ["47.986700", "7.846640", "Kindergarten Sankt Raphael"],
            ["48.004050", "7.816290", "Freiburger Kinderhausinitiative e.V."],
            ["52.207663", "8.065830", "Katholischer Kindergarten St.Marien"],
            ["48.006855", "7.832122", "Kindergarten - Sozial- und Jugendzentrum Breisacher Hof"],
            ["48.003090", "7.822769", "JHW - Kita Lˆwenzahn"],
            ["48.001040", "7.840690", "Uni-Kita Zaubergarten"],
            ["47.997020", "7.835610", "St‰dt. Sch¸lerhort Herz Jesu"],
            ["53.655216", "10.097817", "Kath. Kindergarten St. Bernhard"]];
        this.parkAndRideMarker = [["48.002050", "7.820060", "Bissierstraﬂe 2B, 79114 Freiburg im Breisgau"],
            ["48.002370", "7.823790", "Bissierstraﬂe 7, 79114 Freiburg im Breisgau"],
            ["47.988679", "7.795437", "Munzinger Str. 1, 79111 Freiburg im Breisgau"],
            ["48.033938", "7.863533", "Gundelfinger Str. 55, 79108 Freiburg im Breisgau"],
            ["48.028070", "7.809071", "Els‰sser Str. 100, 79110 Freiburg im Breisgau"],
            ["48.013487", "7.806344", "Breisgauer Str. 1, 79110 Freiburg im Breisgau"],
            ["47.996742", "7.790783", "Ingeborg-Drewitz-Allee 11, 79111 Freiburg im Breisgau"]];
    }
    KitaMapPage.prototype.ionViewDidLoad = function () {
        this.presentLoadingDefault();
        this.loadMap();
        console.log('ionViewDidLoad KitaMapPage');
    };
    KitaMapPage.prototype.presentLoadingDefault = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Einen Moment Bitte :)'
        });
        this.loading.present();
    };
    KitaMapPage.prototype.loadMap = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (position) {
            _this.loading.dismiss();
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            _this.currentPosition = { lat: lat, lng: lng };
            var latLng = new google.maps.LatLng(lat, lng);
            var mapOptions = {
                center: latLng,
                zoom: 12,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            _this.map = new google.maps.Map(_this.mapElement.nativeElement, mapOptions);
        }, function (err) {
            console.log(err);
        });
    };
    KitaMapPage.prototype.show = function (poi, fab) {
        fab.close();
        var pois;
        this.map.setZoom(12);
        this.clear();
        var type;
        switch (poi) {
            case "Blitzer":
                pois = this.blitzerMarker;
                type = "blitzer";
                break;
            case "KiTa":
                pois = this.kitaMarker;
                type = "kita";
                break;
            case "P&R":
                pois = this.parkAndRideMarker;
                type = "parking";
                break;
            default:
                pois = this.parkAndRideMarker;
                type = "parking";
                break;
        }
        var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
        var icons = {
            parking: {
                icon: iconBase + 'parking_lot_maps.png'
            },
            kita: {
                icon: iconBase + 'library_maps.png'
            },
            blitzer: {
                icon: '../../assets/icon/blitzer.png'
            }
        };
        for (var _i = 0, pois_1 = pois; _i < pois_1.length; _i++) {
            var point = pois_1[_i];
            var marker = new google.maps.Marker({
                map: this.map,
                animation: google.maps.Animation.DROP,
                position: new google.maps.LatLng(point[0], point[1]),
                description: point[2],
                icon: icons[type].icon
            });
            this.markers.push(marker);
            var content = "<div #directionsPanel><h4>" + poi + "!</h4> <br> <h5>" + marker.description + "</h5></div>";
            this.addInfoWindow(marker, content);
        }
    };
    KitaMapPage.prototype.clear = function () {
        directionsDisplay.setMap(null);
        this.markerClicked = false;
        if (this.navInfo) {
            this.navInfo.close();
        }
        for (var _i = 0, _a = this.markers; _i < _a.length; _i++) {
            var marker = _a[_i];
            marker.setMap(null);
        }
    };
    KitaMapPage.prototype.startNavigating = function () {
        var _this = this;
        this.infoWindow.close();
        this.markerClicked = false;
        directionsDisplay.setMap(this.map);
        directionsDisplay.setPanel(document.getElementById('directionsPanel'));
        directionsService.route({
            origin: this.currentPosition,
            destination: this.currentMarker,
            travelMode: google.maps.TravelMode['WALKING']
        }, function (res, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(res);
                var step = 1;
                if (_this.navInfo) {
                    _this.navInfo.close();
                    _this.infoWindow.close();
                }
                _this.navInfo = new google.maps.InfoWindow();
                _this.navInfo.setContent(res.routes[0].legs[0].distance.text + "<br>" + res.routes[0].legs[0].duration.text + " ");
                _this.navInfo.setPosition(res.routes[0].legs[0].steps[step].end_location);
                _this.navInfo.open(_this.map);
            }
            else {
                console.warn(status);
            }
        });
    };
    KitaMapPage.prototype.addInfoWindow = function (marker, content) {
        var _this = this;
        var infoWindow = new google.maps.InfoWindow({
            content: content,
        });
        google.maps.event.addListener(marker, 'click', function () {
            directionsDisplay.setMap(null);
            _this.markerClicked = true;
            _this.currentMarker = marker.position;
            if (_this.navInfo) {
                _this.navInfo.close();
                _this.infoWindow.close();
            }
            if (_this.infoWindow) {
                _this.infoWindow.close();
            }
            infoWindow.open(_this.map, marker);
            _this.infoWindow = infoWindow;
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], KitaMapPage.prototype, "mapElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('directionsPanel'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], KitaMapPage.prototype, "directionsPanel", void 0);
    KitaMapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-kita-map',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/kita-map/kita-map.html"*/'<!--\n  Generated template for the KitaMapPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Karte</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n<ion-content padding>\n\n  <div #map id="map"></div>\n    <button *ngIf="markerClicked" id="navButton" (click)="startNavigating()" ion-button icon-only> <ion-icon name="navigate"></ion-icon></button>\n  <ion-fab  bottom left #fab>\n   <button color="light" ion-fab><ion-icon  name="add"></ion-icon></button>\n   <ion-fab-list  side="right">\n     <button ion-fab (click)="show(\'KiTa\', fab)">KiTa</button>\n     <button ion-fab (click)="show(\'Sport\' ,fab)">Sport</button>\n     <button ion-fab (click)="show(\'Spielplatz\', fab)">Spielplatz</button>\n     \n   </ion-fab-list>\n   <ion-fab-list  side="top">\n     <button (click)="show(\'Blitzer\', fab)" ion-fab>Blitzer</button>\n     <button (click)="show(\'P&R\', fab)" ion-fab>P & R</button>\n     <button (click)="show(\'Parkhaus\', fab)" ion-fab>PLS</button>\n   </ion-fab-list>\n </ion-fab> \n  \n\n\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/kita-map/kita-map.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], KitaMapPage);
    return KitaMapPage;
}());

//# sourceMappingURL=kita-map.js.map

/***/ }),

/***/ 375:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__parking_parking__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__news_news__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dump_dump__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__events_events__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__administration_administration__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__park_areas_park_areas__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__play_grounds_play_grounds__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__sport_areas_sport_areas__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__sportclubs_sportclubs__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__parking_permit_parking_permit__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__kita_map_kita_map__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__handicaped_handicaped__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__tourist_attractions_tourist_attractions__ = __webpack_require__(210);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.pages = [
            { title: 'Parkhäuser', icon: 'ios-car', component: __WEBPACK_IMPORTED_MODULE_2__parking_parking__["a" /* ParkingPage */], isShown: true },
            { title: 'News', icon: 'md-paper', component: __WEBPACK_IMPORTED_MODULE_3__news_news__["a" /* NewsPage */], isShown: true },
            { title: 'Müll', icon: 'ios-basket', component: __WEBPACK_IMPORTED_MODULE_4__dump_dump__["a" /* DumpPage */], isShown: true },
            { title: 'Events', icon: 'md-beer', component: __WEBPACK_IMPORTED_MODULE_5__events_events__["a" /* EventsPage */], isShown: true },
            { title: 'Attraktionen', icon: 'ios-camera', component: __WEBPACK_IMPORTED_MODULE_14__tourist_attractions_tourist_attractions__["a" /* TouristAttractionsPage */], isShown: true },
            { title: 'Verwaltung', icon: 'ios-archive', component: __WEBPACK_IMPORTED_MODULE_6__administration_administration__["a" /* AdministrationPage */], isShown: true },
            { title: 'Parkausweis', icon: 'ios-card', component: __WEBPACK_IMPORTED_MODULE_11__parking_permit_parking_permit__["a" /* ParkingPermitPage */], isShown: true },
            { title: 'Parkanlagen', icon: 'ios-leaf', component: __WEBPACK_IMPORTED_MODULE_7__park_areas_park_areas__["a" /* ParkAreasPage */], isShown: false },
            { title: 'Spielplätze', icon: 'md-happy', component: __WEBPACK_IMPORTED_MODULE_8__play_grounds_play_grounds__["a" /* PlayGroundsPage */], isShown: false },
            { title: 'Kita Karte', icon: 'ios-heart', component: __WEBPACK_IMPORTED_MODULE_12__kita_map_kita_map__["a" /* KitaMapPage */], isShown: false },
            { title: 'Sportplätze', icon: 'ios-american-football', component: __WEBPACK_IMPORTED_MODULE_9__sport_areas_sport_areas__["a" /* SportAreasPage */], isShown: false },
            { title: 'Sportvereine', icon: 'ios-american-football', component: __WEBPACK_IMPORTED_MODULE_10__sportclubs_sportclubs__["a" /* SportclubsPage */], isShown: false },
            { title: 'Barrierefreiheit', icon: 'ios-medkit', component: __WEBPACK_IMPORTED_MODULE_13__handicaped_handicaped__["a" /* HandicapedPage */], isShown: true }
        ];
    }
    HomePage.prototype.goToPage = function (page) {
        this.navCtrl.push(page.component);
    };
    HomePage.prototype.getActivePages = function () {
        var activePages = [];
        for (var _i = 0, _a = this.pages; _i < _a.length; _i++) {
            var page = _a[_i];
            if (page.isShown) {
                activePages.push(page);
            }
        }
        return activePages;
    };
    HomePage.prototype.doCustomizing = function () {
        var _this = this;
        var alert = this.alertCtrl.create();
        alert.setTitle('Welche Kacheln möchten Sie sehen?');
        for (var _i = 0, _a = this.pages; _i < _a.length; _i++) {
            var page = _a[_i];
            alert.addInput({
                type: 'checkbox',
                label: page.title,
                value: "",
                checked: page.isShown
            });
        }
        alert.addButton('Cancel');
        alert.addButton({
            text: 'Okay',
            handler: function (data) {
                var i = 0;
                for (var _i = 0, _a = alert.data.inputs; _i < _a.length; _i++) {
                    var box = _a[_i];
                    _this.pages[i].isShown = box.checked;
                    i++;
                }
            }
        });
        alert.present();
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Freiburg Connect</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="doCustomizing()">\n        <ion-icon name="construct"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col col-6 *ngFor="let page of getActivePages()" (click)="goToPage(page)">\n        <ion-card>\n          <ion-card-header>\n            <ion-icon name="{{page.icon}}"></ion-icon>\n          </ion-card-header>\n          <ion-card-content>\n            {{page.title}}\n          </ion-card-content>\n        </ion-card>\n\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 503:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FlatSearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__kita_map_kita_map__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the FlatSearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FlatSearchPage = /** @class */ (function () {
    function FlatSearchPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FlatSearchPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FlatSearchPage');
    };
    FlatSearchPage.prototype.goToMap = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__kita_map_kita_map__["a" /* KitaMapPage */]);
    };
    FlatSearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-flat-search',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/flat-search/flat-search.html"*/'<!--\n  Generated template for the FlatSearchPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Immobilien</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only>\n        <ion-icon name="plus"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n\n</ion-header>\n\n\n<ion-content>\n  <button ion-button block (click)="goToMap()"><ion-icon name="ios-map-outline"> </ion-icon>Auf Karte anzeigen </button>\n  <ion-card>\n    <ion-grid>\n      <ion-row>\n        <ion-col col-3>\n          <img src="../../assets/imgs/wohnung1.jpg">\n        </ion-col>\n        <ion-col col-9>\n          <h2>Schöne Wohnung</h2>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <ion-card>\n    <ion-grid>\n      <ion-row>\n        <ion-col col-3>\n          <img src="../../assets/imgs/wohnung2.jpeg">\n        </ion-col>\n        <ion-col col-9>\n          <h2>Kleine Wohnung in Freiburg</h2>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/flat-search/flat-search.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], FlatSearchPage);
    return FlatSearchPage;
}());

//# sourceMappingURL=flat-search.js.map

/***/ }),

/***/ 504:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(505);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(527);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 527:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(797);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_parking_parking__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_shard_alert_shard_alert__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_news_news__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_dump_dump__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_events_events__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_parking_permit_parking_permit__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_administration_administration__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_park_areas_park_areas__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_play_grounds_play_grounds__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_sport_areas_sport_areas__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_sportclubs_sportclubs__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_kita_map_kita_map__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_handicaped_handicaped__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_geolocation__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_dumpcalendar_dumpcalendar__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22_ion2_calendar__ = __webpack_require__(798);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22_ion2_calendar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_22_ion2_calendar__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_news_detail_news_detail__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_tourist_attractions_tourist_attractions__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_idea_idea__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_pushnotifications_pushnotifications__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_user_administration_user_administration__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_logout_logout__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_attraction_detail_attraction_detail__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_park_detail_park_detail__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_flat_search_flat_search__ = __webpack_require__(503);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_parking_parking__["a" /* ParkingPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_news_news__["a" /* NewsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_dump_dump__["a" /* DumpPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_events_events__["a" /* EventsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_parking_permit_parking_permit__["a" /* ParkingPermitPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_shard_alert_shard_alert__["a" /* ShardAlertPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_administration_administration__["a" /* AdministrationPage */],
                //StudentPermitPage,
                __WEBPACK_IMPORTED_MODULE_14__pages_park_areas_park_areas__["a" /* ParkAreasPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_play_grounds_play_grounds__["a" /* PlayGroundsPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_sport_areas_sport_areas__["a" /* SportAreasPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_sportclubs_sportclubs__["a" /* SportclubsPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_sport_areas_sport_areas__["a" /* SportAreasPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_kita_map_kita_map__["a" /* KitaMapPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_dumpcalendar_dumpcalendar__["a" /* DumpcalendarPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_handicaped_handicaped__["a" /* HandicapedPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_dumpcalendar_dumpcalendar__["a" /* DumpcalendarPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_news_detail_news_detail__["a" /* NewsDetailPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_tourist_attractions_tourist_attractions__["a" /* TouristAttractionsPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_idea_idea__["a" /* IdeaPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_pushnotifications_pushnotifications__["a" /* PushnotificationsPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_user_administration_user_administration__["a" /* UserAdministrationPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_logout_logout__["a" /* LogoutPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_attraction_detail_attraction_detail__["a" /* AttractionDetailPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_park_detail_park_detail__["a" /* ParkDetailPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_flat_search_flat_search__["a" /* FlatSearchPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/administration/administration.module#AdministrationPageModule', name: 'AdministrationPage', segment: 'administration', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/attraction-detail/attraction-detail.module#AttractionDetailPageModule', name: 'AttractionDetailPage', segment: 'attraction-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dump/dump.module#DumpPageModule', name: 'DumpPage', segment: 'dump', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dumpcalendar/dumpcalendar.module#DumpcalendarPageModule', name: 'DumpcalendarPage', segment: 'dumpcalendar', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/events/events.module#EventsPageModule', name: 'EventsPage', segment: 'events', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/flat-search/flat-search.module#FlatSearchPageModule', name: 'FlatSearchPage', segment: 'flat-search', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/handicaped/handicaped.module#HandicapedPageModule', name: 'HandicapedPage', segment: 'handicaped', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/idea/idea.module#IdeaPageModule', name: 'IdeaPage', segment: 'idea', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/kita-map/kita-map.module#KitaMapPageModule', name: 'KitaMapPage', segment: 'kita-map', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/logout/logout.module#LogoutPageModule', name: 'LogoutPage', segment: 'logout', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/news-detail/news-detail.module#NewsDetailPageModule', name: 'NewsDetailPage', segment: 'news-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/news/news.module#NewsPageModule', name: 'NewsPage', segment: 'news', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/park-detail/park-detail.module#ParkDetailPageModule', name: 'ParkDetailPage', segment: 'park-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/park-areas/park-areas.module#ParkAreasPageModule', name: 'ParkAreasPage', segment: 'park-areas', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/parking-permit/parking-permit.module#ParkingPermitPageModule', name: 'ParkingPermitPage', segment: 'parking-permit', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/parking/parking.module#ParkingPageModule', name: 'ParkingPage', segment: 'parking', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/play-grounds/play-grounds.module#PlayGroundsPageModule', name: 'PlayGroundsPage', segment: 'play-grounds', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pushnotifications/pushnotifications.module#PushnotificationsPageModule', name: 'PushnotificationsPage', segment: 'pushnotifications', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/real-estate/real-estate.module#RealEstatePageModule', name: 'RealEstatePage', segment: 'real-estate', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sport-areas/sport-areas.module#SportAreasPageModule', name: 'SportAreasPage', segment: 'sport-areas', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/student-permit/student-permit.module#StudentPermitPageModule', name: 'StudentPermitPage', segment: 'student-permit', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sportclubs/sportclubs.module#SportclubsPageModule', name: 'SportclubsPage', segment: 'sportclubs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tourist-attractions/tourist-attractions.module#TouristAttractionsPageModule', name: 'TouristAttractionsPage', segment: 'tourist-attractions', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/user-administration/user-administration.module#UserAdministrationPageModule', name: 'UserAdministrationPage', segment: 'user-administration', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_22_ion2_calendar__["CalendarModule"]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_parking_parking__["a" /* ParkingPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_news_news__["a" /* NewsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_dump_dump__["a" /* DumpPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_events_events__["a" /* EventsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_parking_permit_parking_permit__["a" /* ParkingPermitPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_shard_alert_shard_alert__["a" /* ShardAlertPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_administration_administration__["a" /* AdministrationPage */],
                //StudentPermitPage,
                __WEBPACK_IMPORTED_MODULE_14__pages_park_areas_park_areas__["a" /* ParkAreasPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_play_grounds_play_grounds__["a" /* PlayGroundsPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_sport_areas_sport_areas__["a" /* SportAreasPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_kita_map_kita_map__["a" /* KitaMapPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_sport_areas_sport_areas__["a" /* SportAreasPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_sportclubs_sportclubs__["a" /* SportclubsPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_dumpcalendar_dumpcalendar__["a" /* DumpcalendarPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_handicaped_handicaped__["a" /* HandicapedPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_dumpcalendar_dumpcalendar__["a" /* DumpcalendarPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_news_detail_news_detail__["a" /* NewsDetailPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_tourist_attractions_tourist_attractions__["a" /* TouristAttractionsPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_idea_idea__["a" /* IdeaPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_pushnotifications_pushnotifications__["a" /* PushnotificationsPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_user_administration_user_administration__["a" /* UserAdministrationPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_logout_logout__["a" /* LogoutPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_attraction_detail_attraction_detail__["a" /* AttractionDetailPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_park_detail_park_detail__["a" /* ParkDetailPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_flat_search_flat_search__["a" /* FlatSearchPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_geolocation__["a" /* Geolocation */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 585:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 587:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 619:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 620:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 687:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 778:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return parkingHouse; });
var parkingHouse = /** @class */ (function () {
    function parkingHouse(max, current, title, loadPercent) {
        this.max = max;
        this.current = current;
        this.title = title;
        this.loadPercent = loadPercent;
        this.name = title;
        this.maxSpots = max;
        this.currentSpots = current;
        this.percent = loadPercent;
    }
    return parkingHouse;
}());

//# sourceMappingURL=parkingHouse.js.map

/***/ }),

/***/ 779:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PARKING_HOUSE_URL; });
/* unused harmony export BLITZER_URL */
var PARKING_HOUSE_URL = 'http://localhost:8100/parkingHouses';
var BLITZER_URL = 'http://localhost:8100/blitzer';
//# sourceMappingURL=constants.js.map

/***/ }),

/***/ 797:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_idea_idea__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_pushnotifications_pushnotifications__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_user_administration_user_administration__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_logout_logout__ = __webpack_require__(200);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Idee einreichen', component: __WEBPACK_IMPORTED_MODULE_5__pages_idea_idea__["a" /* IdeaPage */], icon: 'ios-bulb-outline' },
            { title: 'Benachrichtigungen', component: __WEBPACK_IMPORTED_MODULE_6__pages_pushnotifications_pushnotifications__["a" /* PushnotificationsPage */], icon: 'ios-notifications-outline' },
            { title: 'Account', component: __WEBPACK_IMPORTED_MODULE_7__pages_user_administration_user_administration__["a" /* UserAdministrationPage */], icon: 'ios-person-outline' },
            { title: 'Logout', component: __WEBPACK_IMPORTED_MODULE_8__pages_logout_logout__["a" /* LogoutPage */], icon: 'ios-log-out-outline' },
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.setHome = function () {
        this.nav.root(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.push(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/app/app.html"*/'<ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item (click)="setHome()">\n        <ion-icon name="ios-home-outline"></ion-icon> &nbsp; Home\n      </button>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        <ion-icon name="{{p.icon}}"></ion-icon> &nbsp; {{p.title}}\n      </button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 799:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 376,
	"./af.js": 376,
	"./ar": 377,
	"./ar-dz": 378,
	"./ar-dz.js": 378,
	"./ar-kw": 379,
	"./ar-kw.js": 379,
	"./ar-ly": 380,
	"./ar-ly.js": 380,
	"./ar-ma": 381,
	"./ar-ma.js": 381,
	"./ar-sa": 382,
	"./ar-sa.js": 382,
	"./ar-tn": 383,
	"./ar-tn.js": 383,
	"./ar.js": 377,
	"./az": 384,
	"./az.js": 384,
	"./be": 385,
	"./be.js": 385,
	"./bg": 386,
	"./bg.js": 386,
	"./bm": 387,
	"./bm.js": 387,
	"./bn": 388,
	"./bn.js": 388,
	"./bo": 389,
	"./bo.js": 389,
	"./br": 390,
	"./br.js": 390,
	"./bs": 391,
	"./bs.js": 391,
	"./ca": 392,
	"./ca.js": 392,
	"./cs": 393,
	"./cs.js": 393,
	"./cv": 394,
	"./cv.js": 394,
	"./cy": 395,
	"./cy.js": 395,
	"./da": 396,
	"./da.js": 396,
	"./de": 397,
	"./de-at": 398,
	"./de-at.js": 398,
	"./de-ch": 399,
	"./de-ch.js": 399,
	"./de.js": 397,
	"./dv": 400,
	"./dv.js": 400,
	"./el": 401,
	"./el.js": 401,
	"./en-au": 402,
	"./en-au.js": 402,
	"./en-ca": 403,
	"./en-ca.js": 403,
	"./en-gb": 404,
	"./en-gb.js": 404,
	"./en-ie": 405,
	"./en-ie.js": 405,
	"./en-il": 406,
	"./en-il.js": 406,
	"./en-nz": 407,
	"./en-nz.js": 407,
	"./eo": 408,
	"./eo.js": 408,
	"./es": 409,
	"./es-do": 410,
	"./es-do.js": 410,
	"./es-us": 411,
	"./es-us.js": 411,
	"./es.js": 409,
	"./et": 412,
	"./et.js": 412,
	"./eu": 413,
	"./eu.js": 413,
	"./fa": 414,
	"./fa.js": 414,
	"./fi": 415,
	"./fi.js": 415,
	"./fo": 416,
	"./fo.js": 416,
	"./fr": 417,
	"./fr-ca": 418,
	"./fr-ca.js": 418,
	"./fr-ch": 419,
	"./fr-ch.js": 419,
	"./fr.js": 417,
	"./fy": 420,
	"./fy.js": 420,
	"./gd": 421,
	"./gd.js": 421,
	"./gl": 422,
	"./gl.js": 422,
	"./gom-latn": 423,
	"./gom-latn.js": 423,
	"./gu": 424,
	"./gu.js": 424,
	"./he": 425,
	"./he.js": 425,
	"./hi": 426,
	"./hi.js": 426,
	"./hr": 427,
	"./hr.js": 427,
	"./hu": 428,
	"./hu.js": 428,
	"./hy-am": 429,
	"./hy-am.js": 429,
	"./id": 430,
	"./id.js": 430,
	"./is": 431,
	"./is.js": 431,
	"./it": 432,
	"./it.js": 432,
	"./ja": 433,
	"./ja.js": 433,
	"./jv": 434,
	"./jv.js": 434,
	"./ka": 435,
	"./ka.js": 435,
	"./kk": 436,
	"./kk.js": 436,
	"./km": 437,
	"./km.js": 437,
	"./kn": 438,
	"./kn.js": 438,
	"./ko": 439,
	"./ko.js": 439,
	"./ky": 440,
	"./ky.js": 440,
	"./lb": 441,
	"./lb.js": 441,
	"./lo": 442,
	"./lo.js": 442,
	"./lt": 443,
	"./lt.js": 443,
	"./lv": 444,
	"./lv.js": 444,
	"./me": 445,
	"./me.js": 445,
	"./mi": 446,
	"./mi.js": 446,
	"./mk": 447,
	"./mk.js": 447,
	"./ml": 448,
	"./ml.js": 448,
	"./mn": 449,
	"./mn.js": 449,
	"./mr": 450,
	"./mr.js": 450,
	"./ms": 451,
	"./ms-my": 452,
	"./ms-my.js": 452,
	"./ms.js": 451,
	"./mt": 453,
	"./mt.js": 453,
	"./my": 454,
	"./my.js": 454,
	"./nb": 455,
	"./nb.js": 455,
	"./ne": 456,
	"./ne.js": 456,
	"./nl": 457,
	"./nl-be": 458,
	"./nl-be.js": 458,
	"./nl.js": 457,
	"./nn": 459,
	"./nn.js": 459,
	"./pa-in": 460,
	"./pa-in.js": 460,
	"./pl": 461,
	"./pl.js": 461,
	"./pt": 462,
	"./pt-br": 463,
	"./pt-br.js": 463,
	"./pt.js": 462,
	"./ro": 464,
	"./ro.js": 464,
	"./ru": 465,
	"./ru.js": 465,
	"./sd": 466,
	"./sd.js": 466,
	"./se": 467,
	"./se.js": 467,
	"./si": 468,
	"./si.js": 468,
	"./sk": 469,
	"./sk.js": 469,
	"./sl": 470,
	"./sl.js": 470,
	"./sq": 471,
	"./sq.js": 471,
	"./sr": 472,
	"./sr-cyrl": 473,
	"./sr-cyrl.js": 473,
	"./sr.js": 472,
	"./ss": 474,
	"./ss.js": 474,
	"./sv": 475,
	"./sv.js": 475,
	"./sw": 476,
	"./sw.js": 476,
	"./ta": 477,
	"./ta.js": 477,
	"./te": 478,
	"./te.js": 478,
	"./tet": 479,
	"./tet.js": 479,
	"./tg": 480,
	"./tg.js": 480,
	"./th": 481,
	"./th.js": 481,
	"./tl-ph": 482,
	"./tl-ph.js": 482,
	"./tlh": 483,
	"./tlh.js": 483,
	"./tr": 484,
	"./tr.js": 484,
	"./tzl": 485,
	"./tzl.js": 485,
	"./tzm": 486,
	"./tzm-latn": 487,
	"./tzm-latn.js": 487,
	"./tzm.js": 486,
	"./ug-cn": 488,
	"./ug-cn.js": 488,
	"./uk": 489,
	"./uk.js": 489,
	"./ur": 490,
	"./ur.js": 490,
	"./uz": 491,
	"./uz-latn": 492,
	"./uz-latn.js": 492,
	"./uz.js": 491,
	"./vi": 493,
	"./vi.js": 493,
	"./x-pseudo": 494,
	"./x-pseudo.js": 494,
	"./yo": 495,
	"./yo.js": 495,
	"./zh-cn": 496,
	"./zh-cn.js": 496,
	"./zh-hk": 497,
	"./zh-hk.js": 497,
	"./zh-tw": 498,
	"./zh-tw.js": 498
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 799;

/***/ })

},[504]);
//# sourceMappingURL=main.js.map