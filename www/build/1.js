webpackJsonp([1],{

/***/ 821:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RealEstatePageModule", function() { return RealEstatePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__real_estate__ = __webpack_require__(827);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RealEstatePageModule = /** @class */ (function () {
    function RealEstatePageModule() {
    }
    RealEstatePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__real_estate__["a" /* RealEstatePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__real_estate__["a" /* RealEstatePage */]),
            ],
        })
    ], RealEstatePageModule);
    return RealEstatePageModule;
}());

//# sourceMappingURL=real-estate.module.js.map

/***/ }),

/***/ 827:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RealEstatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the RealEstatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RealEstatePage = /** @class */ (function () {
    function RealEstatePage(navCtrl, navParams, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
    }
    RealEstatePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RealEstatePage');
    };
    RealEstatePage.prototype.sendForm = function () {
        var toast = this.toastCtrl.create({
            message: 'Formular wurde erfolgeich übermittelt',
            duration: 3000,
            position: "top"
        });
        this.navCtrl.pop();
        toast.present();
    };
    RealEstatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-real-estate',template:/*ion-inline-start:"/Users/davidfrey/hackathon-2018/src/pages/real-estate/real-estate.html"*/'<!--\n  Generated template for the RealEstatePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n	<ion-navbar>\n		<ion-title>Mietspiegel</ion-title>\n	</ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n	<ion-slides pager>\n\n		<ion-slide>\n			<ion-card>\n				<ion-card-header>\n					Einwilligungserklärung\n				</ion-card-header>\n				<p>\n					Für den Schutz Ihrer personenbezogenen Daten haben wir alle technischen und organisatorischen\n					Maßnahmen getroffen, um ein hohes Schutzniveau zu schaffen. Wir halten uns dabei strikt an die\n					Datenschutzgesetze und die sonstigen datenschutzrelevaten Vorschriften. Ihre Daten werden\n					ausschließlich über sichere Kommunikationswege an die zuständige Stelle übergeben.\n\n					Zur Bearbeitung Ihres Anliegens werden personenbezogene Daten von Ihnen erhoben wie z.B. Name,\n					Anschrift, Kontaktdaten sowie die notwendigen Angaben zur Bearbeitung. Die Verwendung oder\n					Weitergabe Ihrer Daten an unbeteiligte Dritte wird ausgeschlossen.\n				</p>\n				<ion-item>\n					<ion-label>Ich bin damit einverstanden</ion-label>\n					<ion-checkbox color="secondary" checked="false"></ion-checkbox>\n				</ion-item>\n			</ion-card>\n		</ion-slide>\n\n		<ion-slide>\n			<ion-card>\n				<ion-card-header>\n					Erklärung\n				</ion-card-header>\n				<p>\n					Sehr geehrte Damen und Herren,<br>\n					nachfolgend können Sie den aktuellen Mietspiegel als PDF <b>ohne Rechnung</b> für 7,50 € bestellen. Im Anschluss an Ihre Bestellung erhalten Sie innerhalb weniger Minuten eine Bestellbestätigung per E-Mail zugesandt. Diese enthält den aktuellen Mietspiegel als PDF-Datei.\n				</p>\n				<br>\n			</ion-card>\n		</ion-slide>\n\n		<ion-slide>\n			<ion-card>\n				<ion-card-header>\n					Angaben des Zahlungspflichtigen\n				</ion-card-header>\n				<ion-list inset>\n					<ion-item>\n						<ion-label>Anrede</ion-label>\n						<ion-input type="text"></ion-input>\n					</ion-item>\n					<ion-item>\n						<ion-label>Name</ion-label>\n						<ion-input type="text"></ion-input>\n					</ion-item>\n					<ion-item>\n						<ion-label>Vorname</ion-label>\n						<ion-input type="text"></ion-input>\n					</ion-item>\n					<ion-item>\n						<ion-label>Straße</ion-label>\n						<ion-input type="text"></ion-input>\n					</ion-item>\n					<ion-item>\n						<ion-label>Hausnummer</ion-label>\n						<ion-input type="text"></ion-input>\n					</ion-item>\n					<ion-item>\n						<ion-label>Zusatz</ion-label>\n						<ion-input type="text"></ion-input>\n					</ion-item>\n					<ion-item>\n						<ion-label>Postleitzahl</ion-label>\n						<ion-input type="text"></ion-input>\n					</ion-item>\n					<ion-item>\n						<ion-label>Wohnort</ion-label>\n						<ion-input type="text"></ion-input>\n					</ion-item>\n					<ion-item>\n						<ion-label>E-Mail</ion-label>\n						<ion-input type="text"></ion-input>\n					</ion-item>\n				</ion-list>\n			</ion-card>\n		</ion-slide>\n		<ion-slide>\n			<ion-card>\n				<ion-card-header>\n					Bestellung des aktuellen Mietspiegels\n				</ion-card-header>\n				<p>\n					Ihre Daten werden TSL-verschlüsselt übertragen und dem Empfänger elektronisch zur Verfügung gestellt.\n					Im Anschluss an das Einreichen werden Sie aufgefordert für die jeweilige Dienstleistung online zu bezahlen.\n				</p>\n				<button id="sendButton" ion-button color="secondary" (click)="sendForm()">Bestätigung</button>\n			</ion-card>\n		</ion-slide>\n	</ion-slides>\n\n</ion-content>\n'/*ion-inline-end:"/Users/davidfrey/hackathon-2018/src/pages/real-estate/real-estate.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], RealEstatePage);
    return RealEstatePage;
}());

//# sourceMappingURL=real-estate.js.map

/***/ })

});
//# sourceMappingURL=1.js.map