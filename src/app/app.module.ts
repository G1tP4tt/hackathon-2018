import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {ParkingPage} from "../pages/parking/parking";

import {ShardAlertPage} from "../pages/shard-alert/shard-alert";
import {NewsPage} from "../pages/news/news";
import {DumpPage} from "../pages/dump/dump";
import {EventsPage} from "../pages/events/events";
import {ParkingPermitPage} from "../pages/parking-permit/parking-permit";
import {AdministrationPage} from "../pages/administration/administration";
import {StudentPermitPage} from "../pages/student-permit/student-permit";

import {ParkAreasPage} from "../pages/park-areas/park-areas";
import {PlayGroundsPage} from "../pages/play-grounds/play-grounds";
import {SportAreasPage} from "../pages/sport-areas/sport-areas";
import {SportclubsPage} from "../pages/sportclubs/sportclubs";
import {KitaMapPage} from "../pages/kita-map/kita-map";
import { HandicapedPage } from "../pages/handicaped/handicaped";


import { Geolocation } from '@ionic-native/geolocation';
import {DumpcalendarPage} from "../pages/dumpcalendar/dumpcalendar";
import { CalendarModule } from "ion2-calendar";
import {NewsDetailPage} from "../pages/news-detail/news-detail";
import {TouristAttractionsPage} from "../pages/tourist-attractions/tourist-attractions";
import {IdeaPage} from "../pages/idea/idea";
import {PushnotificationsPage} from "../pages/pushnotifications/pushnotifications";
import {UserAdministrationPage} from "../pages/user-administration/user-administration";
import {LogoutPage} from "../pages/logout/logout";
import {AttractionDetailPage} from "../pages/attraction-detail/attraction-detail";
import {ParkDetailPage} from "../pages/park-detail/park-detail";
import {FlatSearchPage} from "../pages/flat-search/flat-search";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ParkingPage,
    NewsPage,
    DumpPage,
    EventsPage,
    ParkingPermitPage,
    ShardAlertPage,
    AdministrationPage,
    //StudentPermitPage,
    ParkAreasPage,
    PlayGroundsPage,
    SportAreasPage,
    SportclubsPage,
    SportAreasPage,
    KitaMapPage,
    DumpcalendarPage,
    HandicapedPage,
    DumpcalendarPage,
    NewsDetailPage,
    TouristAttractionsPage,
    IdeaPage,
    PushnotificationsPage,
    UserAdministrationPage,
    LogoutPage,
    AttractionDetailPage,
    ParkDetailPage,
    FlatSearchPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    CalendarModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ParkingPage,
    NewsPage,
    DumpPage,
    EventsPage,
    ParkingPermitPage,
    ShardAlertPage,
    AdministrationPage,
    //StudentPermitPage,
    ParkAreasPage,
    PlayGroundsPage,
    SportAreasPage,
    KitaMapPage,
    SportAreasPage,
    SportclubsPage,
    DumpcalendarPage,
    HandicapedPage,
    DumpcalendarPage,
    NewsDetailPage,
    TouristAttractionsPage,
    IdeaPage,
    PushnotificationsPage,
    UserAdministrationPage,
    LogoutPage,
    AttractionDetailPage,
    ParkDetailPage,
    FlatSearchPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
