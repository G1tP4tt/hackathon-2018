import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParkingPermitPage } from './parking-permit';

@NgModule({
  declarations: [
    ParkingPermitPage,
  ],
  imports: [
    IonicPageModule.forChild(ParkingPermitPage),
  ],
})
export class ParkingPermitPageModule {}
