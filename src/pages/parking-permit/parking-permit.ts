import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';

/**
 * Generated class for the ParkingPermitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-parking-permit',
    templateUrl: 'parking-permit.html',
})
export class ParkingPermitPage {

    constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ParkingPermitPage');
    }

    sendForm() {
        const toast = this.toastCtrl.create({
            message: 'Formular wurde erfolgeich übermittelt',
            duration: 3000,
            position: "top"
        });
        this.navCtrl.pop();
        toast.present();
    }

}
