import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { KitaMapPage } from "../kita-map/kita-map";

/**
 * Generated class for the SportAreasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sport-areas',
  templateUrl: 'sport-areas.html',
})
export class SportAreasPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SportAreasPage');
  }

  goToMap() {
  	this.navCtrl.push(KitaMapPage);
  }
}
