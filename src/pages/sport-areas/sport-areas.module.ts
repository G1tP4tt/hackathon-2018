import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SportAreasPage } from './sport-areas';

@NgModule({
  declarations: [
    SportAreasPage,
  ],
  imports: [
    IonicPageModule.forChild(SportAreasPage),
  ],
})
export class SportAreasPageModule {}
