import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { KitaMapPage } from "../kita-map/kita-map";

/**
 * Generated class for the PlayGroundsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-play-grounds',
  templateUrl: 'play-grounds.html',
})
export class PlayGroundsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlayGroundsPage');
  }

   goToMap() {
  	this.navCtrl.push(KitaMapPage);
  }
}	
