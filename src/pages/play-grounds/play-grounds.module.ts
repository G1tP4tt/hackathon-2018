import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlayGroundsPage } from './play-grounds';

@NgModule({
  declarations: [
    PlayGroundsPage,
  ],
  imports: [
    IonicPageModule.forChild(PlayGroundsPage),
  ],
})
export class PlayGroundsPageModule {}
