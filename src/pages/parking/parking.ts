import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import * as request from "request-promise";
import {parkingHouse} from "../parking/parkingHouse";


import  * as Constants from "../../services/constants"
import { KitaMapPage } from "../kita-map/kita-map";





/**
 * Generated class for the ParkingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
   selector: 'page-parking',
   templateUrl: 'parking.html',
 })
 export class ParkingPage {


   parkingHouses: Array<parkingHouse> ;
   title: string;
   icon: string;
   visible: boolean;


   constructor(public navCtrl: NavController, public navParams: NavParams) {

     this.icon = "ios-car";
     this.title = "ANAL";
     this.visible = true;

     const promise = new Promise((resolve, reject) => {
       resolve(request(Constants.PARKING_HOUSE_URL, { contentType: 'application/json', json: 'true'}));
     });

     promise.then((response:any) => {
       var parking: Array<parkingHouse> = new Array();
       for(let entry of response.features){
         parking.push(new parkingHouse(entry.properties.obs_max, entry.properties.obs_free, entry.properties.park_name, entry.properties.prozent))
       }
       console.log(parkingHouse)
       this.parkingHouses = parking;
     });

     promise.catch((err) => {
    // This is never called
  });
     
     

     console.log(this.parkingHouses)
     
   }

   ionViewDidLoad() {
     console.log('ionViewDidLoad ParkingPage');
   }

 goToMap() {
    this.navCtrl.push(KitaMapPage);
  }

 }


