export class parkingHouse {
	
	location: {lat: number, lng: number};
	name: string;
	zone: string;
	maxSpots: number ;
	currentSpots: number;
	percent: number;
	

	constructor(public max: number, public current: number,public title: string, public loadPercent: number) {
		this.name = title;
		this.maxSpots = max;
		this.currentSpots = current;
		this.percent = loadPercent;
	}
}