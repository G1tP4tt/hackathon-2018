import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShardAlertPage } from './shard-alert';

@NgModule({
  declarations: [
    ShardAlertPage,
  ],
  imports: [
    IonicPageModule.forChild(ShardAlertPage),
  ],
})
export class ShardAlertPageModule {}
