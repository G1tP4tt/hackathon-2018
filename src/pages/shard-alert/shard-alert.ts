import { Component, ViewChild, ElementRef } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {AlertController} from 'ionic-angular';



/**
 * Generated class for the ShardAlertPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */



@Component({
  selector: 'page-shard-alert',
  templateUrl: 'shard-alert.html',
})



export class ShardAlertPage {

	
  

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController,public toastCtrl: ToastController) {
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ShardAlertPage');
  }



  sendForm(){
      const toast = this.toastCtrl.create({
          message: 'Verschmutzung wurde erfolgreich gemeldet',
          duration: 3000,
          position: "top"
      });
      this.navCtrl.pop();
      toast.present();
  }

   


}
