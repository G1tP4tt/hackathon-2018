import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import {ShardAlertPage} from "../shard-alert/shard-alert";
import {DumpcalendarPage} from "../dumpcalendar/dumpcalendar";


/**
 * Generated class for the DumpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dump',
  templateUrl: 'dump.html',
})
export class DumpPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goToShardAlert() {
  	this.navCtrl.push(ShardAlertPage);
  }

  goToDumpcalendar() {
  	this.navCtrl.push(DumpcalendarPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DumpPage');
  }

}
