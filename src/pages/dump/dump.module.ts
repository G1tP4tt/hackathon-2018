import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DumpPage } from './dump';
import {ShardAlertPage} from "../shard-alert/shard-alert";

@NgModule({
  declarations: [
    DumpPage,
    ShardAlertPage
  ],
  imports: [
    IonicPageModule.forChild(DumpPage),
  ],
})
export class DumpPageModule {}
