import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {AlertController} from 'ionic-angular';

import {ParkingPage} from "../parking/parking";
import {NewsPage} from "../news/news";
import {DumpPage} from "../dump/dump";
import {EventsPage} from "../events/events";
import {AdministrationPage} from "../administration/administration";

import {ParkAreasPage} from "../park-areas/park-areas";
import {PlayGroundsPage} from "../play-grounds/play-grounds";
import {SportAreasPage} from "../sport-areas/sport-areas";
import {SportclubsPage} from "../sportclubs/sportclubs";
import {ParkingPermitPage} from "../parking-permit/parking-permit";
import { KitaMapPage } from "../kita-map/kita-map";
import { HandicapedPage } from "../handicaped/handicaped";
import {TouristAttractionsPage} from "../tourist-attractions/tourist-attractions";
import {FlatSearchPage} from "../flat-search/flat-search";




@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    pages: Array<{title: string, icon: string, component: any, isShown: boolean}>;


    constructor(public navCtrl: NavController, public alertCtrl: AlertController) {

        this.pages = [
            {title: 'Parkhäuser', icon: 'ios-car', component: ParkingPage, isShown: true},
            {title: 'News', icon: 'md-paper', component: NewsPage, isShown: true},
            {title: 'Müll', icon: 'ios-basket', component: DumpPage, isShown: true},
            {title: 'Events', icon: 'md-beer', component: EventsPage, isShown: true},
            {title: 'Attraktionen', icon: 'ios-camera', component: TouristAttractionsPage, isShown: true},
            {title: 'Verwaltung', icon: 'ios-archive', component: AdministrationPage, isShown: true},
            {title: 'Parkausweis', icon: 'ios-card', component: ParkingPermitPage, isShown: true},
            {title: 'Parkanlagen', icon: 'ios-leaf', component: ParkAreasPage, isShown: false},
            {title: 'Spielplätze', icon: 'md-happy', component: PlayGroundsPage, isShown: false},
            {title: 'Kita Karte', icon: 'ios-heart', component: KitaMapPage, isShown: false},
            {title: 'Sportplätze', icon: 'ios-american-football', component: SportAreasPage, isShown: false},
            {title: 'Sportvereine', icon: 'ios-american-football', component: SportclubsPage, isShown: false},
            {title: 'Barrierefreiheit', icon: 'ios-medkit', component: HandicapedPage, isShown: true}
        ];
    }

    goToPage(page){
        this.navCtrl.push(page.component);
    }

    getActivePages() {
        var activePages = [];
        for (let page of this.pages) {
            if (page.isShown) {
                activePages.push(page)
            }
        }
        return activePages;
    }

    doCustomizing() {
        let alert = this.alertCtrl.create();
        alert.setTitle('Welche Kacheln möchten Sie sehen?');

        for (let page of this.pages) {
            alert.addInput({
                type: 'checkbox',
                label: page.title,
                value: "",
                checked: page.isShown
            });
        }


        alert.addButton('Cancel');
        alert.addButton({
            text: 'Okay',
            handler: (data: any) => {
                var i = 0;
                for (let box of alert.data.inputs) {
                    this.pages[i].isShown = box.checked;
                    i++;
                }
            }
        });

        alert.present();
    }
}
