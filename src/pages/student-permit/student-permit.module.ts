import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StudentPermitPage } from './student-permit';

@NgModule({
  declarations: [
    StudentPermitPage,
  ],
  imports: [
    IonicPageModule.forChild(StudentPermitPage),
  ],
})
export class StudentPermitPageModule {}
