import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import * as request from "request-promise"
import  * as Constants from "../../services/constants"
declare var google;
let directionsService = new google.maps.DirectionsService;
let directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});

/**
 * Generated class for the KitaMapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
   selector: 'page-kita-map',
   templateUrl: 'kita-map.html',
 })
 export class KitaMapPage {

   @ViewChild('map') mapElement: ElementRef;
   @ViewChild('directionsPanel') directionsPanel: ElementRef;

   map: any;
   markers = [];
   currentPosition: any;
   currentMarker: any;
   infoWindow: any;
   navInfo: any;

   markerClicked: boolean;

   constructor(public navCtrl: NavController, public navParams: NavParams,  public geolocation: Geolocation, public loadingCtrl: LoadingController) {

   }

   ionViewDidLoad() {
     this.presentLoadingDefault()
     this.loadMap()
     console.log('ionViewDidLoad KitaMapPage');
   }

   loading: any;

  presentLoadingDefault() {
   this.loading = this.loadingCtrl.create({
    content: 'Einen Moment Bitte :)'
  });

  this.loading.present();

}


   loadMap(){

     this.geolocation.getCurrentPosition().then((position) => {
       this.loading.dismiss();
       var lat = position.coords.latitude;
       var lng = position.coords.longitude
       this.currentPosition = {lat, lng};
       let latLng = new google.maps.LatLng(lat , lng);

       let mapOptions = {
         center: latLng,
         zoom: 12,
         mapTypeId: google.maps.MapTypeId.ROADMAP
       }

       this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

     }, (err) => {
       console.log(err);
     });


   }

   blitzerMarker = [["47.990012", "7.836557" , "Geschwindigkeitskontrolle 50 km/h"],

   ["47.990659" ,"7.856998" , "Rotlichtkontrolle"],

   ["47.999056" ,"7.846157" , "Geschwindigkeits- und Rotlichtkontrolle 50 km/h"],

   ["47.990835" ,"7.853392" , "Geschwindigkeitskontrolle 22-6 Uhr 30km/h, 6-22 Uhr 50km/h"],

   ["47.988948" ,"7.881811" , "Rotlichtkontrolle"],

   ["47.986351" ,"7.871511" , "Geschwindigkeitskontrolle 50 km/h"],
   ["48.024957" ,"7.856532" , "Rotlichtkontrolle"],

   ["47.994564" ,"7.856235" , "Geschwindigkeitskontrolle 50 km/h"],

   ["47.993090" ,"7.832336" , "Geschwindigkeitskontrolle 22-6 Uhr 30km/h, 6-22 Uhr 50km/h"],

   ["47.994985" , "7.828762" , "Geschwindigkeitskontrolle 60 km/h"]];

   kitaMarker = [["47.993090", "7.852925" , "Katholischer Kindergarten Unserer Lieben Frau"],

   ["48.010120" ,"7.808920" , "Kindergarten Matth‰usgemeinde"],

   ["48.008984" ,"7.850273" , "Katholischer Kindergarten St.Konrad-St.Hildegard"],
   ["47.986700" ,"7.846640" , "Kindergarten Sankt Raphael"],
   ["48.004050" ,"7.816290" , "Freiburger Kinderhausinitiative e.V."],
   ["52.207663" ,"8.065830" , "Katholischer Kindergarten St.Marien"],
   ["48.006855" ,"7.832122" , "Kindergarten - Sozial- und Jugendzentrum Breisacher Hof"],
   ["48.003090" ,"7.822769" , "JHW - Kita Lˆwenzahn"],
   ["48.001040" ,"7.840690" , "Uni-Kita Zaubergarten"],
   ["47.997020" ,"7.835610" , "St‰dt. Sch¸lerhort Herz Jesu"],
   ["53.655216" , "10.097817" , "Kath. Kindergarten St. Bernhard"]];


   parkAndRideMarker = [["48.002050", "7.820060" , "Bissierstraﬂe 2B, 79114 Freiburg im Breisgau"],

["48.002370" ,"7.823790" , "Bissierstraﬂe 7, 79114 Freiburg im Breisgau"],

["47.988679" ,"7.795437" , "Munzinger Str. 1, 79111 Freiburg im Breisgau"],
["48.033938" , "7.863533" , "Gundelfinger Str. 55, 79108 Freiburg im Breisgau"],
["48.028070" , "7.809071" , "Els‰sser Str. 100, 79110 Freiburg im Breisgau"],
["48.013487" , "7.806344" , "Breisgauer Str. 1, 79110 Freiburg im Breisgau"],
["47.996742" , "7.790783" , "Ingeborg-Drewitz-Allee 11, 79111 Freiburg im Breisgau"]];

   show(poi, fab){
     fab.close();
     var pois: any[] ; 
     this.map.setZoom(12);

     this.clear();

     var type ; 

     switch (poi) {
       case "Blitzer":
       pois = this.blitzerMarker;
       type = "blitzer"
       break;
       case "KiTa":
       pois = this.kitaMarker;
       type = "kita"
       break;
       case "P&R":
       pois = this.parkAndRideMarker;
       type = "parking"
       break;
       

       default:
       pois = this.parkAndRideMarker;
       type = "parking"
       break;
     }

     var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
        var icons = {
          parking: {
            icon: iconBase + 'parking_lot_maps.png'
          },
          kita: {
            icon: iconBase + 'library_maps.png'
          },
          blitzer: {
            icon: '../../assets/icon/blitzer.png'
          }
        };
     
     for (let point of pois){


       let marker = new google.maps.Marker({
         map: this.map,
         animation: google.maps.Animation.DROP,
         position: new google.maps.LatLng(point[0], point[1]), 
         description: point[2],
         icon: icons[type].icon
       });
      
       this.markers.push(marker);
       let content = "<div #directionsPanel><h4>"+poi+"!</h4> <br> <h5>"+marker.description+"</h5></div>";


       this.addInfoWindow(marker, content);
     }


   }
   clear() {
     directionsDisplay.setMap(null);
     this.markerClicked = false;
     if(this.navInfo){
           this.navInfo.close();
     }
     for (let marker of this.markers) {
       marker.setMap(null);
     }
     
   }

   startNavigating(){

     this.infoWindow.close();
     this.markerClicked = false;
     directionsDisplay.setMap(this.map);

     directionsDisplay.setPanel(document.getElementById('directionsPanel'));


    


     directionsService.route({
       origin: this.currentPosition,
       destination: this.currentMarker,
       travelMode: google.maps.TravelMode['WALKING']
     }, (res, status) => {

       if(status == google.maps.DirectionsStatus.OK){
         directionsDisplay.setDirections(res);
         var step = 1;
         if(this.navInfo){
           this.navInfo.close();
           this.infoWindow.close();
         }
         
         this.navInfo = new google.maps.InfoWindow();
         this.navInfo.setContent(res.routes[0].legs[0].distance.text + "<br>" + res.routes[0].legs[0].duration.text + " ");
         this.navInfo.setPosition(res.routes[0].legs[0].steps[step].end_location);
         this.navInfo.open(this.map);
       } else {
         console.warn(status);
       }

     });
   }

   addInfoWindow(marker, content){

     var infoWindow = new google.maps.InfoWindow({
       content: content,
       
     });

     

     google.maps.event.addListener(marker, 'click', () => {
       directionsDisplay.setMap(null);

       this.markerClicked = true;
       this.currentMarker = marker.position;
       if(this.navInfo){
           this.navInfo.close();
           this.infoWindow.close();
         }

         if(this.infoWindow){
           this.infoWindow.close();
         }

       infoWindow.open(this.map, marker);
       this.infoWindow = infoWindow;
     });

   }

 }
