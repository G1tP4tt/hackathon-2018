import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KitaMapPage } from './kita-map';

@NgModule({
  declarations: [
    KitaMapPage,
  ],
  imports: [
    IonicPageModule.forChild(KitaMapPage),
  ],
})
export class KitaMapPageModule {}
