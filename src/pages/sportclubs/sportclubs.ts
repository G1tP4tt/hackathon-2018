import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { KitaMapPage } from "../kita-map/kita-map";
/**
 * Generated class for the SportclubsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sportclubs',
  templateUrl: 'sportclubs.html',
})
export class SportclubsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SportclubsPage');
  }
goToMap() {
  	this.navCtrl.push(KitaMapPage);
  }
}
