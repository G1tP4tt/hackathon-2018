import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SportclubsPage } from './sportclubs';

@NgModule({
  declarations: [
    SportclubsPage,
  ],
  imports: [
    IonicPageModule.forChild(SportclubsPage),
  ],
})
export class SportclubsPageModule {}
