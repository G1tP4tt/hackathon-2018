import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FlatSearchPage } from './flat-search';

@NgModule({
  declarations: [
    FlatSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(FlatSearchPage),
  ],
})
export class FlatSearchPageModule {}
