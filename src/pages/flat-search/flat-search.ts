import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {KitaMapPage} from "../kita-map/kita-map";

/**
 * Generated class for the FlatSearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-flat-search',
  templateUrl: 'flat-search.html',
})
export class FlatSearchPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FlatSearchPage');
  }

  goToMap() {
    this.navCtrl.push(KitaMapPage);
  }

}
