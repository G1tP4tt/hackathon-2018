import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import {ShardAlertPage} from "../shard-alert/shard-alert";
import {RealEstatePage} from "../real-estate/real-estate";
import {StudentPermitPage} from "../student-permit/student-permit";

/**
 * Generated class for the AdministrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-administration',
  templateUrl: 'administration.html',
})
export class AdministrationPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goToPage(page) {
  	
  	this.navCtrl.push(page);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdministrationPage');
  }

}
