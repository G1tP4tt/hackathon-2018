import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { KitaMapPage } from "../kita-map/kita-map";

/**
 * Generated class for the HandicapedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-handicaped',
  templateUrl: 'handicaped.html',
})
export class HandicapedPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HandicapedPage');
  }

  goToMap() {
  	this.navCtrl.push(KitaMapPage);
  }
}
