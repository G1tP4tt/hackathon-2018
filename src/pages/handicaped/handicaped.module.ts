import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HandicapedPage } from './handicaped';

@NgModule({
  declarations: [
    HandicapedPage,
  ],
  imports: [
    IonicPageModule.forChild(HandicapedPage),
  ],
})
export class HandicapedPageModule {}
