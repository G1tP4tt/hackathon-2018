import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { KitaMapPage } from "../kita-map/kita-map";
import {ParkDetailPage} from "../park-detail/park-detail";

/**
 * Generated class for the ParkAreasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-park-areas',
  templateUrl: 'park-areas.html',
})
export class ParkAreasPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParkAreasPage');
  }

  parkDetails() {
  	this.navCtrl.push(ParkDetailPage);
  }
}
