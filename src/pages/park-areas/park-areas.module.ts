import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParkAreasPage } from './park-areas';
import {ParkDetailPage} from "../park-detail/park-detail";

@NgModule({
  declarations: [
    ParkAreasPage,
    ParkDetailPage
  ],
  imports: [
    IonicPageModule.forChild(ParkAreasPage),
  ],
})
export class ParkAreasPageModule {}
