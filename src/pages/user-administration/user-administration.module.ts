import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserAdministrationPage } from './user-administration';

@NgModule({
  declarations: [
    UserAdministrationPage,
  ],
  imports: [
    IonicPageModule.forChild(UserAdministrationPage),
  ],
})
export class UserAdministrationPageModule {}
