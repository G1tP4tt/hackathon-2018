import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CalendarModule, CalendarComponentOptions} from "ion2-calendar";

/**
 * Generated class for the DumpcalendarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dumpcalendar',
  templateUrl: 'dumpcalendar.html',
})
export class DumpcalendarPage {

  date: string;
  type: 'string'; // 'string' | 'js-date' | 'moment' | 'time' | 'object'
  optionsMulti: CalendarComponentOptions = {
    pickMode: 'multi'
  };

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  onChange($event) {
    console.log($event);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DumpcalendarPage');
  }

}
