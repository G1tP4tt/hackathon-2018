import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DumpcalendarPage } from './dumpcalendar';

@NgModule({
  declarations: [
    DumpcalendarPage,
  ],
  imports: [
    IonicPageModule.forChild(DumpcalendarPage),
  ],
})
export class DumpcalendarPageModule {}
