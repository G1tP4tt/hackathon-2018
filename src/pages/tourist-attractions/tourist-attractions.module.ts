import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TouristAttractionsPage } from './tourist-attractions';
import {AttractionDetailPage} from "../attraction-detail/attraction-detail";

@NgModule({
  declarations: [
    TouristAttractionsPage,
    AttractionDetailPage
  ],
  imports: [
    IonicPageModule.forChild(TouristAttractionsPage),
  ],
})
export class TouristAttractionsPageModule {}
