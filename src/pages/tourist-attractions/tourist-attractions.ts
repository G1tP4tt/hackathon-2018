import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AttractionDetailPage} from "../attraction-detail/attraction-detail";
import {KitaMapPage} from "../kita-map/kita-map";

/**
 * Generated class for the TouristAttractionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tourist-attractions',
  templateUrl: 'tourist-attractions.html',
})
export class TouristAttractionsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TouristAttractionsPage');
  }

  attractionDetail(){
    this.navCtrl.push(AttractionDetailPage)
  }
  goToMap() {
    this.navCtrl.push(KitaMapPage);
  }

}
