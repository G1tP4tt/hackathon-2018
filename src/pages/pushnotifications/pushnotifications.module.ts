import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PushnotificationsPage } from './pushnotifications';

@NgModule({
  declarations: [
    PushnotificationsPage,
  ],
  imports: [
    IonicPageModule.forChild(PushnotificationsPage),
  ],
})
export class PushnotificationsPageModule {}
